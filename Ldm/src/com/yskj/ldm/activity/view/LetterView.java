package com.yskj.ldm.activity.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class LetterView extends View{

	public static String[] letters = { "#", "A", "B", "C", "D", "E", "F", "G", "H",  
	            "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",  
		        "V", "W", "X", "Y", "Z" };  

	
	public int selectedIndex = 0;
	
	private Paint paint = new Paint();
	
	private int height = 1;
	
	public LetterView(Context context, AttributeSet attrs){
		super(context,attrs);
		// TODO Auto-generated constructor stub

	}
	
	
	protected void onDraw(Canvas canvas) {  
		
		 super.onDraw(canvas); 
		 
		 height  =getHeight()/letters.length;
		 
		 for(int i=0;i<letters.length;i++){
			 
			 paint.setColor(0xFF979797);  
			 paint.setAntiAlias(true);  
			 paint.setTextSize(16);  
			 if (i == selectedIndex) {  
				 paint.setColor(Color.parseColor("#3399ff"));  
				 paint.setFakeBoldText(true);  
			 }  

			 canvas.drawText(letters[i], 10, height+i*height, paint); 
			 
		 }
	}
	 
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
 
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			
			selectedIndex = (int)(event.getY()/height);
			
			selectedIndex = (selectedIndex>=letters.length)?(letters.length-1):selectedIndex;
			
			if(listener != null){
				listener.onTouchingLetter(letters[selectedIndex]);
			}
		}
		
	    invalidate();  
		
		return true;
	   
	}
	
	
	private OnTouchingLetterListener listener;
	
	public void setOnTouchLetterListener(OnTouchingLetterListener listn){
		this.listener = listn;
	}
	
	public interface OnTouchingLetterListener {  
		
		public void onTouchingLetter(String s);
   }

	
}
