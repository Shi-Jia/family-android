package com.yskj.ldm.activity.view;



import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.activity.ui.CameraActivity;
import com.yskj.ldm.util.SDCardStoreManager;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;



public class CameraView extends SurfaceView implements Callback {

	SurfaceHolder mSurfaceHolder;
	Camera mCamera = null;

	Context mContext;
	private final int cameraId = 0;
	private CameraActivity activity;
	private Handler mHandler;
	private int cameraPosition = 1;//0代表前置摄像头，1代表后置摄像头
	private ShutterCallback shutter = new ShutterCallback() {

		public void onShutter() {

		}
	};
	public void setActivity(CameraActivity activity)
	{
		this.activity = activity;
	}
	public void setHandle(Handler handler) {
		mHandler = handler;
	}

	private PictureCallback raw = new PictureCallback() {

		public void onPictureTaken(byte[] data, Camera camera) {

		}
	};
	/**
	 * 图片反转
	 * @param img
	 * @return
	 */
	public Bitmap toturn(Bitmap img){
		Matrix matrix = new Matrix();
		matrix.postRotate(90); /*翻转90度*/
		int width = img.getWidth();
		int height =img.getHeight();
		img = Bitmap.createBitmap(img, 0, 0, width, height, matrix, true);
		return img;
	}

	private PictureCallback jpeg = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {

			File file = null;
			Bitmap	bm = null;
			Message message = new Message();
			try {
				bm =  BitmapFactory.decodeByteArray(data, 0, data.length);
				file =  SDCardStoreManager.getInstance().getFilePath(SDCardStoreManager.getInstance().getRootPath()+RecordsPath.ImageRoot,System.currentTimeMillis()+".jpg");
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
				bm.compress(Bitmap.CompressFormat.JPEG, 100, bos); // 保存图片
				Log.i("file", " filePath --> "+ file.getPath());
				bos.flush();
				bos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			finally
			{

				if(null!=file)
				{
					if(file.exists())
						message.obj = file.getPath();

					else
						message.obj = "";
				}
				activity.sendMessage(message);
				if(null!=bm){
					bm = null;
					System.gc();
				}
			}
		}
	};


	private PreviewCallback mPreviewCallback = new PreviewCallback() {

		public void onPreviewFrame(byte[] data, Camera camera) {

		}
	};

	private AutoFocusCallback autoFocus = new AutoFocusCallback() {

		public void onAutoFocus(boolean success, Camera camera) {
		}
	};

	public void changeCamera(SurfaceHolder holder)
	{
		//切换前后摄像头
		int cameraCount = 0;
		CameraInfo cameraInfo = new CameraInfo();
		cameraCount = Camera.getNumberOfCameras();//得到摄像头的个数

		for(int i = 0; i < cameraCount; i ++  ) {
			Camera.getCameraInfo(i, cameraInfo);//得到每一个摄像头的信息
			if(cameraPosition == 1) {
				//现在是后置，变更为前置
				if(cameraInfo.facing  == Camera.CameraInfo.CAMERA_FACING_FRONT) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置  
					mCamera.stopPreview();//停掉原来摄像头的预览
					mCamera.release();//释放资源
					mCamera = null;//取消原来摄像头
					mCamera = Camera.open(i);//打开当前选中的摄像头
					try {
						mCamera.setPreviewDisplay(holder);//通过surfaceview显示取景画面
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					mCamera.startPreview();//开始预览
					cameraPosition = 0;
					break;
				}
			} else {
				//现在是前置， 变更为后置
				if(cameraInfo.facing  == Camera.CameraInfo.CAMERA_FACING_BACK) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置  
					mCamera.stopPreview();//停掉原来摄像头的预览
					mCamera.release();//释放资源
					mCamera = null;//取消原来摄像头
					mCamera = Camera.open(i);//打开当前选中的摄像头
					try {
						mCamera.setPreviewDisplay(holder);//通过surfaceview显示取景画面
					} catch (IOException e) {
						e.printStackTrace();
					}
					mCamera.startPreview();//开始预览
					cameraPosition = 1;
					break;
				}
			}

		}
	}
	public void openFlash()
	{
		Parameters p = mCamera.getParameters();
		p.setFlashMode(Parameters.FLASH_MODE_TORCH);
		mCamera.setParameters(p);
	}
	public void closeFlash()
	{
		Parameters p = mCamera.getParameters();
		p.setFlashMode(Parameters.FLASH_MODE_OFF);
		mCamera.setParameters(p);
	}
	/**
	 * 拍照
	 */
	public void tackPicture() {
		mCamera.takePicture(shutter, raw, jpeg);
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				if(null!=mCamera){
					mCamera.startPreview();
					//					mCamera.autoFocus(autoFocus);
				}
			}
		}, 1500);
	}


	public void startCamera() {
		mCamera.startPreview();
	}

	public void stopCamera() {
		if (mCamera != null) {
			mCamera.stopPreview();
		}
	}
	public void releaseCamera()
	{
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);  
			mCamera.stopPreview();// 停止预览
			mCamera.release();// 释放相机资源
			mCamera = null;
		}
	}
	public CameraView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mSurfaceHolder = getHolder();// 获得surfaceHolder引用
		mSurfaceHolder.addCallback(this);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);// 设置类型
		mContext = context;
	}

	public CameraView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mSurfaceHolder = getHolder();// 获得surfaceHolder引用
		mSurfaceHolder.addCallback(this);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);// 设置类型
		mContext = context;

	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

		Camera.Parameters parameters = mCamera.getParameters();// 获得相机参数
		parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);  
		parameters.set("orientation", "portrait");
		parameters.set("rotation", 90); // 镜头角度转90度（默认摄像头是横拍） 
		mCamera.setDisplayOrientation(this.curDregree(cameraId));
		
//		if (Integer.parseInt(Build.VERSION.SDK) >= 8)
//			setDisplayOrientation(mCamera, 90);
//		else {
//			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//				parameters.set("orientation", "portrait");
//				parameters.set("rotation", 90);
//			}
//			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//				parameters.set("orientation", "landscape");
//				parameters.set("rotation", 90);
//			}
//		}

		//		if (parameters.getMaxNumMeteringAreas() > 0){ // 检查是否支持测光区域  
		//		    List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();  
		//		  
		//		    Rect areaRect1 = new Rect(-100, -100, 100, 100);    // 在图像的中心指定一个区域  
		//		    meteringAreas.add(new Camera.Area(areaRect1, 600)); // 设置宽度到60%  
		//		    Rect areaRect2 = new Rect(800, -1000, 1000, -800);  // 在图像的右上角指定一个区域  
		//		    meteringAreas.add(new Camera.Area(areaRect2, 400)); // 收置宽度为40%  
		//		    parameters.setMeteringAreas(meteringAreas);  
		//		}  

		mCamera.startPreview();
		mCamera.setParameters(parameters);// 设置相机参数
		//		mCamera.autoFocus(autoFocus);
	}

	protected void setDisplayOrientation(Camera camera, int angle) {
		Method downPolymorphic;
		try {
			downPolymorphic = camera.getClass().getMethod(
					"setDisplayOrientation", new Class[] { int.class });
			if (downPolymorphic != null)
				downPolymorphic.invoke(camera, new Object[] { angle });
		} catch (Exception e1) {
		}
	}		  

	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		if (mCamera == null) {
			mCamera = Camera.open(cameraId);// 开启相机,不能放在构造函数中，不然不会显示画面.
			mCamera.setPreviewCallback(mPreviewCallback);
			try {
				mCamera.setPreviewDisplay(holder);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		if(null!=mCamera){
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();// 停止预览
			mCamera.release();// 释放相机资源
			mCamera = null;
		}
	}
	public  int ScreenOrient(Activity activity) {

		int orient = activity.getRequestedOrientation();
		if (orient != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE && orient != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
			// 宽>高为横屏,反正为竖屏
			WindowManager windowManager = activity.getWindowManager();
			Display display = windowManager.getDefaultDisplay();
			int screenWidth = display.getWidth();
			int screenHeight = display.getHeight();
			orient = screenWidth < screenHeight ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
					: ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
		}
		return orient;
	}

	public   int curDregree(int cameraId) {
		android.hardware.Camera.CameraInfo info =
				new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0: degrees = 0; break;
		case Surface.ROTATION_90: degrees = 90; break;
		case Surface.ROTATION_180: degrees = 180; break;
		case Surface.ROTATION_270: degrees = 270; break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
		} else {  // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		return result;
	}
}
