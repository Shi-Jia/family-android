package com.yskj.ldm.activity.view;

 

import com.yskj.ldm.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TabHost;

 

public class AnimTabHost extends TabHost {

	
	private Animation slideLeftIn;// 从屏幕左边进来 
	private Animation slideLeftOut;// 从屏幕左边出去 
	private Animation slideRightIn;// 从屏幕右边进来 
	private Animation slideRightOut;// 从屏幕右边出去 

	
	public AnimTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		slideLeftIn = AnimationUtils.loadAnimation(context, R.anim.push_left_in); 
		slideLeftOut = AnimationUtils.loadAnimation(context,R.anim.push_left_out); 
		slideRightIn = AnimationUtils.loadAnimation(context,R.anim.push_right_in); 
		slideRightOut = AnimationUtils.loadAnimation(context,R.anim.push_right_out); 

	}
	
	
	public void setCurrentTab(int index) { 
		// 切换前所在页的页面 
		int mCurrentTabID = getCurrentTab(); 
		
		if(mCurrentTabID == index){
			return;
		}
		
		int count = this.getChildCount();
		
		if (null != getCurrentView()) { 

			// 循环时，末页到第一页(边界处理) 
			if (mCurrentTabID == (count - 1) && index == 0) { 
				getCurrentView().startAnimation(slideLeftOut); 
			} 
			// 循环时，首页到末页 
			else if (mCurrentTabID == 0 && index == (count - 1)) { 
				getCurrentView().startAnimation(slideRightOut); 
			} 
			// 切换到右边的界面，从左边离开 
			else if (index > mCurrentTabID) { 
				getCurrentView().startAnimation(slideLeftOut); 
			} 
			// 切换到左边的界面，从右边离开 
			else if (index < mCurrentTabID) { 
				getCurrentView().startAnimation(slideRightOut); 
			} 
		}
		
		// 设置当前页 
		super.setCurrentTab(index); 
		  	
		// 循环时，末页到第一页 
		if (mCurrentTabID == (count - 1) && index == 0) { 
			getCurrentView().startAnimation(slideLeftIn); 
		} 
		// 循环时，首页到末页(边界处理) 
		else if (mCurrentTabID == 0 && index == (count - 1)) { 
			getCurrentView().startAnimation(slideLeftIn); 
		} 
		// 切换到右边的界面，从右边进来 
		else if (index > mCurrentTabID) { 
			getCurrentView().startAnimation(slideLeftIn); 
		} 
		// 切换到左边的界面，从左边进来 
		else if (index < mCurrentTabID) { 
			getCurrentView().startAnimation(slideRightIn); 
		} 
	
	} 
}
