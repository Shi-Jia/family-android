package com.yskj.ldm.activity.cache;

 

import java.io.File;

public class RecordsPath {

	public static final String ImageRoot=File.separator+"laodaomi"+File.separator+"images"+File.separator;
	public static final String ImageFilterRoot=File.separator+"laodaomi"+File.separator+"filterimages"+File.separator;
	public static final String CacheImage = File.separator+"laodaomi"+File.separator+"cache"+File.separator+"images"+File.separator;
	public static final String DownLoadFile = File.separator+"laodaomi"+File.separator+"file"+File.separator;


	//	String imageUri = "http://site.com/image.png"; // from Web
	//	String imageUri = "file:///mnt/sdcard/image.png"; // from SD card
	//	String imageUri = "content://media/external/audio/albumart/13"; // from content provider
	//	String imageUri = "assets://image.png"; // from assets
	//	String imageUri = "drawable://" + R.drawable.image; // from drawables (only images, non-9patch)
	public static final String UriWeb = "http://"; // from Web
	public static final String UriSDCard = "file://"; // from SD card
	public static final String UriContent = "content://"; // from content provider
	public static final String UriAssets = "assets://"; // from assets
	public static final String UriDrawable = "drawable://";  //   "drawable://" + R.drawable.image;  from drawables (only images, non-9patch)
	
	
}
