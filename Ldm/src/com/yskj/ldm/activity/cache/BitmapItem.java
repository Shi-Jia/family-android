package com.yskj.ldm.activity.cache;

 
import android.graphics.Bitmap;
import android.widget.ImageView;

public class BitmapItem {

	public Bitmap bmp;

	private Object obj;
	
	public boolean empty = false;
	
	private IDrawableCallback callback;
	
	private String url;
	public ImageView getView() {
		return view;
	}

	public void setView(ImageView view) {
		this.view = view;
	}

	ImageView view;

	
	
	 
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public BitmapItem(IDrawableCallback callback){
		
 
		this.callback = callback;
	}
	
	public BitmapItem(ImageView view,Object obj,IDrawableCallback callback){
		
		this(callback);
		this.obj = obj;
	    this.view = view;
	}
	
	
	public void setObject(Object obj){
		this.obj = obj;
	}
	
	public Object getObj(){
		return obj;
	}
	
	
	public void setCallbackListener(IDrawableCallback callback){
		this.callback = callback;
	}
	
	public IDrawableCallback getCallbackListener(){
		return callback;
	}
	
}
