package com.yskj.ldm.activity.cache;

 

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

 







import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.util.Encrypt;
import com.yskj.ldm.util.SDCardStoreManager;
import com.yskj.ldm.util.StringUtil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;



public class ImageCacheManager {


	private static ImageCacheManager instance;

	//内存中图片库
	private  HashMap<String,BitmapItem> mHash = new HashMap<String,BitmapItem>();

	//SDCard管理
	private SDCardStoreManager sdcardManager = SDCardStoreManager.getInstance();


	public ImageCacheManager(){
		mHash.clear();
	}


	public static ImageCacheManager getInstance(){

		if(instance == null){
			instance = new ImageCacheManager();
		}
		return instance;
	}


	public void clear(){

		Iterator<Entry<String,BitmapItem>> iterator = mHash.entrySet().iterator(); 
		while (iterator.hasNext() ) {

			Entry<String,BitmapItem> entry = iterator.next();

			BitmapItem item = entry.getValue();
			item.bmp = null;
			item.empty = false;

			iterator.remove();	
		}

		System.gc();
	}


	public void remove(String uri){

		final String url = StringUtil.getValidUrl(ConnectionManager.IMAGE_HOST, uri);
		BitmapItem item = mHash.remove(url);

		if(item != null){
			item.bmp = null;
			item.empty = false;
		}
	}

	public BitmapItem getBitmapItem(String uri){

		final String url = StringUtil.getValidUrl(ConnectionManager.IMAGE_HOST, uri);

		if(mHash.containsKey(url)){ //内存中查找
			return mHash.get(url);
		}

		return null;
	}

	/**
	 * 加载相应图片
	 * @param url
	 * @param defaultBmp
	 * @param callback
	 * @return
	 */
	public  void loadDrawable(String uri,final Object obj,final ImageView view,final boolean save,final IDrawableCallback callback){
		if(uri == null){
			return;
		}
		//		final String url = StringUtil.getValidationUrl(uri);
		final String url =  uri;
		System.out.println("url="+uri);
		if(isExsitInSDCard(url)){ //SDCard中查找
			String url_2 = RecordsPath.UriSDCard +SDCardStoreManager.getInstance().getRootPath()+getImageResourcePath(url);
			Message msg = new Message();
			BitmapItem item = new BitmapItem(view,obj,callback);
			item.setUrl(url_2);
			msg.obj =item;
			handler.sendMessage(msg);
		}else{//网络资源中读取
			new Thread(new Runnable(){

				public void run(){

					InputStream input = loadImageFromUrl(url);
					String url_1 = null;
					if(input != null)
					{
						if(save && writeToSDCard(input,url)){ //先写入文件
							url_1 =  RecordsPath.UriSDCard +SDCardStoreManager.getInstance().getRootPath()+getImageResourcePath(url);
						}else{ //直接从网络流中读
							url_1 =  url;
						}
					}
					else
					{
						url_1 =  url;
					}
					Message msg = new Message();
					BitmapItem item = new BitmapItem(view,obj,callback);
					item.setUrl(url_1);
					msg.obj = item;
					handler.sendMessage(msg);
				}
			}).start();
		}
	}



	private boolean isExsitInSDCard(String url){
		return sdcardManager.isExsit(getImageResourcePath(url));
	}



	private String getImageResourcePath(String url){

		return RecordsPath.CacheImage+Encrypt.md5(url);
	}


	private BitmapItem readFromSDCard(String url){

		BitmapItem item = mHash.get(url);

		InputStream input = sdcardManager.read(getImageResourcePath(url));

		if(input != null && item != null){	
			item.bmp = createBitmap(input);
			item.empty = true;
		}

		return item;
	}


	private boolean writeToSDCard(InputStream input,String url){

		try{

			byte[] body  = sdcardManager.readBytes(input);

			if(body != null){

				String path = getImageResourcePath(url);
				return sdcardManager.write(path, body);
			}
		}catch(OutOfMemoryError e){
			e.printStackTrace();
			return false;
		}

		return false;
	}



	private Bitmap createBitmap(InputStream input){

		Bitmap bmp = null;

		try{
			bmp = BitmapFactory.decodeStream(input);
			input.close();
			input = null;
		}catch(OutOfMemoryError e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		return bmp;
	}

	/*************************************************************************
	 * 获取网络资源图片
	 * @param url
	 * @return
	 */
	private  InputStream loadImageFromUrl(String url) {

		InputStream input = null;

		try {

			if(url != null && !("".equals(url))){

				HttpGet httpGet = new HttpGet(url);
				HttpClient client = new DefaultHttpClient();

				HttpResponse response = client.execute(httpGet);
				HttpEntity entity = response.getEntity();

				input = entity.getContent();
			}
		}catch(OutOfMemoryError e){
			e.printStackTrace();
		}catch (Exception e) {  
			e.printStackTrace();
		}

		return input;
	}


	protected Handler handler = new Handler(){

		public void handleMessage(Message msg){

			if(msg.obj != null){

				BitmapItem item = (BitmapItem)msg.obj;
				IDrawableCallback callback = item.getCallbackListener();

				if(callback != null){
					callback.onCallback(item);
				}
			}
		}
	};

}
