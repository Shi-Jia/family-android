package com.yskj.ldm.activity.cache;



import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferencesService {

	private Context context;
	private static final  String PreferencesName ="NAME";
	public  static   PreferencesService  preferencesService= null;
	SharedPreferences preferences ;
	public PreferencesService(Context context) {
		this.context = context;
		preferences = context.getSharedPreferences(PreferencesName, Context.MODE_PRIVATE);
	}
	public static PreferencesService getInstance(Context context)
	{
		if(preferencesService == null)
			preferencesService = new PreferencesService(context);
		return preferencesService;
	} 
	public void saveUserInfo(String userid, String circleid,String phone,String password,
			String nickname,String adress,String birth,String qq,String email,
			String areaid,String regdate,String sex,String headporttait,String state) {
		//获得SharedPreferences对象
	 
		Editor editor = preferences.edit();
		editor.putString("USER_ID", userid);
		editor.putString("CIRCLE_ID", circleid);
		editor.putString("USER_PHONE", phone);
		editor.putString("USER_PASSWORD", password);

		editor.putString("USER_NICKNAME", nickname);
		editor.putString("USER_ADRESS", adress);
		editor.putString("USER_BIRTH",  birth);
		editor.putString("USER_QQ", qq);
		editor.putString("USER_EMAIL", email);

		editor.putString("AREA_ID", areaid);
		editor.putString("REG_DATE", regdate);
		editor.putString("USER_SEX", sex);
		editor.putString("USER_HEADPORTTAIT", headporttait);

		editor.putString("USER_STATE", state);


		editor.commit();

	}
    
	public void savaLoginType(Integer count)
	{
	 
		Editor editor = preferences.edit();
 
		editor.putInt("LOGIN_TYPE", count);
		editor.commit();
	}
	public void savaPushMessageBox(Integer count)
	{
	 
		Editor editor = preferences.edit();
	 
		editor.putInt("BOXCOUNT", count);
		editor.commit();
	}
	public void savaPushCircle(Integer count)
	{
	 
		Editor editor = preferences.edit();
	 
		editor.putInt("CIRCLECOUNT", count);
		editor.commit();
	}
	public void savaPushComments(Integer count)
	{
	 
		Editor editor = preferences.edit();
		 
		editor.putInt("COMMENTSCOUNT", count);
		editor.commit();
	}

	public void savaQqopenId(String openid)
	{
		 
		Editor editor = preferences.edit();
		editor.putString("QQOPENID", openid);
		editor.commit();
	}

	public void savaToken(String token,String expiretime)
	{
	 
		Editor editor = preferences.edit();
		editor.putString("TOKEN", token);
		editor.putString("EXPIRETIOME", expiretime);

		editor.commit();
	}
	public void savaBaiduUserInfo(String appid,String channelId,String requestId,String userId)
	{
		 
		Editor editor = preferences.edit();
		editor.putString("APPID", appid);
		editor.putString("CHANNELID", channelId);
		editor.putString("REQUESTID", requestId);
		editor.putString("USERID", userId);
		editor.commit();
	}
	public void savaVersionTag(boolean isok)
	{
	 
		Editor editor = preferences.edit();
		editor.putBoolean("IS_OK", isok);
		editor.commit();
	}
	public void savaVersion(Integer version)
	{
		 
		Editor editor = preferences.edit();
		editor.putInt("CUR_VERSION", version);
		editor.commit();
	}
	public void savaMessageCount(Integer num)
	{
		 
		Editor editor = preferences.edit();
		editor.putInt("MESSAGE_COUNT", num);
		editor.commit();
	}
	public void savaPushTag(boolean isok)
	{
	 
		Editor editor = preferences.edit();
		editor.putBoolean("PUSH_TAG", isok);
		editor.commit();
	}
	public void clearUserInfo()
	{
	 
		Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
	}
	public void saveLogin(boolean rememberpassword,boolean automaticlogin) {
		//获得//SharedPreferences对象
		 
		Editor editor = preferences.edit();
		editor.putBoolean("R_PASSWORD", rememberpassword);
		editor.putBoolean("A_LOGIN", automaticlogin);
		editor.commit();
	}

	/**
	 * 获取各项参数
	 * @return
	 */
	public Map<String, Object> getBaiduUserInfo() {
		Map<String, Object> params = new HashMap<String, Object>();
		 
		params.put("appid", preferences.getString("APPID",""));
		params.put("channelid", preferences.getString("CHANNELID", ""));
		params.put("requestid", preferences.getString("REQUESTID",""));
		params.put("userid", preferences.getString("USERID", ""));
		return params;
	}
	/**
	 * 获取各项参数
	 * @return
	 */
	public Map<String, Object> getTokenPerferences() {
		Map<String, Object> params = new HashMap<String, Object>();
	 
		params.put("token", preferences.getString("TOKEN",""));
		params.put("expiretime", preferences.getString("EXPIRETIOME", ""));
		return params;
	}
	public String getUser_id()
	{ 
	 
		return	preferences.getString("USER_ID","") ;
	}
	public Integer getLoginType()
	{
  
		return	preferences.getInt("LOGIN_TYPE",0) ;
	}
	/**
	 * 获取各项参数
	 * @return
	 */
	public Map<String, Object> getUserInfoPerferences() {
		Map<String, Object> params = new HashMap<String, Object>();
		 
		params.put("userid", preferences.getString("USER_ID",""));
		params.put("phone", preferences.getString("USER_PHONE", ""));
		params.put("circleid", preferences.getString("CIRCLE_ID",""));
		params.put("password",preferences.getString("USER_PASSWORD", ""));

		params.put("nickname", preferences.getString("USER_NICKNAME",""));
		params.put("address", preferences.getString("USER_ADRESS", ""));
		params.put("birth", preferences.getString("USER_BIRTH",""));
		params.put("qq",preferences.getString("USER_QQ", ""));

		params.put("email", preferences.getString("USER_EMAIL",""));
		params.put("address", preferences.getString("USER_ADRESS", ""));
		params.put("areaid", preferences.getString("AREA_ID",""));
		params.put("regdate",preferences.getString("REG_DATE", ""));

		params.put("sex", preferences.getString("USER_SEX", ""));
		params.put("headporttait", preferences.getString("USER_HEADPORTTAIT",""));
		params.put("state",preferences.getString("USER_STATE", ""));

		return params;
	}

	public Integer getVersion()
	{
		return preferences.getInt("CUR_VERSION", 0);
	}
	public Integer getPushMessageBox()
	{
		 
		return preferences.getInt("BOXCOUNT", 0);
	}
	public Integer getPushCircle()
	{
		return preferences.getInt("CIRCLECOUNT", 0);
	}
	public Integer getPushComments()
	{
		return preferences.getInt("COMMENTSCOUNT", 0);
	}
	public Integer getMessageCount()
	{
		return preferences.getInt("MESSAGE_COUNT", 0);
	}
	public boolean getVersionTag()
	{
		 
		return preferences.getBoolean("IS_OK",false);
	}
	public boolean getPushTag()
	{
		return preferences.getBoolean("PUSH_TAG",false);
	}
	public String getQqopenId()
	{
		return preferences.getString("QQOPENID", "");
	}
    
    
	/**
	 * 获取各项参数
	 * @return
	 */
	public Map<String, Object> getLoginPerferences() {
		Map<String, Object> params = new HashMap<String, Object>();
		 
		params.put("r_password", preferences.getBoolean("R_PASSWORD", false));
		params.put("a_login", preferences.getBoolean("A_LOGIN",false));
		return params;
	}
}
