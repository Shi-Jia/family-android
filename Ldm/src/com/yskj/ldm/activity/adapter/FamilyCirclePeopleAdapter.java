package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;
import com.yskj.ldm.util.StringUtil;


public class FamilyCirclePeopleAdapter  extends AllBaseAdapter {

	public FamilyCirclePeopleAdapter(Context c) {
		super(c);
	}


	protected View creatorView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder= null;
		if(convertView == null){
			holder = new ViewHolder();
			convertView =  (RelativeLayout)inflater.inflate(R.layout.familycircle_people_item, null);
			holder.head_image = (ImageView)convertView.findViewById(R.id.family_head);
			holder.t_name = (TextView)convertView.findViewById(R.id.family_name);

			holder.t_relationship = (TextView)convertView.findViewById(R.id.family_relationship);

			holder.t_content = (TextView)convertView.findViewById(R.id.family_content);
 
			holder.image_choise = (ImageView)convertView.findViewById(R.id.family_choise);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		if(list.size()>0){
			 Map<String,Object> params = ((Map<String,Object>)list.get(position)); 
//			holder.t_relationship.setText(params.get("user_birth").toString());
			if(!StringUtil.isNull(params.get("user_nikename").toString()))
			holder.t_name .setText(params.get("user_nikename").toString());
			holder.t_content.setText(params.get("user_phone").toString() );
		}
		return convertView;
	}

	private class ViewHolder {

		public ImageView head_image;

		public TextView t_name;

		public TextView t_relationship;

		public TextView t_content;

		public ImageView image_choise;

	}
}
