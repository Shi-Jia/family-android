package com.yskj.ldm.activity.adapter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.RecordsPath;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class PhotoAlbumAdapter extends AllBaseAdapter{

	public PhotoAlbumAdapter(Context c) {
		super(c);
 
	}
	protected View creatorView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder= null;
		if(convertView == null)
		{
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.photoalbum_item, null);
			holder.headImg = (ImageView)convertView.findViewById(R.id.photoalbum_head);
			holder.text = (TextView)convertView.findViewById(R.id.photoalbum_text);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}
 
		holder.text.setText(this.list.get(position).get("bucket_name").toString()+"("+this.list.get(position).get("count").toString()+")");
		ImageLoader.getInstance().displayImage(RecordsPath.UriSDCard+this.list.get(position).get("image_path").toString(), holder.headImg,this.options);
		return convertView;
	}

	private class ViewHolder {
		public ImageView headImg;
		public TextView text;
	}
}
