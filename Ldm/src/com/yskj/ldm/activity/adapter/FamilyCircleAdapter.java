package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;
import com.yskj.ldm.beans.CircleBean;


public class FamilyCircleAdapter  extends BaseAdapter {

	Context context;

	private LayoutInflater inflater;

	List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();

	public List<HashMap<String, Object>> recordlist = new ArrayList<HashMap<String, Object>>();
	private boolean isChice[];
	public FamilyCircleAdapter(Context context)
	{
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	public void setDataSource(List<HashMap<String, Object>> items)
	{
		list = items;
		isChice=new boolean[list.size()];
		for (int i = 0; i < list.size(); i++) {
			isChice[i]=false;
		}
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {

		return list==null?0:list.size();
	}

	@Override
	public Object getItem(int position) {

		return list==null?null:list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder= null;
		if(convertView == null){
			holder = new ViewHolder();
			convertView =  (RelativeLayout)inflater.inflate(R.layout.familycircle_item, null);
			holder.t_title =(TextView)convertView.findViewById(R.id.family_title);
			holder.t_num =(TextView)convertView.findViewById(R.id.family_people_num);
			holder.selectIcon = (ImageView)convertView.findViewById(R.id.family_select);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		if(isChice[position])
		{
			holder.selectIcon.setImageResource(R.drawable.choice_icon);
		}
		else
		{
			holder.selectIcon.setImageResource(R.drawable.not_choice_icon);
		}
		holder.t_title.setText(((HashMap<String, Object>)list.get(position)).get("circle_name").toString()
				+"("+((HashMap<String, Object>)list.get(position)).get("circle_num").toString()+")");
		//holder.t_num.setText(((HashMap<String, Object>)list.get(position)).get("user_count").toString());
		return convertView;
	}
	public void chiceState(int post)
	{
		//		isChice[post]=isChice[post]==true?false:true;
		if(isChice[post]==true)
		{
			isChice[post] = false;
		
			recordlist.remove(list.get(post));
		}
		else
		{
			isChice[post] = true;
 
			list.get(post).put("position", post);
			recordlist.add(list.get(post));
		}
		this.notifyDataSetInvalidated();
	}
	private class ViewHolder {
		public TextView t_title;
		public TextView t_num;
		public ImageView selectIcon;
	}
}
