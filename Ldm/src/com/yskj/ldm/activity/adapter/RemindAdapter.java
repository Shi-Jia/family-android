package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yskj.ldm.R;
 
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;

public class RemindAdapter extends BaseAdapter   implements IDrawableCallback{

	Context context;
	
	private LayoutInflater inflater;

	List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
	
	public RemindAdapter(Context context)
	{
		this.context = context;
		inflater = LayoutInflater.from(context);
	}
	
	public void setDataSource(ArrayList<HashMap<String, Object>> items)
	{
		list = items;
		Log.i("list", "  items -- > "+items.toString());
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list==null?0:list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list==null?null:list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder= null;
		if(convertView == null){
			holder = new ViewHolder();
			convertView =  (RelativeLayout)inflater.inflate(R.layout.remind_item, null);
			holder.t_date = (TextView)convertView.findViewById(R.id.remind_date);
			holder.t_time = (TextView)convertView.findViewById(R.id.remind_time);
			holder.t_content = (TextView)convertView.findViewById(R.id.remind_content);
			holder.t_relationship = (TextView)convertView.findViewById(R.id.remind_relationship);
			holder.t_circle  = (TextView)convertView.findViewById(R.id.remind_circle);
			

			holder.image_choise = (ImageView)convertView.findViewById(R.id.family_choise);
			
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		Log.i("list", "  holder.t_date -->  "+((HashMap<String,Object>)list.get(position)).get("date").toString());
		Log.i("list", "  holder.t_time -->  "+((HashMap<String,Object>)list.get(position)).get("time").toString());
		Log.i("list", "  holder.t_content -->  "+((HashMap<String,Object>)list.get(position)).get("content").toString());
		Log.i("list", "  holder.t_relationship -->  "+((HashMap<String,Object>)list.get(position)).get("relationship").toString());
		Log.i("list", "  holder.t_circle -->  "+((HashMap<String,Object>)list.get(position)).get("circle").toString());
		holder.t_circle .setText(((HashMap<String,Object>)list.get(position)).get("circle").toString());

		holder.t_relationship.setText(((HashMap<String,Object>)list.get(position)).get("relationship").toString());
 
		holder.t_content.setText(((HashMap<String,Object>)list.get(position)).get("content").toString());
		
		holder.t_time.setText(((HashMap<String,Object>)list.get(position)).get("time").toString());
		
		holder.t_date.setText(((HashMap<String,Object>)list.get(position)).get("date").toString());
		
		
		
		return convertView;
	}

	@Override
	public void onCallback(BitmapItem item) {
		// TODO Auto-generated method stub

	}
	private class ViewHolder {

 

		public TextView t_date;
		
		public TextView t_time;
		
		public TextView t_circle;

		public TextView t_relationship;

		public TextView t_content;

		public ImageView image_choise;

	}
}