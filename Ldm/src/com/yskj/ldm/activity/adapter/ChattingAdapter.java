package com.yskj.ldm.activity.adapter;
import java.util.Map;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ChattingAdapter extends PageBaseAdapter{

	String userId ;
	public ChattingAdapter(Context c) 
	{
		super(c);
		this.userId = PreferencesService.getInstance(c).getUser_id();
	}

	@Override
	public View getView(int positon, View convertView, ViewGroup arg2) {
		ViewHolder holder= null;
		Map<String, Object> listMap = this.list.get(positon);
		if(convertView == null)
		{
			holder = new ViewHolder();

			if(listMap.get("user_id").toString().equals(this.userId))
			{
				convertView = inflater.inflate(R.layout.chatting_left_item, null);
				holder.content = (TextView)convertView.findViewById(R.id.chat_left_tv);
				holder.head = (ImageView)convertView.findViewById(R.id.chat_left_head);
			}
			else
			{
				convertView = inflater.inflate(R.layout.chatting_right_item, null);
				holder.content = (TextView)convertView.findViewById(R.id.chat_right_tv);
				holder.head = (ImageView)convertView.findViewById(R.id.chat_right_head);
			}
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}

		holder.content.setText(listMap.get("message_content").toString());
		if(listMap.get("user_id").toString().equals(this.userId))
			ImageLoader.getInstance().displayImage(listMap.get("user_headporttait").toString(), holder.head, options_head);
		else
			ImageLoader.getInstance().displayImage(listMap.get("belong_user_nikename").toString(), holder.head, options_head);
		//		holder.albuminstructions.setText(albumbean.get(position).get("album_instructions").toString());
		return convertView;
	}
	private class ViewHolder {
		public TextView content;
		public ImageView head;
		public TextView time;
	}
}
