package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;

import com.yskj.ldm.R;
import com.yskj.ldm.beans.ContactElement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

 

public class MyContactAdapter extends BaseAdapter {

	private ArrayList<ContactElement> contactArr;
	private LayoutInflater inflater;

	
	public MyContactAdapter(Context context) {

		this.inflater = LayoutInflater.from(context);
	}

	public void setDataSource(ArrayList<ContactElement> arr){
		contactArr = arr;
		this.notifyDataSetChanged();
	}
	
	public ArrayList<ContactElement> getDataSource(){
		return contactArr;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contactArr==null?0:contactArr.size();
	}

	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return contactArr==null?null:contactArr.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		return 0;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.contact_list_item2, null);
			holder = new ViewHolder();
			
			holder.name = (TextView) convertView.findViewById(R.id.contact_name);
			holder.checked = (CheckBox) convertView.findViewById(R.id.contact_item_selected);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		ContactElement element = contactArr.get(position);
		
		holder.name.setText(element.getName()+"  ( "+element.getPhoneNum()+" )");
		holder.checked.setChecked(element.checked);

		return convertView;
	}

	class ViewHolder {
		
		TextView name;
		CheckBox checked;
	}


}
