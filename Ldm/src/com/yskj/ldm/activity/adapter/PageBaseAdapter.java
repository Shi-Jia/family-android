package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yskj.ldm.R;
 
 

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class  PageBaseAdapter  extends BaseAdapter{

	//是不是第一次取数据;
	protected boolean isfirst = false;

	protected LayoutInflater inflater;

	protected List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

	protected Context c;
	
	protected DisplayImageOptions options_head; //配置图片加载及显示选项
	protected DisplayImageOptions options_content; //配置图片加载及显示选项
 
	//是不是正在加载
	public boolean isloading = false;
	
	public Integer param =0;
 
	public PageBaseAdapter(Context c)
	{
		this.c = c;
		inflater = LayoutInflater.from(c);
		options_head  = new DisplayImageOptions.Builder()
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.showStubImage(R.drawable.head_picture_default)    //在ImageView加载过程中显示图片
		.showImageForEmptyUri(R.drawable.head_picture_default)  //image连接地址为空时
		.showImageOnFail(R.drawable.head_picture_default)  //image加载失败
		.cacheInMemory(true)  //加载图片时会在内存中加载缓存
		.cacheOnDisc(true)   //加载图片时会在磁盘中加载缓存
		.build();
		options_content = new DisplayImageOptions.Builder()
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.showStubImage(R.drawable.loading_pic)    //在ImageView加载过程中显示图片
		.showImageForEmptyUri(R.drawable.loading_pic)  //image连接地址为空时
		.showImageOnFail(R.drawable.loading_pic)  //image加载失败
		.cacheInMemory(true)  //加载图片时会在内存中加载缓存
		.cacheOnDisc(true)   //加载图片时会在磁盘中加载缓存
		.build();
	}
	public List<Map<String, Object>> getList()
	{
		return this.list;
	}
 
	protected void setDataSource(ArrayList<HashMap<String, Object>> items)
	{
		combine(items);
		this.notifyDataSetChanged();
	}

	public void addNetworkItems(List<HashMap<String, Object>>items,Integer param){
 
		this.isloading = false;
		this.param = param;

		if(param ==0){
			clear();
		}

		this.combine(items);

		this.notifyDataSetChanged();
	}
	protected void clear(){

		if(list != null){
			list.clear();
		}
	}

	protected void combine(List<HashMap<String, Object>> items)
	{
		if(items  == null){
			return;
		}
		for(HashMap<String, Object> map:items){
			list.add(map);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list==null?0:list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list==null?null:list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
 
}
