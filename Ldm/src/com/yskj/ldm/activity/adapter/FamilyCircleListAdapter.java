package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
import com.yskj.ldm.R.color;
import com.yskj.ldm.R.drawable;
import com.yskj.ldm.activity.impl.FamilyOnClickListener;
import com.yskj.ldm.activity.ui.TouchImageActivity;
import com.yskj.ldm.beans.CommentsBean;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FamilyCircleListAdapter extends PageBaseAdapter{

	private FamilyOnClickListener  commentsListener;
	private HashMap<Integer,List<CommentsBean>> commentList;
	public FamilyCircleListAdapter(Context c) {
		super(c);
		commentList = new HashMap<Integer,List<CommentsBean>>();
	}
	public void setCommentsListener(FamilyOnClickListener  commentsListener)
	{
		this.commentsListener =  commentsListener;

	}
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder= null;
		if (convertView==null) {  
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.familycirclelist_item, null);
			holder.userHead = (ImageView)convertView.findViewById(R.id.family_user_head);

			holder.userName = (TextView)convertView.findViewById(R.id.family_user_name);

			holder.sendTime = (TextView)convertView.findViewById(R.id.family_sendtime);

			holder.sendContent = (TextView)convertView.findViewById(R.id.family_send_content);
			holder.familyCommentsTxt  = (TextView)convertView.findViewById(R.id.family_comments_txt);
			//			holder.gv = (GridView)convertView.findViewById(R.id.moon_list_grid);
			holder.photoContent = (ImageView)convertView.findViewById(R.id.family_share_photo);
			holder.commentsIcon = (ImageView)convertView.findViewById(R.id.family_comments);

			holder.commentsLayout = (LinearLayout)convertView.findViewById(R.id.family_user_comments);

			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		if(list.size()==0)
			return convertView ;
		holder.userName.setText(((Map<String,Object>)list.get(position)).get("title").toString());
		holder.sendTime.setText(((Map<String,Object>)list.get(position)).get("upload_time").toString());
		String content = ((Map<String,Object>)list.get(position)).get("content").toString();
		if(!StringUtil.isNull(content))
		{
			holder.sendContent.setVisibility(View.VISIBLE);
			holder.sendContent.setText(content);
		}
		else
		{
			holder.sendContent.setVisibility(View.GONE);
		}

		ImageLoader.getInstance().displayImage(((Map<String, Object>)list.get(position)).get("user_headporttait").toString(), holder.userHead,this.options_head);
		holder.commentsLayout.removeAllViews();
		List<CommentsBean> cb = commentList.get(position);
		if(null!=cb)
		{
			for(int j = 0;j<cb.size();j++)
			{
				LinearLayout ll = new LinearLayout(this.c);
				ll.setOrientation(LinearLayout.HORIZONTAL);  
				TextView tvname = new TextView(this.c);
				tvname.setText(cb.get(j).getName()+": ");
				tvname.setTextSize(17);
				tvname.setTextColor(color.blue);
				TextView tvcontent = new TextView(this.c);
				tvcontent.setText(cb.get(j).getContent());
				tvcontent.setTextSize(17);
				ll.addView(tvname);
				ll.addView(tvcontent);
				ll.setPadding(20, 0, 0, 0);
				holder.commentsLayout.addView(ll);
			}
		}

		final  List<HashMap<String, Object>> filebean =	JsonUtil.getList(((Map<String,Object>)list.get(position)).get("attachs").toString());
		if(filebean.size()>0)
		{
			holder.photoContent.setVisibility(View.VISIBLE);
			ImageLoader.getInstance().displayImage(((Map<String, Object>)filebean.get(0)).get("downurl").toString(), holder.photoContent,this.options_content);
			holder.photoContent.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent();
					intent.setClass(c, TouchImageActivity.class);
					Session.getSession().put("images",filebean);
					Session.getSession().put("postion", 0);
					c.startActivity(intent);	
				}});
		}
		else
		{
			holder.photoContent.setVisibility(View.GONE);
		}

		final  Integer contentId = Integer.parseInt( ((Map<String,Object>)list.get(position)).get("content_id").toString());
		final  int mypostion = position;
		holder.familyCommentsTxt.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) 
			{
				commentsListener.onComments(contentId, mypostion);
			}
		});

		holder.commentsIcon.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) 
			{
				commentsListener.onComments(contentId, mypostion);
			}
		});
		return convertView;
	}
	public void add(int positon ,List<CommentsBean> recordCommentsString) {  

		List<CommentsBean> addData = new ArrayList<CommentsBean>();
		for(int i =0;i<recordCommentsString.size();i++)
		{
//			Log.i("log", positon+"  <<-----recordCommentsString");
//			Log.i("log", recordCommentsString.get(i).getPostion()  +"  <<-----recordCommentsString");
			if(recordCommentsString.get(i).getPostion().equals(positon))
			{
				addData.add(recordCommentsString.get(i));
			}
		}
//		Log.i("log", addData.size() +"  <<-----recordCommentsString");
//		Log.i("log",positon  +"  <<-----recordCommentsString");
		commentList.put(positon, addData);
	}  
	private class ViewHolder {

		public ImageView userHead;

		public TextView sendTime;

		public TextView userName;

		public TextView sendContent;

		//		public GridView gv;

		public ImageView photoContent;

		public ImageView commentsIcon;

		public LinearLayout commentsLayout;

		public LinearLayout likesLayout;

		public TextView familyCommentsTxt;

	}

}
