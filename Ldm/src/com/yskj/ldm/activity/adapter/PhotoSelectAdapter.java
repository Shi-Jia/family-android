package com.yskj.ldm.activity.adapter;

 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
 
 





import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;




import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;
import com.yskj.ldm.activity.cache.ImageCacheManager;
import com.yskj.ldm.beans.FileBean;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


public class PhotoSelectAdapter  extends BaseAdapter  implements IDrawableCallback{
	private LayoutInflater inflater;
	private DisplayImageOptions options; //配置图片加载及显示选项
	private ImageLoader imageLoader = ImageLoader.getInstance();
	private ArrayList<FileBean> filebean = null;
	HashMap<Integer,String > record;
	private boolean isChice[];
	ArrayList<Object> recordFileBean = null;
	public PhotoSelectAdapter(Context c){
		inflater = LayoutInflater.from(c);
		//配置图片加载及显示选项（还有一些其他的配置，查阅doc文档吧）
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.head_photo)    //在ImageView加载过程中显示图片
		.showImageForEmptyUri(R.drawable.head_photo)  //image连接地址为空时
		.showImageOnFail(R.drawable.head_photo)  //image加载失败
		.cacheInMemory(true)  //加载图片时会在内存中加载缓存
		.cacheOnDisc(true)   //加载图片时会在磁盘中加载缓存
		//			.displayer(new RoundedBitmapDisplayer(20))  //设置用户加载图片task(这里是圆角图片显示)
		.build();
	}
	/**
	 * 设置数据源
	 * @param arr
	 */
	public void setDataSource(ArrayList<FileBean> filebean){

		this.filebean = filebean;
		isChice=new boolean[filebean.size()];
		for (int i = 0; i < filebean.size(); i++) {
			isChice[i]=false;
		}
		this.notifyDataSetChanged();
	}


	public ArrayList<Object> getRecordFileBean() {
		return recordFileBean;
	}
	public void setRecordFileBean(ArrayList<Object> recordFileBean) {
		this.recordFileBean = recordFileBean;
	}
	@Override
	public int getCount() {
 
		return filebean==null?0:filebean.size();
	}

	@Override
	public Object getItem(int position) {
	 
		return filebean==null?null:filebean.get(position);
	}

	@Override
	public long getItemId(int position) {
 
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder= null;
		if(convertView == null){
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.photoselect_item, parent,false);
			holder.selectIcon = (ImageView)convertView.findViewById(R.id.photoselect_choice_normal);
			holder.systempic = (ImageView)convertView.findViewById(R.id.photoselect_item_img);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}
		if(isChice[position])
		{
			holder.selectIcon.setImageResource(R.drawable.choice_press);
		}
		else
		{
			holder.selectIcon.setImageResource(R.drawable.choice_normal);
		}
		imageLoader.displayImage(filebean.get(position).getFileUri(), holder.systempic,options);
		return convertView;
	}



	private class ViewHolder {
		public ImageView systempic;
		public ImageView selectIcon;
	}

	public void chiceState(int post)
	{
		//		isChice[post]=isChice[post]==true?false:true;
		if(isChice[post]==true)
		{
			isChice[post] = false;
			recordFileBean.remove(this.filebean.get(post));
		}
		else
		{
			isChice[post] = true;
			recordFileBean.add(this.filebean.get(post));
		}
		this.notifyDataSetInvalidated();
	}


	@Override
	public void onCallback(BitmapItem item) {
		FileBean fb = (FileBean)item.getObj();
		fb.setView(item.getView());
		fb.setUrl(item.getUrl());
		Log.i("onCallback", " onCallback  --> " +fb.getUrl());
		Log.i("onCallback", " onCallback1  --> " +item.getView());
		this.notifyDataSetChanged();

	}
}
