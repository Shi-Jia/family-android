package com.yskj.ldm.activity.adapter;

import java.util.Map;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.android.commu.net.ICommuDataListener;
import com.yskj.ldm.R;
import com.yskj.ldm.R.color;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.util.StringUtil;


public class MassageBoxAdapter  extends PageBaseAdapter   {


	private ICommuDataListener listener;

	public MassageBoxAdapter(Context c,ICommuDataListener listener) {
		super(c);
		this.listener = listener;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder= null;
		if(convertView == null){

			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.messagebox_item, null);
			holder.content = (TextView)convertView.findViewById(R.id.message_name);
			//			holder.selectIcon.setVisibility(View.INVISIBLE);
			holder.name = (TextView)convertView.findViewById(R.id.message_content);
			holder.ok = (Button)convertView.findViewById(R.id.message_ok);
			holder.cancel = (Button)convertView.findViewById(R.id.message_cancel);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}
		Map<String, Object> resList =  (Map<String, Object>)this.list.get(position);

		String remarks = resList.get("extparams").toString();
		if(StringUtil.isNull(remarks)) {
			remarks = "0";
		}
		final String circle_num  =  remarks;
		holder.content.setText(((Map<String, Object>)this.list.get(position)).get("message_title").toString());
		holder.name.setText(((Map<String, Object>)this.list.get(position)).get("message_content").toString()+" "+((Map<String, Object>)this.list.get(position)).get("message_sendtime").toString());
//		String messageType = resList.get("message_type").toString();
//		if(messageType.equals(Command.Message_User_Validation))
//		{
//			holder.ok.setVisibility(View.VISIBLE);
//			holder.cancel.setVisibility(View.VISIBLE);
//			if(resList.get("message_state").toString().equals("3"))
//			{
//				holder.ok.setVisibility(View.GONE);
//				holder.cancel.setText("已添加");
//				holder.cancel.setTextColor(color.commonly_used_gray);
//				holder.cancel.setEnabled(false);
//			}
//			else if (resList.get("message_state").toString().equals("4"))
//			{
//				holder.ok.setVisibility(View.GONE);
//				holder.cancel.setText("已拒绝");
//				holder.cancel.setTextColor(color.commonly_used_gray);
//				holder.cancel.setEnabled(false);
//			}
//			else
//			{
//				holder.ok.setOnClickListener(new OnClickListener(){
//					public void onClick(View arg0) {
//
//						ConnectionManager.getInstance().requestUserApplyCricleResponse(Integer.parseInt(resList.get("message_id").toString()), 
//								3, circle_num, true, listener);		
//					}
//				});
//				holder.cancel.setOnClickListener(new OnClickListener(){
//
//					public void onClick(View arg0) {
//						ConnectionManager.getInstance().requestUserApplyCricleResponse(Integer.parseInt(resList.get("message_id").toString()), 
//								4, circle_num, true, listener);		
//					}
//				});
//			}
//		}
		//		else if(messageType.equals(Command.Message_Moon_Comments))
		//		{
		//			
		//		}

		return convertView;
	}



	private class ViewHolder {
		public TextView name;
		public TextView content;
		public Button ok;
		public Button cancel;
	}

}
