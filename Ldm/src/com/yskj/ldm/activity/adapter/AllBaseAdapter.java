package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
 
import java.util.List;
import java.util.HashMap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.yskj.ldm.R;
 
 

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public  class AllBaseAdapter  extends BaseAdapter{
 

	protected LayoutInflater inflater;

	protected List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();

	protected Context c;
	
	protected DisplayImageOptions options; //配置图片加载及显示选项
 
 
	protected AllBaseAdapter(Context c)
	{
		this.c = c;
		inflater = LayoutInflater.from(c);
	 	options = new DisplayImageOptions.Builder()
		.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
		.showStubImage(R.drawable.head_photo)    //在ImageView加载过程中显示图片
		.showImageForEmptyUri(R.drawable.head_photo)  //image连接地址为空时
		.showImageOnFail(R.drawable.head_photo)  //image加载失败
		.cacheInMemory(true)  //加载图片时会在内存中加载缓存
		.cacheOnDisc(true)   //加载图片时会在磁盘中加载缓存
		.build();
	}
	public List<HashMap<String, Object>> getList()
	{
		return this.list;
	}
 
	public void setDataSource(List<HashMap<String, Object>> items)
	{
	    this.list = items;
		this.notifyDataSetChanged();
	}

 
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list==null?0:list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list==null?null:list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
 
		return this.creatorView(position, convertView, parent);
	}
	protected View creatorView(int position, View convertView, ViewGroup parent)
	{
		return null;
	}
}
