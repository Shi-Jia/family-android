package com.yskj.ldm.activity.adapter;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;






















import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;
import com.yskj.ldm.activity.impl.FamilyOnClickListener;
import com.yskj.ldm.activity.ui.DisplayImagePagerActivity;
import com.yskj.ldm.activity.ui.LaodaoQuanListActivity;
import com.yskj.ldm.activity.ui.TouchImageActivity;
import com.yskj.ldm.beans.CommentsBean;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;


public class LaoDaoQuanListAdapter1 extends BaseAdapter implements IDrawableCallback{



	//是不是第一次取数据;
	public boolean isfirst = false;


	//	HashMap<Integer,View> lmap = new HashMap<Integer,View>();
	private LayoutInflater inflater;

	private List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

	private Context c;

	private HashMap<Integer,View> recordComments = new HashMap<Integer,View>();
	//是不是正在加载
	public boolean isloading = false;
	public Integer param =0 ;


	FamilyOnClickListener  layoutflash;
	public LaoDaoQuanListAdapter1(Context c)
	{
		this.c = c;
		inflater = LayoutInflater.from(c);

	}
	public void setRecordComments(HashMap<Integer,View> recordComments)
	{
		this.recordComments = recordComments;
	}
	public void setReflash(FamilyOnClickListener  layoutflash)
	{
		this.layoutflash =  layoutflash;
	}
	public void setDataSource(ArrayList<HashMap<String, Object>> items)
	{
		combine(items);
		this.notifyDataSetChanged();
	}

	public void addNetworkItems(List<HashMap<String, Object>>items,Integer param){

		//		this.isfirst = true;
		this.isloading = false;
		this.param = param;

		if(param ==0){
			clear();
		}

		this.combine(items);

		this.notifyDataSetChanged();
	}
	public void clear(){

		if(list != null){
			list.clear();
		}
	}

	private void combine(List<HashMap<String, Object>> items){

		if(items  == null){
			return;
		}

		for(HashMap<String, Object> map:items){
			list.add(map);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list==null?0:list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list==null?null:list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		ViewHolder holder= null;
		if (convertView==null) {  
			holder = new ViewHolder();
			convertView =  (LinearLayout)inflater.inflate(R.layout.laodaoquan_item, null);
			holder.t_title = (TextView)convertView.findViewById(R.id.q_name);

			holder.t_time = (TextView)convertView.findViewById(R.id.q_time);

			holder.t_content = (TextView)convertView.findViewById(R.id.q_content);

			holder.gv = (GridView)convertView.findViewById(R.id.moon_list_grid);
			holder.pinglunIcon = (ImageView)convertView.findViewById(R.id.pinglun_icon);

			holder.pinglunLayout = (LinearLayout)convertView.findViewById(R.id.pinglun);
			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.t_title.setText(((Map<String,Object>)list.get(position)).get("title").toString());
		holder.t_time.setText(((Map<String,Object>)list.get(position)).get("upload_time").toString());
		holder.t_content.setText(((Map<String,Object>)list.get(position)).get("content").toString());
		if(null!=(LinearLayout)recordComments.get(position))
		{
			LinearLayout ll = (LinearLayout)recordComments.get(position);
			for(int i = 0;i<ll.getChildCount();i++)
			{
				Log.i("adapter", " position = "+position +"   content = "+((Map<String,Object>)list.get(position)).get("title").toString()) ;
				Log.i("adapter", " position = "+position +"   content = "+((TextView)ll.getChildAt(i)).getText().toString()) ;
			}
			if(null != ll.getParent())
				((ViewGroup)ll.getParent()).removeView(ll);
			holder.pinglunLayout.removeAllViews();
			holder.pinglunLayout.addView(ll);
		}
		DownloadMoonListAdapter spa = new DownloadMoonListAdapter(this.c);
		spa.setDataSource(JsonUtil.getList(((Map<String,Object>)list.get(position)).get("attachs").toString()));
		final  List<HashMap<String, Object>> filebean =  spa.getDataSource();
		holder.gv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				Intent intent = new Intent();
				intent.setClass(c, TouchImageActivity.class);
				Session.getSession().put("images",filebean);
				Session.getSession().put("postion", position);
				c.startActivity(intent);	
			}
		});
		holder.gv.setAdapter(spa);
		final  int mypostion = position;
		holder.pinglunIcon.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) 
			{
				layoutflash.onComments(null, mypostion);
			}
		});
		return convertView;
	}
	@Override

	public void unregisterDataSetObserver(DataSetObserver observer) {
		if(observer != null){
			super.unregisterDataSetObserver(observer);
		}
	}

	public void add(int positon ,List<CommentsBean> recordCommentsString) {  

		LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.comment_item, null);

		layout.setOrientation(LinearLayout.VERTICAL);  

		for(int i =0;i<recordCommentsString.size();i++)
		{
			if(recordCommentsString.get(i).getPostion().equals(positon))
			{
				TextView tvcontent = new TextView(this.c);
//				Log.i("positon ", " positon  --->"+positon+"  content --->  "+recordCommentsString.get(i).getContent());
				tvcontent.setText(recordCommentsString.get(i).getContent());
				layout.addView(tvcontent);
			}
		}
		this.recordComments.put( positon, layout);
		this.notifyDataSetChanged();

	}  
	@Override
	public void onCallback(BitmapItem item) {


	}
	private class ViewHolder {

		public ImageView mImage;

		public TextView t_time;

		public TextView t_title;

		public TextView t_content;

		public GridView gv;

		public ImageView pinglunIcon;
		public LinearLayout pinglunLayout;

	}
}

