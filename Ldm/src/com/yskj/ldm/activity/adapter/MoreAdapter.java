package com.yskj.ldm.activity.adapter;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;

import com.yskj.ldm.activity.cache.RecordsPath;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class MoreAdapter extends  AllBaseAdapter{

	public MoreAdapter(Context c) {
		super(c);
	}
	protected View creatorView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder= null;
		if(convertView == null)
		{
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.more_item, null);
			holder.contentImg = (ImageView)convertView.findViewById(R.id.more_item_img);
			holder.text = (TextView)convertView.findViewById(R.id.more_item_txt);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.text.setText(this.list.get(position).get("bucket_name").toString());
		ImageLoader.getInstance().displayImage(RecordsPath.UriDrawable+this.list.get(position).get("image_path").toString(), holder.contentImg,this.options);
		return convertView;
	}

	private class ViewHolder {
		public ImageView contentImg;
		public TextView text;
	}
}
