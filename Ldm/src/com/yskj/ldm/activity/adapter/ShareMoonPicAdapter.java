package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;
import com.yskj.ldm.beans.FileBean;

public class ShareMoonPicAdapter  extends BaseAdapter  implements IDrawableCallback{


	private LayoutInflater inflater;
//	private DisplayImageOptions options; //配置图片加载及显示选项
 
	private  ImageLoader imageLoader = ImageLoader.getInstance();
	private ArrayList<FileBean> filebean = new ArrayList<FileBean>();
 
	public ShareMoonPicAdapter(Context c){
		inflater = LayoutInflater.from(c);
	}
	 
	/**
	 * 设置数据源
	 * @param arr
	 */
	public void setDataSource(ArrayList<FileBean> filebean){

		this.filebean = filebean;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return filebean==null?0:filebean.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return filebean==null?null:filebean.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder= null;
		if(convertView == null){

			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.photoselect_item, parent,false);
			holder.selectIcon = (ImageView)convertView.findViewById(R.id.photoselect_choice_normal);
			holder.selectIcon.setVisibility(View.INVISIBLE);
			holder.systempic = (ImageView)convertView.findViewById(R.id.photoselect_item_img);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}
 
		imageLoader.displayImage(filebean.get(position).getFileUri(), holder.systempic);

		return convertView;
	}



	private class ViewHolder {
		public ImageView systempic;
		public ImageView selectIcon;
	}



	@Override
	public void onCallback(BitmapItem item) {
		FileBean fb = (FileBean)item.getObj();
		fb.setView(item.getView());
		fb.setUrl(item.getUrl());
		Log.i("onCallback", " onCallback  --> " +fb.getUrl());
		Log.i("onCallback", " onCallback1  --> " +item.getView());
		this.notifyDataSetChanged();

	}
}
