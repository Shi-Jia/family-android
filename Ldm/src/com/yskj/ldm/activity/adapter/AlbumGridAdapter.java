package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
 
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;
import com.yskj.ldm.beans.AlbumBean;
import com.yskj.ldm.beans.FileBean;

public class AlbumGridAdapter  extends BaseAdapter  implements IDrawableCallback{
	private LayoutInflater inflater;
	private DisplayImageOptions options; //配置图片加载及显示选项
	private ImageLoader imageLoader = ImageLoader.getInstance();
	private List<HashMap<String, Object>> albumbean = new ArrayList<HashMap<String, Object>>();
	//是不是正在加载
	public boolean isloading = false;
	public Integer param =0 ;
 
	public AlbumGridAdapter(Context c){
		inflater = LayoutInflater.from(c);
		//配置图片加载及显示选项（还有一些其他的配置，查阅doc文档吧）
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.head_photo)    //在ImageView加载过程中显示图片
		.showImageForEmptyUri(R.drawable.head_photo)  //image连接地址为空时
		.showImageOnFail(R.drawable.head_photo)  //image加载失败
		.cacheInMemory(true)  //加载图片时会在内存中加载缓存
		.cacheOnDisc(true)   //加载图片时会在磁盘中加载缓存
		//			.displayer(new RoundedBitmapDisplayer(20))  //设置用户加载图片task(这里是圆角图片显示)
		.build();
	}
	public void addNetworkItems(List<HashMap<String, Object>>items,Integer param){
		 
		this.isloading = false;
		this.param = param;

		if(param ==0){
			clear();
		}

		this.combine(items);

		this.notifyDataSetChanged();
	}
	public void clear(){

		if(albumbean != null){
			albumbean.clear();
		}
	}
	private void combine(List<HashMap<String, Object>> items){

		if(items  == null){
			return;
		}

		for(HashMap<String, Object> map:items){
			albumbean.add(map);
		}
	}
	/**
	 * 设置数据源
	 * @param arr
	 */
	public void setDataSource(List<HashMap<String, Object>> list){

		combine(list);
		this.notifyDataSetChanged();
	}


	@Override
	public int getCount() {

		return albumbean==null?0:albumbean.size();
	}

	@Override
	public Object getItem(int position) {

		return albumbean==null?null:albumbean.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder= null;
		if(convertView == null)
		{
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.album_item, parent,false);
			holder.albumtitle = (TextView)convertView.findViewById(R.id.album_title);
			holder.systempic = (ImageView)convertView.findViewById(R.id.photoselect_item_img);
			holder.albuminstructions = (TextView)convertView.findViewById(R.id.album_instructions);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}

		holder.albumtitle.setText(albumbean.get(position).get("album_name").toString());
		imageLoader.displayImage(albumbean.get(position).get("album_path").toString(), holder.systempic,options);
		holder.albuminstructions.setText(albumbean.get(position).get("album_instructions").toString());
		return convertView;
	 
	}



	private class ViewHolder {
		public TextView albumtitle;
		public ImageView systempic;
		public TextView albuminstructions;
	}
 

	@Override
	public void onCallback(BitmapItem item) {
		//		FileBean fb = (FileBean)item.getObj();
		//		fb.setView(item.getView());
		//		fb.setUrl(item.getUrl());
		//		Log.i("onCallback", " onCallback  --> " +fb.getUrl());
		//		Log.i("onCallback", " onCallback1  --> " +item.getView());
		//		this.notifyDataSetChanged();
	}
}
