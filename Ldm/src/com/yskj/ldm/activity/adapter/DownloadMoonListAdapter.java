package com.yskj.ldm.activity.adapter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;









import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.download.HttpClientImageDownloader;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;
import com.yskj.ldm.beans.FileBean;

public class DownloadMoonListAdapter  extends BaseAdapter  implements IDrawableCallback{


	private LayoutInflater inflater;
	private DisplayImageOptions options; //配置图片加载及显示选项
	//	private ImageCacheManager imgManager = ImageCacheManager.getInstance();

	private List<HashMap<String, Object>> filebean = new ArrayList<HashMap<String, Object>>();

	public DownloadMoonListAdapter(Context c){
		inflater = LayoutInflater.from(c);

		options = new DisplayImageOptions.Builder()
		.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
		.showStubImage(R.drawable.head_photo)    //在ImageView加载过程中显示图片
		.showImageForEmptyUri(R.drawable.head_photo)  //image连接地址为空时
		.showImageOnFail(R.drawable.head_photo)  //image加载失败
		.cacheInMemory(true)  //加载图片时会在内存中加载缓存
		.cacheOnDisc(true)   //加载图片时会在磁盘中加载缓存
		.build();
//		initImageLoaderParams( c);
	}
	public void initImageLoaderParams(Context c)
	{
		HttpParams params = new BasicHttpParams();
		// Turn off stale checking. Our connections break all the time anyway,
		// and it's not worth it to pay the penalty of checking every time.
		HttpConnectionParams.setStaleCheckingEnabled(params, false);
		// Default connection and socket timeout of 10 seconds. Tweak to taste.
		HttpConnectionParams.setConnectionTimeout(params, 10 * 1000);
		HttpConnectionParams.setSoTimeout(params, 10 * 1000);
		HttpConnectionParams.setSocketBufferSize(params, 8192);

		// Don't handle redirects -- return them to the caller. Our code
		// often wants to re-POST after a redirect, which we must do ourselves.
		HttpClientParams.setRedirecting(params, false);
		// Set the specified user agent and register standard protocols.
		HttpProtocolParams.setUserAgent(params, "some_randome_user_agent");
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

		ClientConnectionManager manager = new ThreadSafeClientConnManager(params, schemeRegistry);


		ImageLoaderConfiguration config =
				new ImageLoaderConfiguration
				.Builder(c)
		.imageDownloader(new BaseImageDownloader(c, 5 * 1000, 30 * 1000)) 
		.build();
		ImageLoader.getInstance().init(config);//全局初始化此配置  
	}

	public List<HashMap<String, Object>> getDataSource ()
	{
		return this.filebean;
	}
	/**
	 * 设置数据源
	 * @param arr
	 */
	public void setDataSource(List<HashMap<String, Object>> filebean){

		this.filebean = filebean;

		this.notifyDataSetChanged();
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return filebean==null?0:filebean.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return filebean==null?null:filebean.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder= null;
		if(convertView == null){

			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.photoselect_item,null);
			holder.selectIcon = (ImageView)convertView.findViewById(R.id.photoselect_choice_normal);
			holder.selectIcon.setVisibility(View.INVISIBLE);
			holder.systempic = (ImageView)convertView.findViewById(R.id.photoselect_item_img);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}
		//		if(null == filebean.get(position).getView())
		//			imgManager.loadDrawable(filebean.get(position).getFilePath(),holder.systempic, true, this);
		//		else
		//			imageLoader.displayImage(filebean.get(position).getUrl(), holder.systempic);
		ImageLoader.getInstance().displayImage(((Map<String, Object>)filebean.get(position)).get("downurl").toString(), holder.systempic,options);

		return convertView;
	}



	private class ViewHolder {
		public ImageView systempic;
		public ImageView selectIcon;
	}

	@Override
	public void onCallback(BitmapItem item) {
		FileBean fb = (FileBean)item.getObj();
		fb.setView(item.getView());
		fb.setUrl(item.getUrl());
		Log.i("onCallback", " onCallback  --> " +fb.getUrl());
		Log.i("onCallback", " onCallback1  --> " +item.getView());
		this.notifyDataSetChanged();
	}
}
