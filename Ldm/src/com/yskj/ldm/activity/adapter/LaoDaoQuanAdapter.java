package com.yskj.ldm.activity.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yskj.ldm.R;
import com.yskj.ldm.R.color;
import com.yskj.ldm.R.drawable;

import com.yskj.ldm.activity.impl.FamilyOnClickListener;
import com.yskj.ldm.activity.ui.TouchImageActivity;
import com.yskj.ldm.beans.CommentsBean;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class LaoDaoQuanAdapter extends PageBaseAdapter{
	private FamilyOnClickListener  layoutflash;
	private HashMap<Integer,List<CommentsBean>> CommentList;
	public LaoDaoQuanAdapter(Context c) {
		super(c);
		CommentList = new HashMap<Integer,List<CommentsBean>>();
	}
	public void setReflash(FamilyOnClickListener  layoutflash)
	{
		this.layoutflash =  layoutflash;
 
	}
	private View creatorView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder= null;
		if (convertView==null) {  
			holder = new ViewHolder();
			convertView =  (LinearLayout)inflater.inflate(R.layout.laodaoquan_item, null);
			holder.t_title = (TextView)convertView.findViewById(R.id.q_name);

			holder.t_time = (TextView)convertView.findViewById(R.id.q_time);

			holder.t_content = (TextView)convertView.findViewById(R.id.q_content);

			holder.gv = (GridView)convertView.findViewById(R.id.moon_list_grid);

			holder.pinglunIcon = (ImageView)convertView.findViewById(R.id.pinglun_icon);

			holder.pinglunLayout = (LinearLayout)convertView.findViewById(R.id.pinglun);

			convertView.setTag(holder);
		}
		else{
			holder = (ViewHolder)convertView.getTag();
		}
		holder.t_title.setText(((Map<String,Object>)list.get(position)).get("title").toString());
		holder.t_time.setText(((Map<String,Object>)list.get(position)).get("upload_time").toString());
		holder.t_content.setText(((Map<String,Object>)list.get(position)).get("content").toString());

		holder.pinglunLayout.removeAllViews();
		List<CommentsBean> cb = CommentList.get(position);
		if(null!=cb)
		{
			for(int j = 0;j<cb.size();j++)
			{
				LinearLayout ll = new LinearLayout(this.c);
				ll.setOrientation(LinearLayout.HORIZONTAL);  
				ll.setBackgroundResource(drawable.add_bg_press);
				TextView tvname = new TextView(this.c);
				tvname.setText(cb.get(j).getName()+": ");
				tvname.setTextColor(color.color_blue);
				TextView tvcontent = new TextView(this.c);
				tvcontent.setText(cb.get(j).getContent());
				ll.addView(tvname);
				ll.addView(tvcontent);
				holder.pinglunLayout.addView(ll);
			}
		}
		DownloadMoonListAdapter spa = new DownloadMoonListAdapter(this.c);
		spa.setDataSource(JsonUtil.getList(((Map<String,Object>)list.get(position)).get("attachs").toString()));
		final  List<HashMap<String, Object>> filebean =  spa.getDataSource();
		holder.gv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				Intent intent = new Intent();
				intent.setClass(c, TouchImageActivity.class);
				Session.getSession().put("images",filebean);
				Session.getSession().put("postion", position);
				c.startActivity(intent);	
			}
		});
		holder.gv.setAdapter(spa);
		final  Integer contentId = Integer.parseInt( ((Map<String,Object>)list.get(position)).get("content_id").toString());
		final  int mypostion = position;
		holder.pinglunIcon.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) 
			{
				layoutflash.onComments(contentId, mypostion);
			}
		});
		return convertView;
	}
	public void add(int positon ,List<CommentsBean> recordCommentsString) {  
		List<CommentsBean> addData = new ArrayList<CommentsBean>();
		for(int i =0;i<recordCommentsString.size();i++)
		{
			if(recordCommentsString.get(i).getPostion().equals(positon))
			{
				addData.add(recordCommentsString.get(i));
			}
		}
		CommentList.put(positon, addData);
	}  
	private class ViewHolder {

		public ImageView mImage;

		public TextView t_time;

		public TextView t_title;

		public TextView t_content;

		public GridView gv;

		public ImageView pinglunIcon;

		public LinearLayout pinglunLayout;

	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
 
		return this.creatorView(position, convertView, parent);
	}
}
