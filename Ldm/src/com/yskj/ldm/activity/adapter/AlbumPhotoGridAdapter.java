package com.yskj.ldm.activity.adapter;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.BitmapItem;
import com.yskj.ldm.activity.cache.IDrawableCallback;

public class AlbumPhotoGridAdapter  extends PageBaseAdapter  implements IDrawableCallback{

	public AlbumPhotoGridAdapter(Context c) {
		super(c);
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder= null;
		if(convertView == null)
		{
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.albumphoto_item, null);
			holder.systempic = (ImageView)convertView.findViewById(R.id.albumphone_item_img);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)convertView.getTag();
		}
		Log.i("album ", "album  ---->  "+this.list.get(position).get("downurl").toString() );
		ImageLoader.getInstance().displayImage(this.list.get(position).get("downurl").toString(), holder.systempic,this.options_content);
		return convertView;
	}

	private class ViewHolder {
		public ImageView systempic;
	}

	@Override
	public void onCallback(BitmapItem item) {
 
	}
}
