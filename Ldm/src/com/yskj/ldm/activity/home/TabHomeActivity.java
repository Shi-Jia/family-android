package com.yskj.ldm.activity.home;
import java.io.File;
import java.util.HashMap;
import java.util.List;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.activity.impl.PushUpdata;
import com.yskj.ldm.activity.ui.*;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.services.BCSPushMessageReceiver;
import com.yskj.ldm.services.DownloadService;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.SDCardStoreManager;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class TabHomeActivity extends NavigationActivity implements PushUpdata{

	Button b_photo_share = null;
	Button b_camera = null;

	Button b_jiaren = null;
	Button b_shebeiguanli = null;

	Button b_tixing = null;

	Button b_laodaoquan = null;
	TextView updataNew = null;
	boolean  isSDcard;
	int versionCode;
	String version_url ;
	File file =  null;
	private static final int TAKE_BIG_PICTURE = 1;
	private static final int LAODAOQUAN_LIST = 2;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.tab_home_screen);
		registerComponent();
	}
	private void registerComponent()
	{
		b_photo_share = (Button)this.findViewById(R.id.b_photo_share);
		b_camera = (Button)this.findViewById(R.id.b_camera);
		b_jiaren = (Button)this.findViewById(R.id.b_jiaren);
		b_shebeiguanli = (Button)this.findViewById(R.id.b_shebeiguanli);
		b_tixing = (Button)this.findViewById(R.id.b_tixing);
		b_laodaoquan = (Button)this.findViewById(R.id.b_laodaoquan);
		updataNew = (TextView)this.findViewById(R.id.laodaoquan_display_new);
		isSDcard = SDCardStoreManager.getInstance().checkSDCardState();
		versionCode = DeviceUtil.getVersionCode(this);

		if(PreferencesService.getInstance(this).getPushCircle()>0)
		{
			updataNew.setVisibility(View.VISIBLE);
		}
		b_tixing.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				TabHomeActivity.this.startActivity(RemindActivity.class);
			}});
		b_camera.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				if(isSDcard){
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					if(null!=file)
						file = null;
					file = SDCardStoreManager.getInstance().getFilePath(SDCardStoreManager.getInstance().getRootPath()+RecordsPath.ImageRoot,System.currentTimeMillis()+".jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); 
					//				intent.setType("image/*");
					//				intent.putExtra("crop", "true");
					//				intent.putExtra("aspectX", 2);
					//				intent.putExtra("aspectY", 1);
					//				intent.putExtra("outputX", 200);
					//				intent.putExtra("outputY", 100);
					//				intent.putExtra("scale", true);
					intent.putExtra("return-data", true);
					intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
					//				intent.putExtra("noFaceDetection", true); // no face detection

					startActivityForResult(intent, TAKE_BIG_PICTURE);  
				}
				else
					TabHomeActivity.this.showToast("亲,请插入SDCard进入拍照");
			}});
		b_photo_share.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				TabHomeActivity.this.startActivity(PhotoShareActivity.class);
			}});
		b_jiaren.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				TabHomeActivity.this.startActivity(FamilyCircleActivity.class);
			}});
		b_shebeiguanli.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
//				TabHomeActivity.this.startActivity(PhotoFrameSettingActivity.class);
				TabHomeActivity.this.startActivity(CommentDetailActivity.class);
			}});
		b_laodaoquan.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				//				TabHomeActivity.this.startActivity(LaodaoQuanListActivity.class);
				//				startActivityForResult(intent, LAODAOQUAN_LIST);  
				TabHomeActivity.this.startActivityForResult(LaodaoQuanListActivity.class, LAODAOQUAN_LIST);
			}
		});
		//是否有版本升级
		if(pservice.getVersion()>versionCode){
			if(!this.pservice.getVersionTag())
				ConnectionManager.getInstance().requestUpdataVesion(versionCode, true, this);
		}
	}
	private void showUpdateDialog(String verName,  String curVerName,
			String descrip) {
		this.showAlertDialog("稍后再说", "更新", "最新版本：v" + verName + "\n当前版本：v" + curVerName
				+ "\n更新说明:" + descrip + "\n是否更为最新版本?","更新提示");
	}

	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{ 
		super.onActivityResult(requestCode, resultCode, data);  
		if (resultCode == Activity.RESULT_OK) { 
			if(requestCode == TAKE_BIG_PICTURE)
			{
				if(null!=file){
					Session.getSession().put("imagepath", file.getPath());
					TabHomeActivity.this.startActivity(CameraPicActivity.class);
				}
			}
		}
		else if(requestCode == LAODAOQUAN_LIST)
		{
			if(PreferencesService.getInstance(this).getPushCircle()==0)
			{
				updataNew.setVisibility(View.GONE);
			}
		}
	}

	private void startDownloadService(String url,String version) {

		Intent intent = new Intent(this, DownloadService.class);
		intent.putExtra("url",url);
		intent.putExtra("version",version);
		this.startService(intent);
		showToast("正在下载...");
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(null ==data||"".equals(data))
		{
			return ;
		}
		Log.i("whb", "data -->  "+data);
		JsonObject jo = JsonUtil.parseJson(data);
		String errorNo = jo.get("errorNo").toString();
		//		String errorInfo = jo.get("errorInfo").toString();
		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.VESION_CHECK.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				HashMap<String, Object> pramasMap = list.get(0);
				if(pservice.getVersion()>versionCode)
				{
					version_url = jo.get("andrVerUrl").toString().replaceAll("\"", "");
					showUpdateDialog(pramasMap.get("versionno").toString(),DeviceUtil.getVersionName(this),pramasMap.get("versiondesc").toString().replaceAll("n", "\n"));
				}
			}
		}
	}
	@Override
	protected void dialogOnCancel() {
		this.pservice.savaVersionTag(true);
		super.dialogOnCancel();
	}
	@Override
	protected void dialogOnSure()
	{
		if(!StringUtil.isNull(version_url))
		{
			startDownloadService(version_url.replaceAll("\"", ""),pservice.getVersion()+"");
		}
		super.dialogOnSure();
	}
	@Override
	public void onUpdataNew(String type) {
		if(type.equals("1"))
		{

		}
		else if(type.equals("3"))
		{
			updataNew.setVisibility(View.VISIBLE);
		}

	}
	@Override
	public void onBindUser(Object o) {
 		
	}
}
