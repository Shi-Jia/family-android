package com.yskj.ldm.activity.home;


import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.impl.PushUpdata;
import com.yskj.ldm.activity.ui.FamilyCircleListActivity;
import com.yskj.ldm.activity.ui.FamilyPeopleActivity;
import com.yskj.ldm.activity.ui.MessageBoxActivity;
import com.yskj.ldm.activity.ui.MoreActivity;
import com.yskj.ldm.activity.ui.MoreGridActivity;
import com.yskj.ldm.activity.ui.SettingActivity;
import com.yskj.ldm.activity.ui.UserInfoActivity;
import com.yskj.ldm.activity.view.AnimTabHost;
import com.yskj.ldm.base.BaseGroupActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.services.BCSPushMessageReceiver;
import com.yskj.ldm.util.DeviceUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;



public class ContainerHomeActivity extends BaseGroupActivity implements OnTabChangeListener,PushUpdata {

	public static AnimTabHost tabhost;

	public static TabHost.TabSpec tabOne;
	public static TabHost.TabSpec tabTwo;
	public static TabHost.TabSpec tabThree;
	public static TabHost.TabSpec tabFour;

	boolean  isSDcard;

	private static final String[]  menus = {"家庭圈","家人","消息","更多"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.home_container_screen);

		registerComponent();
		BCSPushMessageReceiver.setPushUpdata(this);
	}


	private void registerComponent(){

		tabhost = (AnimTabHost) this.findViewById(R.id.tabhost);
		tabhost.setup(getLocalActivityManager());
		tabhost.getTabWidget().setDividerDrawable(null);

		tabOne = tabhost.newTabSpec(menus[0]).setIndicator(getTabView(0))
				.setContent(new Intent(this, FamilyCircleListActivity.class));

		tabTwo = tabhost.newTabSpec(menus[1]).setIndicator(getTabView(1))
				.setContent(new Intent(this, FamilyPeopleActivity.class));

		tabThree = tabhost.newTabSpec(menus[2]).setIndicator(getTabView(2))
				.setContent(new Intent(this, MessageBoxActivity.class));

		tabFour = tabhost.newTabSpec(menus[3]).setIndicator(getTabView(3))
				.setContent(new Intent(this, MoreGridActivity.class));

		tabhost.addTab(tabOne);
		tabhost.addTab(tabTwo);
		tabhost.addTab(tabThree);
		tabhost.addTab(tabFour);
		tabhost.setCurrentTab(0);
		onItemSelect(0);
		tabhost.setOnTabChangedListener(this);
	}



	public static void goToHomeScreen(){
		tabhost.setCurrentTab(0);
	}

	private View getTabView(int paramInt) {

		View localView = this.inflate(R.layout.tab_index_item);

		ImageView tabImg = (ImageView) localView.findViewById(R.id.tab_item_img);
		TextView tabText = (TextView) localView.findViewById(R.id.tab_item_text);

		tabText.setText(menus[paramInt]);

		switch (paramInt)
		{
		case 0:
			tabImg.setImageResource(R.drawable.tab_home_selector);
			break;
		case 1:
			tabImg.setImageResource(R.drawable.tab_mine_selector);
			break;
		case 2:
			tabImg.setImageResource(R.drawable.tab_set_selector);
			break;
		case 3:
			tabImg.setImageResource(R.drawable.tab_more_selector);
			break;
		}
		return localView;
	}


	public void onTabChanged(String tabId) {

		int i = tabhost.getCurrentTab();

		tabhost.getCurrentView();

		this.onItemSelect(i);
	}



	private void onItemSelect(int paramInt) {

		for (int i = 0; i < 4; i++) {

			if (i == paramInt) {

				TextView tv = (TextView) ((RelativeLayout)tabhost
						.getTabWidget().getChildAt(i)).getChildAt(1);
				TextView tv1 = (TextView) ((RelativeLayout)tabhost
						.getTabWidget().getChildAt(i)).getChildAt(2);
				tv1.setVisibility(View.INVISIBLE);

				tv.setTextColor(getResources().getColor(
						R.color.color_blue));
			} else {

				TextView tv = (TextView) ((RelativeLayout)tabhost
						.getTabWidget().getChildAt(i)).getChildAt(1);

				tv.setTextColor(getResources().getColor(
						R.color.color_black));
			}
		}
	}


	@Override
	public void onUpdataNew(String type) {
		Log.i("type", "type  --->>"+type);
		if(type.equals(Command.Message_Moon_Comments))
		{
			TextView tv = (TextView) ((RelativeLayout)tabhost
					.getTabWidget().getChildAt(0)).getChildAt(2);
			tv.setText(PreferencesService.getInstance(this).getPushCircle());
			tv.setVisibility(View.VISIBLE);
		}
		else
		{
			Log.i("type", "TextView  --->>"+type);
			PreferencesService.getInstance(this).savaMessageCount(PreferencesService.getInstance(this).getMessageCount()+1);
			TextView tv = (TextView) ((RelativeLayout)tabhost
					.getTabWidget().getChildAt(2)).getChildAt(2);
			tv.setText(PreferencesService.getInstance(this).getMessageCount());
			tv.setVisibility(View.VISIBLE);
		}
	}


	@Override
	public void onBindUser(Object o) {

	}
}
