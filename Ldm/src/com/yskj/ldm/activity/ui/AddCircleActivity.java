package com.yskj.ldm.activity.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

public class AddCircleActivity extends NavigationActivity{

	private EditText addCirecleName,addCirecleContent;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.add_circle_screen);
		registerHeadComponent();
		setHeadTitle("加入圈子");
		registerComponent();
	}
 
	public void registerComponent()
	{

		addCirecleName =   (EditText)this.findViewById(R.id.add_circle_name);
		addCirecleName.setHint("请输入圈子ID");
		addCirecleContent = (EditText)this.findViewById(R.id.add_circle_introduce);
		addCirecleContent.setHint("请输入验证信息...");
		Button addOk = (Button)this.findViewById(R.id.btn_add_circle);
		addOk.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) {
				if(StringUtil.isNull(addCirecleName.getText().toString()))
				{
					AddCircleActivity.this.showToast("亲,圈子ID不能为空.");    
				}
				else 
				{
					ConnectionManager.getInstance().requestuserApplyCricle(AddCircleActivity.this.userid,addCirecleName.getText().toString(),addCirecleContent.getText().toString(),true,AddCircleActivity.this);
				}
			}
		});
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{

			if(Command.CIRC_APPLY.equals(cmd))
			{
				this.showToast("亲,申请已经发出,请管理员等待通过."); 
				this.setResult(RESULT_OK);
				this.finish();
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
