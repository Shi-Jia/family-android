package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.FamilyCirclePeopleAdapter;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class FamilyCirclePeopleActivity  extends NavigationActivity{
	FamilyCirclePeopleAdapter adapter;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.familycircle_screen);
		registerHeadComponent();
//		getRightButton().setBackgroundResource(R.drawable.add_normal);
		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("家人");
		registerComponent();
	}
	public void registerComponent()
	{
		ListView list = (ListView)findViewById(R.id.family_listview);
		adapter = new FamilyCirclePeopleAdapter(this);
		list.setAdapter(adapter);
		Integer circle_id = Integer.parseInt(Session.getSession().get("circle_id").toString());
		ConnectionManager.getInstance().requestCircleQueryUserBycircleid(this.circleid, 0, 100, true, this);
	}

	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.CIRC_QUERY_PEOPLE.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
								adapter.setDataSource(list);
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
