package com.yskj.ldm.activity.ui;

import android.os.Bundle;

import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;

public class CreaterAlbumActivity extends NavigationActivity{

	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.album_screen);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.add_normal);
		setHeadTitle("选择相册");
	}
}
