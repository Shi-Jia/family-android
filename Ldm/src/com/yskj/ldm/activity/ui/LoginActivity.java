package com.yskj.ldm.activity.ui;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.commu.parse.Response;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.home.ContainerHomeActivity;
import com.yskj.ldm.activity.impl.PushUpdata;
import com.yskj.ldm.base.AppFrameActivity;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.UserBean;
import com.yskj.ldm.dao.UserDao;
import com.yskj.ldm.dao.impl.UserDaoImpl;
import com.yskj.ldm.db.DatabaseHelper;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.Encrypt;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class LoginActivity extends NavigationActivity  
{
	private Button btnLogin;
	private TextView zhuce,wangjimima;
	private EditText edit_login_account,edit_login_pwd;
	private ImageButton remember_password,automatic_login; 
	private boolean rememberpassword ,automaticlogin;

	//	private UserDao ud = null;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.user_login_screen);
		this.initWithApiKey();
		this.registerLoadingPanel();
		this.registerComponent();
	}
	public void registerComponent()
	{
		btnLogin = (Button)this.findViewById(R.id.btn_login);
		edit_login_account = (EditText)this.findViewById(R.id.edit_login_account);
		edit_login_pwd = (EditText)this.findViewById(R.id.edit_login_pwd);
		zhuce = (TextView)this.findViewById(R.id.zhuce);
		wangjimima = (TextView)this.findViewById(R.id.wangjimima);
		remember_password = (ImageButton)this.findViewById(R.id.remember_password);
		automatic_login = (ImageButton)this.findViewById(R.id.automatic_login);
		//		ud = new UserDaoImpl();
		if(this.r_password)
		{
			remember_password.setBackgroundResource(R.drawable.choice_press);
			rememberpassword = true;
		}
		if(this.a_login)
		{
			automatic_login.setBackgroundResource(R.drawable.choice_press);
			automaticlogin = true;
		}
		if(!StringUtil.isNull(phone))
		{
			edit_login_account.setText(this.phone);
		}
		if(this.a_login||this.r_password)
		{
			if(!StringUtil.isNull(password))
			{
				//				edit_login_account.setText(this.phone);
				edit_login_pwd.setText(this.password);
			}
		}
		remember_password.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0)
			{
				if(!rememberpassword)
				{
					remember_password.setBackgroundResource(R.drawable.choice_press);
					rememberpassword = true;
				}
				else
				{
					remember_password.setBackgroundResource(R.drawable.choice_normal);
					rememberpassword = false;
				}
			}
		});
		automatic_login.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0)
			{
				if(!automaticlogin)
				{
					automatic_login.setBackgroundResource(R.drawable.choice_press);
					automaticlogin = true;
				}
				else
				{
					automatic_login.setBackgroundResource(R.drawable.choice_normal);
					automaticlogin = false;
				}

			}
		});

		zhuce.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {

				LoginActivity.this.startActivity(RegistActivity.class);
			}
		});
		wangjimima.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				LoginActivity.this.startActivity(ResetPasswordActivity.class);
			}
		});
		btnLogin.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {

				if(edit_login_account.getText().toString().equals("")||edit_login_pwd.getText().toString().equals(""))
				{
					LoginActivity.this.showToast("请输入有效的用户名密码!");
				}
				else
				{
					String user_name = edit_login_account.getText().toString().trim();
					String password =edit_login_pwd.getText().toString().trim();
					if(!LoginActivity.this.password.equals(password))
						password = Encrypt.md5(password) ;
//					Map<String, Object> map = PreferencesService.getInstance(LoginActivity.this).getBaiduUserInfo();
//					String reAppid = map.get("appid").toString();
//					String reUserId = map.get("userid").toString();
//					String reChannelId = map.get("channelid").toString();
//					String reRequestId = map.get("requestid").toString();
//					Log.i("user_id", " clound_userId  ==>  "+reUserId);
					ConnectionManager.getInstance().requestLogin( user_name,password,DeviceUtil.getMobileID(LoginActivity.this), 
 
							 true, LoginActivity.this);
				}
			}});
	}
	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))
		{
			return ;
		}
		Log.i("whb", "data -->  "+data);
		try {
			JSONObject jo = JsonUtil.getJSON(data);
			String	errorNo = jo.get("errorNo").toString();
			String errorInfo = jo.getString("errorInfo");

			if(Command.USER_LOGIN.equals(cmd))
			{
				if(errorNo.equals(Command.RESULT_OK)){
					LoginActivity.this.showToast("登陆成功");
					String token =  jo.getString("token");
					String time =  jo.getString("time");
					String reList = jo.get("resList").toString();
					List<HashMap<String, Object>> list = JsonUtil.getList(reList);
					HashMap<String, Object> pramasMap = list.get(0);
 
					UserBean ub = new UserBean();
					ub.setAREA_ID(pramasMap.get("area_id").toString());
					ub.setCIRCLE_ID(pramasMap.get("circle_id").toString());
					ub.setUSER_PASSWORLD(pramasMap.get("user_passworld").toString());
					ub.setUSER_NIKENAME(pramasMap.get("user_nikename").toString());
					ub.setUSER_ADRESS(pramasMap.get("user_adress").toString());
					ub.setUSER_BIRTH(pramasMap.get("user_birth").toString());
					ub.setUSER_EMAIL(pramasMap.get("user_email").toString());
					ub.setUSER_HEADPORTTAIT(pramasMap.get("user_headporttait").toString());
					ub.setREG_DATE(pramasMap.get("reg_date").toString());
					ub.setUSER_STATE(pramasMap.get("user_state").toString());
					ub.setUSER_NAME(pramasMap.get("user_name").toString());
					ub.setUSER_QQ(pramasMap.get("user_qq").toString());
					ub.setUSER_PHONE(pramasMap.get("user_phone").toString());
					ub.setUSER_ID(pramasMap.get("user_id").toString());
					ub.setUSER_QQ(pramasMap.get("user_sex").toString());
					Integer versionno  = Integer.parseInt(StringUtil.isNull(pramasMap.get("versioncode").toString())?"0":pramasMap.get("versioncode").toString());

					pservice.saveUserInfo(ub.getUSER_ID(), ub.getCIRCLE_ID(), ub.getUSER_PHONE(), ub.getUSER_PASSWORLD(),
							ub.getUSER_NIKENAME(),ub.getUSER_ADRESS(),ub.getUSER_BIRTH(),
							ub.getUSER_QQ(),ub.getUSER_EMAIL(),ub.getAREA_ID(),ub.getREG_DATE(),
							ub.getUSER_SEX(),ub.getUSER_HEADPORTTAIT(),ub.getUSER_STATE());
					pservice.savaVersion(versionno);

//					pservice.saveLogin(rememberpassword, automaticlogin);
					
 
					ConnectionManager.getInstance().setToken(ub.getUSER_ID(),DeviceUtil.getMobileID(LoginActivity.this), token, time);
 
					pservice.savaVersionTag(false);
					if(!this.pservice.getPushTag())
						LoginActivity.this.stopBaiduPush();
					LoginActivity.this.startActivity(ContainerHomeActivity.class);
					LoginActivity.this.finish();
				}
				else
					LoginActivity.this.showToast(errorInfo);	
			}
		} catch (JSONException e) {

			e.printStackTrace();
		}
	}

}
