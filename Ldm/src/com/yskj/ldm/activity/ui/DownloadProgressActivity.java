package com.yskj.ldm.activity.ui;



import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;

import android.os.Bundle;
import android.view.View;


public class DownloadProgressActivity extends NavigationActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.download_status);

		registerHeadComponent();
		this.setHeadTitle("正在下载");
		this.getRightButton().setVisibility(View.GONE);
	}
}
