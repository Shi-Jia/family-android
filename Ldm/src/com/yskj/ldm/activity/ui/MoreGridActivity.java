package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.MoreAdapter;
import com.yskj.ldm.base.NavigationActivity;

public class MoreGridActivity extends NavigationActivity
{
	GridView  gv;
	MoreAdapter adapter;
	List<HashMap<String, Object>> items;
	ImageView head;
	TextView nickName,gender;
	RelativeLayout usernumManager;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.moregrid_screen);
		registerHeadComponent();
		setHeadTitle("更多");
		this.getBackButton().setVisibility(View.INVISIBLE);
		registerComponent();
	}
	public void registerComponent()
	{
		head = (ImageView)this.findViewById(R.id.user_head);
		ImageLoader.getInstance().displayImage( pservice.getUserInfoPerferences().get("headporttait").toString(), head);
		nickName = (TextView)this.findViewById(R.id.nick_name);
		gender  = (TextView)this.findViewById(R.id.gender);
		gender.setText( pservice.getUserInfoPerferences().get("sex").toString()); 
		nickName.setText( pservice.getUserInfoPerferences().get("nickname").toString());
		usernumManager = (RelativeLayout)this.findViewById(R.id.usernum_manager);
		usernumManager.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				MoreGridActivity.this.startActivity(UserManagerActivity.class);
			}});
		//		gv = (GridView)this.findViewById(R.id.more_gridview);
		//		adapter = new MoreAdapter( this);
		//		items = new ArrayList<HashMap<String, Object>>();
		//		HashMap<String, Object> bullet1 = new HashMap<String, Object>();
		//		bullet1.put("image_path", R.drawable.remind);
		//		bullet1.put("bucket_name", "提醒");
		//		HashMap<String, Object> bullet2 = new HashMap<String, Object>();
		//		bullet2.put("image_path", R.drawable.activity);
		//		bullet2.put("bucket_name", "活动");
		//		HashMap<String, Object> bullet3 = new HashMap<String, Object>();
		//		bullet3.put("image_path", R.drawable.set_up);
		//		bullet3.put("bucket_name", "设置");
		//		HashMap<String, Object> bullet4 = new HashMap<String, Object>();
		//		bullet4.put("image_path", R.drawable.about);
		//		bullet4.put("bucket_name", "关于");
		//		HashMap<String, Object> bullet5 = new HashMap<String, Object>();
		//		bullet5.put("image_path", R.drawable.help);
		//		bullet5.put("bucket_name", "帮助");
		//		HashMap<String, Object> bullet6 = new HashMap<String, Object>();
		//		bullet6.put("image_path", R.drawable.contact);
		//		bullet6.put("bucket_name", "联系我们");
		//		HashMap<String, Object> bullet7 = new HashMap<String, Object>();
		//		bullet7.put("image_path", R.drawable.account_management);
		//		bullet7.put("bucket_name", "账号管理");
		//		items.add(bullet1);
		//		items.add(bullet2);
		//		items.add(bullet3);
		//		items.add(bullet4);
		//		items.add(bullet5);
		//		items.add(bullet6);
		//		items.add(bullet7);
		//		adapter.setDataSource(items);
		//		gv.setAdapter(adapter);
	}
}
