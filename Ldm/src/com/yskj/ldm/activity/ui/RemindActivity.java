package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.widget.ListView;

import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.LaoDaoQuanListAdapter1;
import com.yskj.ldm.activity.adapter.RemindAdapter;
import com.yskj.ldm.base.NavigationActivity;

public class RemindActivity  extends NavigationActivity{

	RemindAdapter adapter = null;
	ArrayList<HashMap<String, Object>> listdata = new ArrayList<HashMap<String, Object>>();
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.remind_screen);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.add_normal);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("提醒列表");
		registerComponent();
	}
	
	public void registerComponent()
	{
		ListView list = (ListView)findViewById(R.id.r_listview);
//		list.setOnScrollListener(scrollerListener);
		adapter = new RemindAdapter(this);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("circle", "家庭圈一");
		hm.put("relationship", "爸爸");
		hm.put("content", "记得吃药");
		hm.put("date", "2014-7-25");
		hm.put("time", "9:00");
		HashMap<String, Object> hm1 = new HashMap<String, Object>();
		hm1.put("circle", "家庭圈一");
		hm1.put("relationship", "妹妹");
		hm1.put("content", "记得考试复习");
		hm1.put("date", "2014-7-25");
		hm1.put("time", "9:00");
		listdata.add(hm);
		listdata.add(hm1);
		adapter.setDataSource(listdata);
		list.setAdapter(adapter);

	}
}
