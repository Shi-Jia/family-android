package com.yskj.ldm.activity.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.android.commu.net.HandlerException;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.R.color;
import com.yskj.ldm.activity.adapter.FamilyCircleAdapter;
import com.yskj.ldm.activity.adapter.FamilyCircleListAdapter;
import com.yskj.ldm.activity.adapter.FamilyCirclePeopleAdapter;
import com.yskj.ldm.activity.adapter.LaoDaoQuanAdapter;
import com.yskj.ldm.activity.adapter.LaoDaoQuanListAdapter1;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.activity.impl.FamilyOnClickListener;
import com.yskj.ldm.activity.view.PullToRefreshView;
import com.yskj.ldm.activity.view.PullToRefreshView.OnFooterRefreshListener;
import com.yskj.ldm.activity.view.PullToRefreshView.OnHeaderRefreshListener;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.CommentsBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.services.DownloadService;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.SDCardStoreManager;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;
/**
 * @author haibo.wang
 * 登陆之后的分享展示*/
public class FamilyCircleListActivity  extends NavigationActivity implements FamilyOnClickListener,OnHeaderRefreshListener,OnFooterRefreshListener{
	private FamilyCircleListAdapter adapter = null;
	private PullToRefreshView mPullToRefreshView;
	private ListView circleList;
	public static RelativeLayout commentsLayout;
	TextView iv1,iv2;
	EditText etComments;
	LinearLayout  circle;
	TextView circleSelectOne;
	//	TextView []circleSelect;
	private int positon,contentId;
	private boolean isHeader,isFooter;
	ArrayList<HashMap<String, Object>> listdata = new ArrayList<HashMap<String, Object>>();

	private boolean showRight = false;
	private File file =  null;
	private static final int TAKE_BIG_PICTURE = 1;
	RelativeLayout  []circleSelect;
	RelativeLayout  allCircleSelect;
	int versionCode;
	String version_url ;
	private String familyCircle ="";
	private List<CommentsBean> recordComments = new  ArrayList<CommentsBean>();
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.familycirclelist_screen);
		registerHeadComponent();
		this.getBackButton().setVisibility(View.INVISIBLE);
		getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);
		setHeadTitle("家庭圈");
		registerComponent();
	}
	protected void onRightAction()
	{
		if(!showRight)
		{
			getRightButton().setBackgroundResource(R.drawable.family_add_icon_normal);
			circledialogItemView.setVisibility(View.VISIBLE);
			showRight= true;
		}
		else
		{
			getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);
			circledialogItemView.setVisibility(View.GONE);
			showRight = false;
		}
	}
	public void registerComponent()
	{
		familyCircle = this.circleid;
		RelativeLayout dialogCramre=(RelativeLayout)circledialogItemView.findViewById(R.id.family_dialog_cramre_layout);
		RelativeLayout dialogPicupload=(RelativeLayout)circledialogItemView.findViewById(R.id.family_dialog_picupload_layout);
		dialogCramre.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				if(SDCardStoreManager.getInstance().checkSDCardState())
				{
					circledialogItemView.setVisibility(View.GONE);
					getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);
					showRight = false;
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					if(null!=file)file = null;
					file = SDCardStoreManager.getInstance().getFilePath(SDCardStoreManager.getInstance().getRootPath()+RecordsPath.ImageRoot,System.currentTimeMillis()+".jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); 
					intent.putExtra("return-data", true);
					intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
					startActivityForResult(intent, TAKE_BIG_PICTURE);  
				}
				else
				{
					FamilyCircleListActivity.this.showToast("亲,请插入SDCard进入拍照");
				}

			}});
		dialogPicupload.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {

				circledialogItemView.setVisibility(View.GONE);
				getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);

				showRight = false;
				Session.getSession().put("jump_type", "LaodaoQuanListActivity");
				FamilyCircleListActivity.this.startActivity(PhotoAlbumActivity.class);

			}});

		commentsLayout  = (RelativeLayout)this.findViewById(R.id.familylist_comments_layout);
		etComments = (EditText)commentsLayout.findViewById(R.id.familylist_edit_comments);
		Button send = (Button)commentsLayout.findViewById(R.id.familylist_btn_send_comments);
		send.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				if(!StringUtil.isNull(etComments.getText().toString()))
				{
					String commentsContent = etComments.getText().toString();
					ConnectionManager.getInstance().requestCommentsInsert( FamilyCircleListActivity.this.userid, contentId, 0, commentsContent, true, FamilyCircleListActivity.this);
				}
				else
				{
					FamilyCircleListActivity.this.showToast("亲,请说点什么吧..");
				}
			}
		});


		mPullToRefreshView = (PullToRefreshView)findViewById(R.id.main_pull_refresh_view);
		circleList = (ListView)findViewById(R.id.listview);
		mPullToRefreshView.setOnHeaderRefreshListener(this);
		mPullToRefreshView.setOnFooterRefreshListener(this);
		adapter = new FamilyCircleListAdapter(this);
		adapter.setCommentsListener(this);
		circleList.setAdapter(adapter);
		circleList.setOverScrollMode(View.OVER_SCROLL_NEVER);

		versionCode = DeviceUtil.getVersionCode(this);
		//是否有版本升级
		if(this.pservice.getVersion()>versionCode){
			if(!this.pservice.getVersionTag())
				ConnectionManager.getInstance().requestUpdataVesion(versionCode, true, this);
		}
		//		Log.i("versionCode", " versionCode1  --->  "+versionCode);
		//		Log.i("versionCode", " versionCode2  --->  "+this.pservice.getVersion());
		//		circleList.setOnItemClickListener(new OnItemClickListener(){
		//			@Override
		//			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
		//					long arg3) {
		//				Toast.makeText(FamilyCircleListActivity.this, "positon = "+position, 1000).show();
		//				Session.getSession().put("contentid",((Map<String, Object>)adapter.getItem(position)).get("content_id"));
		//				FamilyCircleListActivity.this.startActivity(CommentDetailActivity.class);
		//			}});
		ConnectionManager.getInstance().requestQueryCircle(this.userid, true, this);
		ConnectionManager.getInstance().requestDownloadMoon(this.familyCircle, "", adapter.param++,5, true,this);
	}
	public void addAllCircle(LinearLayout ll,LinearLayout.LayoutParams rlparams,final Integer size)
	{
		circle = (LinearLayout)this.findViewById(R.id.family_circle_layout);
		allCircleSelect = (RelativeLayout)this.inflate(R.layout.select_button_item);
		allCircleSelect.setLayoutParams(rlparams);
		TextView tv_name = (TextView)allCircleSelect.findViewById(R.id.select_btn_tv);
		final View view_bg = allCircleSelect.findViewById(R.id.select_btn_bg);
		tv_name.setText("所有圈子");
		allCircleSelect.setOnClickListener(  new OnClickListener(){
			public void onClick(View view) {
				clearCircleBlack(size);
				view_bg.setVisibility(View.VISIBLE);
				adapter.param = 0;
				adapter.getList().clear();
				recordComments.clear();
				ConnectionManager.getInstance().requestDownloadMoon( FamilyCircleListActivity.this.familyCircle, "", adapter.param++,5, false,FamilyCircleListActivity.this);
			}
		});
		View view = new View(this);
		view.setBackgroundResource(color.white);
		LinearLayout.LayoutParams viewparams = new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		viewparams.width = 1;
		viewparams.height = 60;
		view.setLayoutParams(viewparams);
		circle.addView(allCircleSelect);
		//		circle.addView(view);
		allCircleSelect.findViewById(R.id.select_btn_bg).setVisibility(View.VISIBLE);
	}
	public void creatCircle(List<HashMap<String, Object>> list)
	{

		circle = (LinearLayout)this.findViewById(R.id.family_circle_layout);
		circle.removeAllViews();
		circleSelect = new RelativeLayout[list.size()];
		LinearLayout.LayoutParams rlparams = new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		if(list.size()<=1)
		{
			rlparams.width = this.mWidth/2;
		}
		else
		{
			rlparams.width = this.mWidth/3;
		}
		this.addAllCircle(circle, rlparams,list.size());
		for(int i = 0;i<list.size();i++)
		{
			circleSelect [i] = (RelativeLayout)this.inflate(R.layout.select_button_item);
			circleSelect [i].setLayoutParams(rlparams);
			TextView tv_name = (TextView)circleSelect [i].findViewById(R.id.select_btn_tv);
			final View view_bg = circleSelect [i].findViewById(R.id.select_btn_bg);
			final HashMap<String, Object> mapList=(HashMap<String, Object>) list.get(i);
			tv_name.setText(mapList.get("circle_name").toString());

			circleSelect [i].setOnClickListener(  new OnClickListener(){
				public void onClick(View view) {
					clearCircleBlack(circleSelect.length);
					view_bg.setVisibility(View.VISIBLE);
					adapter.param = 0;
					 
					adapter.getList().clear();
					recordComments.clear();
					FamilyCircleListActivity.this.familyCircle = mapList.get("circle_id").toString();
					ConnectionManager.getInstance().requestDownloadMoon(FamilyCircleListActivity.this.familyCircle, "", adapter.param++,5, false,FamilyCircleListActivity.this);
				}
			});
			//			View view = new View(this);
			//			view.setBackgroundResource(color.white);
			//			LinearLayout.LayoutParams viewparams = new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			//			viewparams.width = 1;
			//			viewparams.height = 60;
			//			view.setLayoutParams(viewparams);

			circle.addView(circleSelect[i]);
			//			circle.addView(view);
		}
	}

	public void clearCircleBlack(int size)
	{
		allCircleSelect.findViewById(R.id.select_btn_bg).setVisibility(View.INVISIBLE);
		for(int i =0;i<size;i++)
		{
			circleSelect [i].findViewById(R.id.select_btn_bg).setVisibility(View.INVISIBLE);
		}

	}
	//检车到下载升级
	private void showUpdateDialog(String verName,  String curVerName,
			String descrip) {
		this.showAlertDialog("稍后再说", "更新", "最新版本：v" + verName + "\n当前版本：v" + curVerName
				+ "\n更新说明:" + descrip + "\n是否更为最新版本?","更新提示");
	}
	//版本下载升级
	private void startDownloadService(String url,String version) {

		Intent intent = new Intent(this, DownloadService.class);
		intent.putExtra("url",url);
		intent.putExtra("version",version);
		this.startService(intent);
		showToast("正在下载...");
	}
	@Override
	protected void dialogOnCancel() {
		this.pservice.savaVersionTag(true);
		super.dialogOnCancel();
	}
	@Override
	protected void dialogOnSure()
	{
		if(!StringUtil.isNull(version_url))
		{
			startDownloadService(version_url.replaceAll("\"", ""),pservice.getVersion()+"");
		}
		super.dialogOnSure();
	}
	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{ 
		super.onActivityResult(requestCode, resultCode, data);  
		if (resultCode == Activity.RESULT_OK) { 
			if(requestCode == TAKE_BIG_PICTURE){
				if(null!=file){
					Session.getSession().put("imagepath", file.getPath());
					FamilyCircleListActivity.this.startActivity(CameraPicActivity.class);
				}
			}
		}
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))return;

		JsonObject jo = JsonUtil.parseJson(data);
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.DOWN_MOON.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				Log.i("resList", "  resList   --> " +reList);
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				if(list.size()>0)
				{
					mPullToRefreshView.setVisibility(View.VISIBLE);
					for(int i = 0;i< list.size();i++)
					{
						List<HashMap<String, Object>> comments =JsonUtil.getList(list.get(i).get("comments").toString());
						for(int j = 0;j<comments.size();j++)
						{
							CommentsBean   cb = new CommentsBean();
							String content = comments.get(j).get("comments_content").toString();
							String name = comments.get(j).get("user_nikename").toString();
							cb.setName(name);
							cb.setContent(content);
							cb.setPostion(((adapter.param-1)*5)+i);
							recordComments.add(cb);
						}
						adapter.add(i, recordComments);
					}
					if(this.isHeader) 
						adapter.getList().clear();

					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					if(adapter.param ==1)
						mPullToRefreshView.setVisibility(View.GONE);
					adapter.param --;
					this.showToast("亲,没有更多心情了哦.");  
				}
				if(this.isHeader)
				{
					mPullToRefreshView.onHeaderRefreshComplete();
					this.isHeader = false;
					PreferencesService.getInstance(this).savaPushCircle(0);
				}
				else if(this.isFooter)
				{
					mPullToRefreshView.onFooterRefreshComplete();
					this.isFooter = false;
				}
				else 
				{
					PreferencesService.getInstance(this).savaPushCircle(0);
				}
			} 
			else if(Command.MOON_COMMENTS.equals(cmd))
			{
				this.showToast("评论成功");  
				String commentsContent = etComments.getText().toString();
				CommentsBean   cb = new CommentsBean();
				cb.setPostion(FamilyCircleListActivity.this.positon);
				cb.setContent(commentsContent);
				cb.setName(pservice.getUserInfoPerferences().get("nickname").toString());
				recordComments.add(0, cb);
				adapter.add(FamilyCircleListActivity.this.positon, recordComments);
				adapter.notifyDataSetChanged();
				commentsLayout.setVisibility(View.GONE);
				etComments.setText("");
			}
			else if( Command.CIRC_QUERY.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				//				this.circleListData = list;

				this.creatCircle(list);
				Session.getSession().put("headlist", list);
			}
			else if(Command.VESION_CHECK.equals(cmd))
			{
				Log.i("whb", "data -->  "+data);
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				HashMap<String, Object> pramasMap = list.get(0);
				if(pservice.getVersion()>versionCode)
				{
					version_url = jo.get("andrVerUrl").toString().replaceAll("\"", "");
					showUpdateDialog(pramasMap.get("versionno").toString(),DeviceUtil.getVersionName(this),pramasMap.get("versiondesc").toString().replaceAll("n", "\n"));
				}
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		List<HashMap<String, Object>> list = (List<HashMap<String, Object>>)Session.getSession().get("headlist");
		if(null!=list){
			Log.i("onresume", list.size() +"  <<-----this.circleListData");

			this.creatCircle(list);
		}
		//		ConnectionManager.getInstance().requestQueryCircle(this.userid, true, this);
	}
	/**
	 * 网络异常处理
	 */
	protected void onErrorAction(int cmd,HandlerException e){
		if(cmd ==Command.DOWN_MOON){
			adapter.isloading = false;
		}
		super.onErrorAction(cmd,e);
	}
	@Override
	public void onFooterRefresh(PullToRefreshView view) {
		adapter.isloading = true;
		this.isFooter = true;
		ConnectionManager.getInstance().requestDownloadMoon(FamilyCircleListActivity.this.familyCircle, "", adapter.param,5, false,this);
		adapter.param++;
	}

	@Override
	public void onHeaderRefresh(PullToRefreshView view) {
		isHeader = true;
		adapter.param = 0;
		recordComments.clear();
		ConnectionManager.getInstance().requestDownloadMoon(FamilyCircleListActivity.this.familyCircle, "", adapter.param++,5, false,this);
	}
	@Override
	public void onComments(Integer contentId, int postion) {
		this.positon = postion;
		this.contentId = contentId;
		commentsLayout.setVisibility(View.VISIBLE);
	}
}
