package com.yskj.ldm.activity.ui;
 
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

public class CreaterCircleActivity extends NavigationActivity{

	private EditText creatCircleName,creatCircleInstration;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.creater_circle_screen);
		registerHeadComponent();
		setHeadTitle("创建圈子");
		registerComponent();
	}
	public void registerComponent()
	{

		creatCircleName =   (EditText)this.findViewById(R.id.creater_circle_name);
		creatCircleName.setHint("请输入圈子名字");
		creatCircleInstration = (EditText)this.findViewById(R.id.creater_circle_introduce);
		creatCircleInstration.setHint("请输入圈子简介");
		Button createrOk = (Button)this.findViewById(R.id.btn_creater_circle);
		createrOk.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) {
				if(StringUtil.isNull(creatCircleName.getText().toString()))
				{
					CreaterCircleActivity.this.showToast("亲,名字不能为空.");    
				}
				else 
				{
					ConnectionManager.getInstance().requestAddCircle(CreaterCircleActivity.this.userid, creatCircleName.getText().toString(), creatCircleInstration.getText().toString(),
							1.123f,1.123f, "1", true, CreaterCircleActivity.this); 
				}
			}
		});
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.CIRC_CREA.equals(cmd))
			{
				this.showToast("创建成功");  
				this.setResult(RESULT_OK);
				this.finish();
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
