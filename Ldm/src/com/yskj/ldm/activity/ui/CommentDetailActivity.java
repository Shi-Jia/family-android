package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.R.color;
import com.yskj.ldm.R.drawable;
import com.yskj.ldm.activity.adapter.FamilyCircleListAdapter;
import com.yskj.ldm.activity.impl.FamilyOnClickListener;
import com.yskj.ldm.activity.view.JListView;
import com.yskj.ldm.activity.view.JListView.IXListViewListener;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.CommentsBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class CommentDetailActivity extends NavigationActivity implements FamilyOnClickListener,IXListViewListener{
	private JListView commentsList;
	//	private LinearLayout commentsLayout;
	private int positon,contentId;
	private RelativeLayout commentsLayout;
	private FamilyCircleListAdapter adapter = null;
	EditText etComments;
	private List<CommentsBean> recordComments = new  ArrayList<CommentsBean>();
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.comment_detail_screen);
		registerHeadComponent();
		setHeadTitle("评论详情");
		registerComponent();
	}

	private void registerComponent()
	{
		String contentId1 = Session.getSession().get("contentid").toString();
		if(StringUtil.isNull(contentId1))
		{
			contentId1 = "0";
		}
		contentId = Integer.parseInt(contentId1);
		commentsList = (JListView)this.findViewById(R.id.comments_list);
		commentsList.setPullLoadEnable(false);
		commentsList.setPullRefreshEnable(false);
		commentsList.setXListViewListener(this);
		adapter = new FamilyCircleListAdapter(this);
		adapter.setCommentsListener(this);
		commentsList.setAdapter(adapter);

		commentsLayout  = (RelativeLayout)this.findViewById(R.id.familylist_comments_layout);
		etComments = (EditText)commentsLayout.findViewById(R.id.familylist_edit_comments);
		Button send = (Button)commentsLayout.findViewById(R.id.familylist_btn_send_comments);
		send.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				String commentsContent = etComments.getText().toString();
				ConnectionManager.getInstance().requestCommentsInsert( CommentDetailActivity.this.userid,  contentId, 0, commentsContent, true, CommentDetailActivity.this);
			}
		});
		ConnectionManager.getInstance().requestAllbyContentid( contentId, true, CommentDetailActivity.this);
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.CIRC_QUERY_CONTENTID.equals(cmd))
			{ 
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				if(list.size()>0)
				{
					for(int i = 0;i< list.size();i++)
					{
						List<HashMap<String, Object>> comments =JsonUtil.getList(list.get(i).get("comments").toString());
						for(int j = 0;j<comments.size();j++)
						{
							CommentsBean   cb = new CommentsBean();
							String content = comments.get(j).get("comments_content").toString();
							String name = comments.get(j).get("user_nikename").toString();
							cb.setName(name);
							cb.setContent(content);
							cb.setPostion(0);
							recordComments.add(cb);
						}
						adapter.add(i, recordComments);
					}
					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					adapter.param --;
					this.showToast("亲,没有更多心情了哦.");  
				}
			}	
			else if(Command.MOON_COMMENTS.equals(cmd))
			{
				this.showToast("评论成功");  
				String commentsContent = etComments.getText().toString();
				CommentsBean   cb = new CommentsBean();
				cb.setPostion(CommentDetailActivity.this.positon);
				cb.setContent(commentsContent);
				cb.setName(pservice.getUserInfoPerferences().get("nickname").toString());
				recordComments.add(0, cb);
				adapter.add(CommentDetailActivity.this.positon, recordComments);
				adapter.notifyDataSetChanged();
				commentsLayout.setVisibility(View.GONE);
				etComments.setText("");
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}

	@Override
	public void onComments(Integer contentId, int postion) {
		this.positon = postion;
		this.contentId = contentId;
		commentsLayout.setVisibility(View.VISIBLE);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub

	}
}
