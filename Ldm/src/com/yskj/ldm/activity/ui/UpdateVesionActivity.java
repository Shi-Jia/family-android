package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.services.DownloadService;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

public class UpdateVesionActivity  extends NavigationActivity{

	Button update;
	TextView tv_version,update_tv_version,vesion_code;
	int versionCode;
	String version_url;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.vesion_update_screen);

		registerHeadComponent();
		this.setHeadTitle("版本更新");
		this.getRightButton().setVisibility(View.GONE);
		versionCode = DeviceUtil.getVersionCode(this);
		update = (Button)this.findViewById(R.id.vesion_btn);
		tv_version =(TextView)this.findViewById(R.id.vesion_txt);
		update_tv_version =(TextView)this.findViewById(R.id.updata_vesion_code);
		vesion_code =(TextView)this.findViewById(R.id.vesion_code);
		vesion_code.setText("当前版本:V"+DeviceUtil.getVersionName(this));

		update.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0)
			{
				if(!StringUtil.isNull(version_url))
				{
					startDownloadService(version_url.replaceAll("\"", ""),pservice.getVersion()+"");
				}
			}
		});
		ConnectionManager.getInstance().requestUpdataVesion(versionCode, true, this);	    
	}

	private void startDownloadService(String url,String version) {

		Intent intent = new Intent(this, DownloadService.class);
		intent.putExtra("url",url);
		intent.putExtra("version",version);
		this.startService(intent);
		showToast("正在下载...");
	}


	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))return;
		Log.i("whb", "data -->  "+data);
		JsonObject jo = JsonUtil.parseJson(data);
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		//		08-14 17:18:57.504: I/whb(10809): data -->  {"errorInfo":null,"interfaceId":4000,
		//			"resList":[{"savepath":"laodaomi/appversion/android/Ldm.apk","versionno":"3.0","submittime":"2014-08-14",
		//			"versiondesc":"可以分享照片和更新唠叨圈了","versioncode":3,"updatetype":1,"apptype":1}],"errorNo":0,
		//			"andrVerUrl":"http://bcs.duapp.com/laodaomi/ppversion%2Fandroid%2FLdm.apk?sign=MBO:uOrXDZ2ojGbfTG8QXFniRkw4:0Ya9EtMgQY0lyOeIeFectnpNDO8%3D"}

		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.VESION_CHECK.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				HashMap<String, Object> pramasMap = list.get(0);
				//				Integer versionno  = Integer.parseInt(StringUtil.isNull(pramasMap.get("versioncode").toString())?"0":pramasMap.get("versioncode").toString());
				//				Log.i("versionno", "versionno1 --> " +versionno);
				//				Log.i("versionno", "versionno2 --> " +versionCode);
				if(pservice.getVersion()>versionCode)
				{
					version_url = jo.get("andrVerUrl").toString();
					tv_version.setText(pramasMap.get("versiondesc").toString().replaceAll("n", "\n"));
					update_tv_version.setText( "最新版本:V"+pramasMap.get("versionno").toString());
				}
				else
				{
					update.setEnabled(true);
					tv_version.setText("当前已是最新版本");
				}
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
