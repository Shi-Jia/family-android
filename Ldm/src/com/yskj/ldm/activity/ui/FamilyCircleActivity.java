package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.FamilyCircleAdapter;
import com.yskj.ldm.activity.adapter.FamilyCirclePeopleAdapter;
import com.yskj.ldm.activity.adapter.LaoDaoQuanListAdapter1;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.home.TabHomeActivity;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.CircleBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class FamilyCircleActivity  extends NavigationActivity{


	FamilyCircleAdapter adapter = null;
	ArrayList<HashMap<String, Object>> listdata = new ArrayList<HashMap<String, Object>>();
	TextView circleCount;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.familycircle_screen);
		registerHeadComponent();
		getRightButton().setText("确定");
		setHeadTitle("选择家庭圈");
		registerComponent();
	}
	protected void onRightAction()
	{
		List<CircleBean> cblist = new ArrayList<CircleBean>();
		for(int i = 0;i<adapter.recordlist.size();i++)
		{
			CircleBean cb = new CircleBean();
			cb.setCircle_id(Integer.parseInt(this.adapter.recordlist.get(i).get("circle_id").toString()));
			cb.setCircle_name(this.adapter.recordlist.get(i).get("circle_name").toString());
			cb.setPosition(Integer.parseInt(this.adapter.recordlist.get(i).get("position").toString()));
			cblist.add(cb);
			Log.i("whb", "  recordlist  --->  "+cb.getCircle_name());
		}
		Session.getSession().put("see_circle", cblist);
		this.setResult(RESULT_OK);
		this.finish();
	}
	public void registerComponent()
	{
		ListView list = (ListView)findViewById(R.id.family_listview);
		adapter = new FamilyCircleAdapter(this);
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener(){

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				adapter.chiceState(position);
			}
		});
		circleCount = (TextView)this.findViewById(R.id.circle_count);
		ConnectionManager.getInstance().requestQueryCircle(this.userid, true, this);
	}


	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{

			if(Command.CIRC_QUERY.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				adapter.setDataSource(list);
				circleCount.setText("圈子"+list.size()+"个");
				List<CircleBean> recordlist = (List<CircleBean>)Session.getSession().get("see_circle");
				if(null != recordlist )
				{
					if(recordlist.size()>0)
					{
						for(int i = 0;i<recordlist.size();i++)
						{
							adapter.chiceState(recordlist.get(i).getPosition());
						}
					}
				}
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
