package com.yskj.ldm.activity.ui;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.AbsListView.OnScrollListener;
import android.widget.RelativeLayout.LayoutParams;

import com.android.commu.net.HandlerException;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.LaoDaoQuanListAdapter1;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.activity.home.TabHomeActivity;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.SDCardStoreManager;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class PhotoShareActivity extends NavigationActivity{

	private LaoDaoQuanListAdapter1 adapter;
	private PopupWindow topDialog;
	private Button paizhao,piliangshangchuan,cancel;
	private File file =  null;
	private static final int TAKE_BIG_PICTURE = 1;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.laodaoquan_list_screen);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.add_normal);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("照片分享");
		registerComponent();
	}

	private void registerComponent(){
		View view = (View)LayoutInflater.from(this).inflate(R.layout.topdialog_item, null);
		topDialog = new PopupWindow(view,LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		getRightButton().setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {

				topDialog.showAtLocation(PhotoShareActivity.this.getWindow().getDecorView(), Gravity.BOTTOM, 0, 0);
			}
		} );
		paizhao = (Button)view.findViewById(R.id.toppaizhao);
		piliangshangchuan = (Button)view.findViewById(R.id.piliangshangchuan);
		cancel = (Button)view.findViewById(R.id.cancel);
		paizhao.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(null!=file)
                	file = null;
				file = SDCardStoreManager.getInstance().getFilePath(SDCardStoreManager.getInstance().getRootPath()+RecordsPath.ImageRoot,System.currentTimeMillis()+".jpg");
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); 
				intent.putExtra("return-data", true);
				intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
				startActivityForResult(intent, TAKE_BIG_PICTURE);  
//				if(SDCardStoreManager.getInstance().checkSDCardState())
//					PhotoShareActivity.this.startActivity(CameraActivity.class);
//				else
//					PhotoShareActivity.this.showToast("亲,请插入SDCard进入拍照");
		 
			}
		});
		piliangshangchuan.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
				Session.getSession().put("jump_type", "LaodaoQuanListActivity");
				PhotoShareActivity.this.startActivity(PhotoSelectActivity.class);
			}
		});
		cancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
			}
		});
		ListView list = (ListView)findViewById(R.id.listview);
		list.setOnScrollListener(scrollerListener);
//		adapter = new LaoDaoQuanListAdapter(this);
		list.setAdapter(adapter);

		ConnectionManager.getInstance().requestDownloadMoon(PhotoShareActivity.this.circleid, "", adapter.param++,5, true, PhotoShareActivity.this);
	}
	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{ 
		super.onActivityResult(requestCode, resultCode, data);  
		if (resultCode == Activity.RESULT_OK) { 
			if(requestCode == TAKE_BIG_PICTURE){
				if(null!=file){
				Session.getSession().put("imagepath", file.getPath());
				PhotoShareActivity.this.startActivity(CameraPicActivity.class);
				}
			}
		}
	}
	private OnScrollListener scrollerListener = new OnScrollListener() {

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {

				if (!adapter.isloading && view.getLastVisiblePosition() == view.getCount() - 1) {
					adapter.isloading = true;
					ConnectionManager.getInstance().requestDownloadMoon(PhotoShareActivity.this.circleid, "", adapter.param,5, true, PhotoShareActivity.this);
					adapter.param++;
				}
			}
		}
	};
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){
 
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))
		{
			return ;
		}
		Log.i("data", "  data  -->  "+data);
		JsonObject jo = JsonUtil.parseJson(data);

		String reList = jo.get("resList").toString();
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		Integer  interfaceId = Integer.parseInt(jo.get("interfaceId").toString()); 
		List<HashMap<String, Object>> list = JsonUtil.getList(reList);
		if(errorNo.equals(Command.RESULT_OK))
		{
 
			if(interfaceId.equals(Command.DOWN_MOON))
			{
				if(list.size()>0)
				{
					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					adapter.param --;
					this.showToast("亲,没有更多心情了哦.");  
				}
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
	/**
	 * 网络异常处理
	 */
	protected void onErrorAction(int cmd,HandlerException e){
		if(cmd ==Command.DOWN_MOON){
			adapter.isloading = false;
		}
		super.onErrorAction(cmd,e);
	}

}
