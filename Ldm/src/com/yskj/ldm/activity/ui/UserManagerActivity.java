package com.yskj.ldm.activity.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;

public class UserManagerActivity  extends NavigationActivity{

	RelativeLayout moreExit;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.user_manager);
		registerHeadComponent();
		setHeadTitle("账号管理");
		this.getBackButton().setVisibility(View.INVISIBLE);
		registerComponent();
	}
	public void registerComponent()
	{
		moreExit = (RelativeLayout)this.findViewById(R.id.more_exit);
		moreExit.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) 
			{			 
				UserManagerActivity.this.showAlertDialog("取消", "确定","亲,您不再留会儿了吗","唠叨觅温馨提示");
			}});
	}

	@Override
	protected void dialogOnCancel() {

		super.dialogOnCancel();
	}

	@Override
	protected void dialogOnSure() {
		UserManagerActivity.this.pservice.saveLogin(false, false);
		if(UserManagerActivity.this.pservice.getLoginType().equals(1))
		{
			mQQAuth.logout(this);
		}
		UserManagerActivity.this.pservice.savaLoginType(0);
		UserManagerActivity.this.laodaomiApplication.finishAll();
		super.dialogOnSure();
	}

}
