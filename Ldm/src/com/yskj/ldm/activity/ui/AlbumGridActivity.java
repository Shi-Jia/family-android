package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout.LayoutParams;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.AlbumGridAdapter;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.AlbumBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class AlbumGridActivity extends NavigationActivity
{
	private GridView gridView;
	private AlbumGridAdapter  adapter;
	private PopupWindow topDialog,topDialog1;
	private List<HashMap<String, Object>> filebean = new ArrayList<HashMap<String, Object>>();
	private Button addalbumBtn;
	private EditText title,instruction;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.album_screen);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.add_normal);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("选择相册");
		gridView =  (GridView)this.findViewById(R.id.album_select_grid);
		adapter = new AlbumGridAdapter(this);
		adapter.setDataSource(filebean);
		gridView.setAdapter(adapter);
		gridView.setOnScrollListener(scrollerListener);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Session.getSession().put("album_id", ((HashMap<String, Object>)adapter.getItem(position)).get("album_id").toString());
				AlbumGridActivity.this.startActivity(AlbumPhotoGridAvtivity.class);
			}
		});
		View view = (View)LayoutInflater.from(this).inflate(R.layout.topdialog_item, null);
		View addalbumview = (View)LayoutInflater.from(this).inflate(R.layout.addalbumdialog_item, null);
		topDialog = new PopupWindow(view,LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		topDialog1 = new PopupWindow(addalbumview,LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT,true);

		addalbumBtn = (Button)view.findViewById(R.id.toppaizhao);
		addalbumBtn.setText("添加相册");
		((Button)view.findViewById(R.id.piliangshangchuan)).setVisibility(View.GONE);
		((Button)view.findViewById(R.id.cancel)).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
			}});
		addalbumBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
				topDialog1.showAtLocation(AlbumGridActivity.this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
			}});
		((Button)addalbumview.findViewById(R.id.cancel)).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				topDialog1.dismiss();
			}});
		title  =  (EditText)addalbumview.findViewById(R.id.add_album_title);
		instruction = (EditText)addalbumview.findViewById(R.id.add_album_instration);
		((Button)addalbumview.findViewById(R.id.ok)).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				topDialog1.dismiss();
				if(StringUtil.isNull(title.getText().toString()))
					AlbumGridActivity.this.showToast("亲,标题不能为空.");  
				else
					ConnectionManager.getInstance().requestAddAlbum(userid,title.getText().toString(),instruction.getText().toString(),true,AlbumGridActivity.this );
			}});
		ConnectionManager.getInstance().requestQueryAlbum(this.userid,adapter.param++,6,true,AlbumGridActivity.this);
	}
	private OnScrollListener scrollerListener = new OnScrollListener() {
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
		}
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {

				if (!adapter.isloading && view.getLastVisiblePosition() == view.getCount() - 1) {
					adapter.isloading = true;
					ConnectionManager.getInstance().requestQueryAlbum(userid,adapter.param,6,true,AlbumGridActivity.this);
					adapter.param++;
				}
			}
		}
	};
	protected void onRightAction()
	{
		topDialog.showAtLocation(AlbumGridActivity.this.getWindow().getDecorView(), Gravity.BOTTOM, 0, 0);
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.ALBL_QUERY.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				if(list.size()>0)
				{
					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					adapter.param --;
					this.showToast("亲,没有更多相册了哦.");  
				}
			}
			else if(Command.ALBL_CREA.equals(cmd))
			{
				this.showToast("创建成功");  
				adapter.clear();
				adapter.param = 0;
				ConnectionManager.getInstance().requestQueryAlbum(this.userid,adapter.param++,6,true,this);

			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
