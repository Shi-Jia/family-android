package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.*;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore.*;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.PhotoAlbumAdapter;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.util.Session;

public class PhotoAlbumActivity  extends NavigationActivity{

	ListView albumList;
	PhotoAlbumAdapter adapter;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photoablum_screen);
		registerHeadComponent();
		this.setHeadTitle("选择相册");
		albumList = (ListView)this.findViewById(R.id.photoablum_list);
		adapter = new PhotoAlbumAdapter(this);
		adapter.setDataSource(this.queryPhotoAlbum(this));
		albumList.setAdapter(adapter);
		albumList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3)
			{
				Session.getSession().put("bucket_id", adapter.getList().get(position).get("bucket_id").toString());
				PhotoAlbumActivity.this.startActivity(PhotoSelectActivity.class);
			}
		});
	}

	public static List <HashMap<String, Object>> queryPhotoAlbum(Activity context) {

		List <HashMap<String, Object>> galleryList = new ArrayList <HashMap<String, Object>>();

		ContentResolver cr = context.getContentResolver();

		String[] columns = {Images.Media._ID, Images.Media.DATA, Images.Media.BUCKET_ID, Images.Media.BUCKET_DISPLAY_NAME, "COUNT(1) AS count"};
		String selection = "0==0) GROUP BY (" + Images.Media.BUCKET_ID;
		String sortOrder = Images.Media.DATE_MODIFIED;
		Cursor cur = cr.query(Images.Media.EXTERNAL_CONTENT_URI, columns, selection, null, sortOrder);

		if (cur.moveToFirst()) {
			int id_column = cur.getColumnIndex(Images.Media._ID);
			int image_id_column = cur.getColumnIndex(Images.Media.DATA);
			int bucket_id_column = cur.getColumnIndex(Images.Media.BUCKET_ID);
			int bucket_name_column = cur.getColumnIndex(Images.Media.BUCKET_DISPLAY_NAME);
			int count_column = cur.getColumnIndex("count");

			do {
				// Get the field values
				int id = cur.getInt(id_column);
				String image_path = cur.getString(image_id_column);
				int bucket_id = cur.getInt(bucket_id_column);
				String bucket_name = cur.getString(bucket_name_column);
				int count = cur.getInt(count_column);
				// Do something with the values.
				Log.i("whb ", "  id  =  "+id +"  image_path  =  "+
						image_path+"  bucket_id  = "+bucket_id+"   bucket_name  =  "+bucket_name);
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id", id);
				params.put("image_path", image_path);
				params.put("bucket_id", bucket_id);
				params.put("bucket_name", bucket_name);
				params.put("count", count);
				galleryList.add(params);
			} while (cur.moveToNext());
		}
		return  galleryList;
	}
}
