package com.yskj.ldm.activity.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

public class UpdatePasswordActivity extends NavigationActivity {

	EditText oldpassword,password,repassword;
	Button submitPassword;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.updatepassword_screen);
		registerHeadComponent();
		getRightButton().setVisibility(View.GONE);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("修改密码");	
		registerComponent();
	 
	}

	public void registerComponent()
	{
		oldpassword = (EditText)this.findViewById(R.id.edit_oldpassword);
		password = (EditText)this.findViewById(R.id.edit_password);
		repassword = (EditText)this.findViewById(R.id.edit_repassword);
		submitPassword = (Button)this.findViewById(R.id.submit_password);
		submitPassword.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) 
			{

				if(StringUtil.isNull(oldpassword.getText().toString()))
				{
					UpdatePasswordActivity.this.showToast("亲,旧密码不能为空");
				}
				else if(StringUtil.isNull(password.getText().toString())||StringUtil.isNull(repassword.getText().toString()))
				{
					UpdatePasswordActivity.this.showToast("亲,新密码不能为空");
				}
				else if (password.getText().toString().equals(oldpassword.getText().toString()))
				{
					UpdatePasswordActivity.this.showToast("亲,新旧密码不能相同");
				}
				else if(!password.getText().toString().equals(repassword.getText().toString()))
				{
					UpdatePasswordActivity.this.showToast("亲,新密码不一致");
				}
				else
				{
					ConnectionManager.getInstance().requestUpdatePassword(UpdatePasswordActivity.this.phone, oldpassword.getText().toString(), repassword.getText().toString(), true, UpdatePasswordActivity.this);
				}
			}
		});
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response)
	{
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))return;
	 
		JsonObject jo = JsonUtil.parseJson(data);
		Log.i("whb", " jo1--->  "+jo.toString());
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.USER_UPDATEPSW.equals(cmd))
			{
				UpdatePasswordActivity.this.showToast("密码修改成功");
				UpdatePasswordActivity.this.finish();
			}
		}
		else 
		{
			UpdatePasswordActivity.this.showToast(errorInfo);
		}
	}
}
