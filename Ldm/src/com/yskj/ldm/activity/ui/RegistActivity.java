package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.android.commu.parse.Response;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.home.ContainerHomeActivity;
import com.yskj.ldm.base.AppFrameActivity;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.UserBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.Encrypt;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;
import com.yskj.ldm.util.WIFIUtil;

public class RegistActivity extends NavigationActivity{
	private Button btn_register,btn_verify ;
	private EditText etphone,edit_nickname,edit_password,edit_repassword,register_verify;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.register_screen);
		this.registerHeadComponent();
		this.setHeadTitle("注册");
		this.getRightButton().setVisibility(View.INVISIBLE);
		initWithApiKey();
		etphone =  (EditText)this.findViewById(R.id.register_num);
		edit_nickname  =  (EditText)this.findViewById(R.id.register_nickname);
		edit_password  =  (EditText)this.findViewById(R.id.register_password);
		edit_repassword  =  (EditText)this.findViewById(R.id.register_repassword);
		register_verify = (EditText)this.findViewById(R.id.register_verify);
		btn_register =  (Button)this.findViewById(R.id.btn_register);
		btn_verify =  (Button)this.findViewById(R.id.btn_verify);
		btn_verify.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				onVerifyAction();
			}});

		btn_register.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0)
			{
				if(StringUtil.isNull(etphone.getText().toString()))
				{
					RegistActivity.this.showToast("亲,请输入您的电话号码!");
				}
				else if(StringUtil.isNull(edit_nickname.getText().toString()))
				{
					RegistActivity.this.showToast("亲,请输入您的昵称!");
				}
				else if(StringUtil.isNull(edit_password.getText().toString()))
				{
					RegistActivity.this.showToast("亲,请输入您的密码!");
				}
				else if(StringUtil.isNull(edit_repassword.getText().toString()))
				{
					RegistActivity.this.showToast("亲,请再次输入您的密码!");
				}
				else if(StringUtil.isNull(register_verify.getText().toString()))
				{
					RegistActivity.this.showToast("亲,请输入您的验证码!");
				}
				else
				{
					if(!edit_password.getText().toString().equals(edit_repassword.getText().toString()))
					{
						RegistActivity.this.showToast("两次密码不一致!");
						return ;
					}
					if(WIFIUtil.isNetworkConnected(RegistActivity.this))
					{
//						Map<String, Object> map = PreferencesService.getInstance(RegistActivity.this).getBaiduUserInfo();
//						String reAppid = map.get("appid").toString();
//						String reUserId = map.get("userid").toString();
//						String reChannelId = map.get("channelid").toString();
//						String reRequestId = map.get("requestid").toString();

//						if(!StringUtil.isNull(reAppid)&&!StringUtil.isNull(reChannelId)&&!StringUtil.isNull(reRequestId)&&!StringUtil.isNull(reUserId))
							ConnectionManager.getInstance().requestUserRegister(
									etphone.getText().toString().trim(), 
									edit_nickname.getText().toString().trim(),
									edit_password.getText().toString().trim(),
									register_verify.getText().toString().trim().trim(),
									DeviceUtil.getMobileID(RegistActivity.this), 
								 
									true,RegistActivity.this);
//						else
//						{
//							RegistActivity.this.showToast("获取云端ID失败..");
//						}
					}
					else
					{
						RegistActivity.this.showToast("亲!请连接网络..");
					}
				}
			}});
	}
	protected void onBackAction()
	{
		this.setResult(1);
		this.finish();
	}
	/*****************************************************************
	 * 网络请求回调
	 * @throws JSONException 
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		if(null ==data||"".equals(data))
		{
			return ;
		}
		JSONObject jo = JsonUtil.getJSON(data);

		String errorNo;
		try {
			errorNo = jo.get("errorNo").toString();
			String errorInfo = jo.get("errorInfo").toString();
	
			if(errorNo.equals(Command.RESULT_OK))
			{
				if(Command.USER_REGISTER.equals(cmd)){
					String token =  jo.getString("token");
					String time =  jo.getString("time");
					this.showToast("注册成功,开始体验!");
					PreferencesService.getInstance(RegistActivity.this).savaLoginType(4);
					String reList = jo.get("resList").toString();
					List<HashMap<String, Object>> list = JsonUtil.getList(reList);
					HashMap<String, Object> pramasMap = list.get(0);
					UserBean ub = new UserBean();
					ub.setAREA_ID(pramasMap.get("area_id").toString());
					ub.setCIRCLE_ID(pramasMap.get("circle_id").toString());
					ub.setUSER_PASSWORLD(pramasMap.get("user_passworld").toString());
					ub.setUSER_NIKENAME(pramasMap.get("user_nikename").toString());
					ub.setUSER_ADRESS(pramasMap.get("user_adress").toString());
					ub.setUSER_BIRTH(pramasMap.get("user_birth").toString());
					ub.setUSER_EMAIL(pramasMap.get("user_email").toString());
					ub.setUSER_HEADPORTTAIT(pramasMap.get("user_headporttait").toString());
					ub.setREG_DATE(pramasMap.get("reg_date").toString());
					ub.setUSER_STATE(pramasMap.get("user_state").toString());
					ub.setUSER_NAME(pramasMap.get("user_name").toString());
					ub.setUSER_QQ(pramasMap.get("user_qq").toString());
					ub.setUSER_PHONE(pramasMap.get("user_phone").toString());
					ub.setUSER_ID(pramasMap.get("user_id").toString());
					ub.setUSER_QQ(pramasMap.get("user_sex").toString());
					Integer versionno  = Integer.parseInt(StringUtil.isNull(pramasMap.get("versioncode").toString())?"0":pramasMap.get("versioncode").toString());
					pservice.saveUserInfo(ub.getUSER_ID(), ub.getCIRCLE_ID(), ub.getUSER_PHONE(), ub.getUSER_PASSWORLD(),
							ub.getUSER_NIKENAME(),ub.getUSER_ADRESS(),ub.getUSER_BIRTH(),
							ub.getUSER_QQ(),ub.getUSER_EMAIL(),ub.getAREA_ID(),ub.getREG_DATE(),
							ub.getUSER_SEX(),ub.getUSER_HEADPORTTAIT(),ub.getUSER_STATE());
                    //保存版本号
					pservice.savaVersion(versionno);
					//检测版本升级开发置为开
					pservice.savaVersionTag(false);
					ConnectionManager.getInstance().setToken(ub.getUSER_ID(), DeviceUtil.getMobileID(this),token, time);
					Log.i("register", "  register --> password =" +ub.getUSER_PASSWORLD());
					this.startActivity(ContainerHomeActivity.class);
					this.finish();
				}
				else if(Command.VERIFY_CODE.equals(cmd))
				{
					this.showToast("获取验证码");
					btn_verify.setEnabled(false);
					new Thread(new TimerThread()).start();
				}
			}		
			else
			{
				this.showToast(errorInfo);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void onVerifyAction(){

		String ucode = etphone.getText().toString();

		if(StringUtil.isMobile(ucode)){

			ConnectionManager.getInstance().requestVerify(ucode,edit_nickname.getText().toString(),edit_password.getText().toString(), true, RegistActivity.this);
		}	
		else if(StringUtil.isNull(edit_nickname.getText().toString()))
		{
			this.showToast("亲,请输入您的昵称!");
		}
		else if(StringUtil.isNull(edit_password.getText().toString()))
		{
			this.showToast("亲,请输入您的密码!");
		}
		else{
			showToast("请输入正确的手机号");
		}
	}



	/*********************************************************************************/

	private boolean exit = false;
	private int count =60;

	private class TimerThread implements Runnable {

		public void run(){

			while(!exit){
				try{
					Thread.sleep(1000);
					if(--count <=0){
						exit = false;
					}

					Message msg = new Message();
					msg.what = 100;
					msg.arg1 = count;
					sendMessage(msg);

				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	protected void onSubHandleAction(Message msg) {

		if(msg.what == 100){
			if(msg.arg1 >0){
				btn_verify.setText("请稍后("+msg.arg1+")");
			}else{
				btn_verify.setEnabled(true);
				btn_verify.setText("获取验证码");
			}
		}
	}
}
