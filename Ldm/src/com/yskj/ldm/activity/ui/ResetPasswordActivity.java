package com.yskj.ldm.activity.ui;
 

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

public class ResetPasswordActivity extends NavigationActivity
{
	private Button btnVerify,btnSure;
	private EditText etphone,etVerify;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.user_resetpassword_screen);
		registerHeadComponent();
		getRightButton().setVisibility(View.INVISIBLE);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("忘记密码");	 
		btnVerify =  (Button)this.findViewById(R.id.btn_verify);
		etphone =  (EditText)this.findViewById(R.id.edit_phone);
		etVerify  =  (EditText)this.findViewById(R.id.et_verify);
		btnSure =  (Button)this.findViewById(R.id.btn_sure);
		btnSure.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) 
			{
				if(StringUtil.isNull(etVerify.getText().toString().trim()))
				{
					ResetPasswordActivity.this.showToast("亲,验证码不能为空.");
				}
				else if (StringUtil.isNull(etphone.getText().toString().trim()))
				{
					ResetPasswordActivity.this.showToast("亲,手机号不能为空.");
				}
				else
				{
					ConnectionManager.getInstance().requestResetPassword(etphone.getText().toString().trim(), etVerify.getText().toString().trim(), true, ResetPasswordActivity.this);
				}
			}
		});

		btnVerify.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				onVerifyAction();
			}
		});
	}

	private void onVerifyAction(){

		String ucode = etphone.getText().toString();

		if(StringUtil.isMobile(ucode))
		{
			ConnectionManager.getInstance().requestResetPswVerfy(ucode, true,this);
		}	
		else
		{
			showToast("请输入正确的手机号");
		}
	}

	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		if(null ==data||"".equals(data))
		{
			return ;
		}
		JsonObject jo = JsonUtil.parseJson(data);
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.USER_RESETPSW.equals(cmd))
			{
				this.showToast("重置成功!密码正在发送到您手机.");
				this.finish();
			}
			else if(Command.RESET_VERIFY_CODE.equals(cmd))
			{
				this.showToast("亲,正在发送验证码到您手机.");
				btnVerify.setEnabled(false);
				new Thread(new TimerThread()).start();
			}
		}		
		else
		{
			this.showToast(errorInfo);
		}
	}

	/*********************************************************************************/

	private boolean exit = false;
	private int count =60;

	private class TimerThread implements Runnable {

		public void run(){

			while(!exit){
				try{
					Thread.sleep(1000);
					if(--count <=0){
						exit = false;
					}

					Message msg = new Message();
					msg.what = 100;
					msg.arg1 = count;
					sendMessage(msg);

				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	protected void onSubHandleAction(Message msg) {

		if(msg.what == 100){
			if(msg.arg1 >0){
				btnVerify.setText("请稍后("+msg.arg1+")");
			}else{
				btnVerify.setEnabled(true);
				btnVerify.setText("获取密码");
			}
		}
	}
}
