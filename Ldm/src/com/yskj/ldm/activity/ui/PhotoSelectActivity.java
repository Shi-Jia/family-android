package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.PhotoSelectAdapter;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.FileBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class PhotoSelectActivity extends NavigationActivity{


	private GridView gridView;
	private PhotoSelectAdapter photoSelectAdapter;
	ArrayList<FileBean> filebean = new ArrayList<FileBean>();
	ArrayList<Object> recordFileBean = new ArrayList<Object>();
	int startIndex ,endIndex;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photoselect_screen);
		registerHeadComponent();
		if(Session.getSession().get("jump_type").toString().equals("LaodaoQuanListActivity"))
			getRightButton().setBackgroundResource(R.drawable.next);
		else
			getRightButton().setBackgroundResource(R.drawable.uploadbtn);
		setHeadTitle("选择图片");
		gridView =  (GridView)this.findViewById(R.id.photo_select_grid);
		getSystemPhotoPath(Session.getSession().get("bucket_id").toString());
		photoSelectAdapter  = new PhotoSelectAdapter(this);
		photoSelectAdapter.setDataSource(filebean);
		photoSelectAdapter.setRecordFileBean(recordFileBean);
		gridView.setAdapter(photoSelectAdapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				photoSelectAdapter.chiceState(position);
			}
		});
	}

	/**
	 * 
	 */
	protected void onRightAction()
	{
		if(Session.getSession().get("jump_type").toString().equals("LaodaoQuanListActivity"))
		{
			Session.getSession().put("imagemap", recordFileBean);
			startActivity(UploadMoonActivity.class);
		}
		else
		{
			List<Map<String,Object>> attachs = new ArrayList<Map<String,Object>> ();
			for(int i = 0;i<recordFileBean.size();i++)
			{
				Map<String,Object> att2=new HashMap<String,Object>();
				att2.put("format",((FileBean)recordFileBean.get(i)).getFormat());
				att2.put("name",((FileBean)recordFileBean.get(i)).getName());
				att2.put("size", ((FileBean)recordFileBean.get(i)).getSize());
				Integer albumId = Integer.parseInt(Session.getSession().get("album_id").toString());
				att2.put("album_id", albumId);
				attachs.add(att2);
			}
			ConnectionManager.getInstance().requestUploadPhotoforAlbum(this.userid ,attachs, true, this);
		}
	}
 
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response)
	{
		switch(cmd)
		{
		case Command.PHO_ALBUM_UPLOAD:
			String data = ((AllResponse)response).getMsg();
			if(StringUtil.isNull(data))
			{
				return ;
			}
			JsonObject jo = JsonUtil.parseJson(data);
			String reList = jo.get("resList").toString();
			String errorNo = jo.get("errorNo").toString();
			String errorInfo = jo.get("errorInfo").toString();
			Integer interfaceId =  Integer.parseInt(jo.get("interfaceId").toString());
			List<HashMap<String, Object>> list = 	JsonUtil.getList(reList);
			if(errorNo.equals(Command.RESULT_OK))
			{
				if(interfaceId.equals(Command.PHO_SHARE))
				{
					startIndex = list.size();
					for(int i = 0 ;i<list.size();i++)
					{
						Map<String, Object> oMap = (Map<String, Object>)list.get(i);
						ConnectionManager.getInstance().requestUploadFile(oMap.get("urltoken").toString(),filebean.get(i).getLocalpath(),true,this);
					}
				}
			}
			else
			{
				this.showToast(errorInfo);
			}
			break;
		case Command.PHO_UPLOAD:
			endIndex++;
			if(startIndex==endIndex)
			{
				this.showToast("上传"+endIndex+"图片成功.");
				startIndex = 0;
				endIndex= 0;
				this.finish();
			}
			break;
		}
	}
	public void getSystemPhotoPath(String bucketId)
	{
		Uri mUri = Uri.parse("content://media/external/images/media");
		Uri mImageUri = null;
		String selection =  Images.Media.BUCKET_ID+" = " +bucketId ;
		@SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, selection, null,
				MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			int ringtoneID = cursor.getInt(cursor
					.getColumnIndex(MediaStore.MediaColumns._ID));
			int size = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns.SIZE));
			String format = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE));
			String name  = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.TITLE));
			String data = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));
			mImageUri = Uri.withAppendedPath(mUri, "" + ringtoneID);
			FileBean fb = new FileBean();
			fb.setFileUri(mImageUri.toString());
			fb.setLocalpath(data);
			fb.setFormat(format);
			fb.setSize(size+"");
			fb.setName(name);
			filebean.add(fb);
			Log.i("whb","format -->" +fb.getFormat() + " size -->  " +fb.getSize()+   " name -->  " +fb.getName());
			cursor.moveToNext();
		}
	}
}
