package com.yskj.ldm.activity.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.yskj.ldm.R;
import com.yskj.ldm.activity.view.SwitchView;
import com.yskj.ldm.activity.view.SwitchView.OnCheckedChangeListener;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.util.DeviceUtil;

public class SettingActivity  extends NavigationActivity implements OnCheckedChangeListener{

	SwitchView sv = null;
	RelativeLayout	switch_relayout,curversion ;
	RelativeLayout  exitSystem,updataPassword ;
	ImageView updateNew;
	Integer versionCode;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_screen);
		registerHeadComponent();
		getRightButton().setVisibility(View.INVISIBLE);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("设置");
		registerSwitch();
		versionCode = DeviceUtil.getVersionCode(this);
		exitSystem = (RelativeLayout)this.findViewById(R.id.exit_system);
		curversion = (RelativeLayout)this.findViewById(R.id.curversion);
		updataPassword = (RelativeLayout)this.findViewById(R.id.updata_password);
		updateNew = (ImageView)this.findViewById(R.id.display_new);
		if(SettingActivity.this.pservice.getVersion()>versionCode)
			updateNew.setVisibility(View.VISIBLE);
		exitSystem.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) 
			{			 
				SettingActivity.this.showAlertDialog("取消", "确定","亲,您不再留会儿了吗","唠叨觅温馨提示");
			}});
		curversion.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				Log.i("version", "version  --> "+SettingActivity.this.pservice.getVersion());
				Log.i("version", "version  --> "+versionCode);
				if(SettingActivity.this.pservice.getVersion()>versionCode)
				{
					SettingActivity.this.startActivity(UpdateVesionActivity.class);
				}
				else 
				{
					SettingActivity.this.showToast("当前是最新版本");
				}
			}});
		updataPassword.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) 
			{			 
				SettingActivity.this.startActivity(UpdatePasswordActivity.class);
			}});
	}

	@Override
	protected void dialogOnCancel() {

		super.dialogOnCancel();
	}

	@Override
	protected void dialogOnSure() {
		SettingActivity.this.pservice.saveLogin(false, false);
		if(SettingActivity.this.pservice.getLoginType().equals(1))
		{
			mQQAuth.logout(this);
		}
		SettingActivity.this.pservice.savaLoginType(0);
		SettingActivity.this.finish();
		super.dialogOnSure();
	}

	public void registerSwitch()
	{
		sv = new SwitchView(this);
		sv.setOnCheckedChangeListener(this);
//		sv.setChecked(this.pservice.getPushTag());
		sv.changeChecked(this.pservice.getPushTag());
        Log.i("getPushTag", "  getPushTag--->  "+this.pservice.getPushTag());
		switch_relayout = (RelativeLayout)this.findViewById(R.id.switch_relayout);

		RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		lp2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

		lp2.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);

		lp2.setMargins(0, 0, 20, 0);

		sv.setLayoutParams(lp2);
		switch_relayout.addView(sv);
	}

	@Override
	public void onCheckedChanged(boolean isChecked) {

//		if(isChecked)
//		{
//			SettingActivity.this.initWithApiKey();
//		}
//		else
//		{
//			SettingActivity.this.stopBaiduPush();
//		}
//		this.pservice.savaPushTag(isChecked);
//		this.showToast(isChecked+"");
	}
}
