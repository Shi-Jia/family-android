package com.yskj.ldm.activity.ui;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.android.commu.net.HandlerException;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.LaoDaoQuanListAdapter;
import com.yskj.ldm.activity.adapter.LaoDaoQuanAdapter;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.activity.impl.PushUpdata;
import com.yskj.ldm.activity.impl.FamilyOnClickListener;
import com.yskj.ldm.activity.view.PullToRefreshView;
import com.yskj.ldm.activity.view.PullToRefreshView.OnFooterRefreshListener;
import com.yskj.ldm.activity.view.PullToRefreshView.OnHeaderRefreshListener;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.CommentsBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.SDCardStoreManager;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class LaodaoQuanListActivity extends NavigationActivity implements FamilyOnClickListener,OnHeaderRefreshListener,OnFooterRefreshListener{

	private LaoDaoQuanAdapter adapter;
	private PopupWindow topDialog;
	private Button paizhao,piliangshangchuan,cancel;
	private RelativeLayout pinglun;
	private PullToRefreshView mPullToRefreshView;
	private File file =  null;
	private static final int TAKE_BIG_PICTURE = 1;
	public  EditText  etComments;
	private List<CommentsBean> recordCommentsString  = new  ArrayList<CommentsBean>();
	private int positon,contentId;
	private boolean isHeader,isFooter;
	private ListView list ;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.laodaoquan_list_screen);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.add_normal);
		//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("唠叨圈");
		registerComponent();


	}
	protected void onBackAction()
	{
		this.setResult(2);
		this.finish();
	}
	private void registerComponent(){
		pinglun  = (RelativeLayout)this.findViewById(R.id.laodapquan_edit);
		etComments = 	(EditText)pinglun.findViewById(R.id.edit_pinglun);
		Button send = (Button)pinglun.findViewById(R.id.btn_send_pinglun);
		send.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				String pingluncontent = etComments.getText().toString();
				ConnectionManager.getInstance().requestCommentsInsert( LaodaoQuanListActivity.this.userid, contentId, 0, pingluncontent, true, LaodaoQuanListActivity.this);
			}
		});
		View view = (View)LayoutInflater.from(this).inflate(R.layout.topdialog_item, null);
		topDialog = new PopupWindow(view,LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		getRightButton().setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				topDialog.showAtLocation(LaodaoQuanListActivity.this.getWindow().getDecorView(), Gravity.BOTTOM, 0, 0);
			}
		} );
		paizhao = (Button)view.findViewById(R.id.toppaizhao);
		piliangshangchuan = (Button)view.findViewById(R.id.piliangshangchuan);
		cancel = (Button)view.findViewById(R.id.cancel);
		paizhao.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) {
				if(SDCardStoreManager.getInstance().checkSDCardState())
				{
					topDialog.dismiss();
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					if(null!=file)file = null;
					file = SDCardStoreManager.getInstance().getFilePath(SDCardStoreManager.getInstance().getRootPath()+RecordsPath.ImageRoot,System.currentTimeMillis()+".jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)); 
					intent.putExtra("return-data", true);
					intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
					startActivityForResult(intent, TAKE_BIG_PICTURE);  
				}
				else
				{
					LaodaoQuanListActivity.this.showToast("亲,请插入SDCard进入拍照");
				}
			}
		});
		piliangshangchuan.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
				Session.getSession().put("jump_type", "LaodaoQuanListActivity");
				LaodaoQuanListActivity.this.startActivity(PhotoSelectActivity.class);
			}
		});
		cancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
			}
		});
		//		
		mPullToRefreshView = (PullToRefreshView)findViewById(R.id.main_pull_refresh_view);
		list = (ListView)findViewById(R.id.listview);
//		list.setOnScrollListener(new OnScrollListener(){
//
//			@Override
//			public void onScroll(AbsListView arg0, int arg1, int arg2,
//					int arg3) {
//				if(LaodaoQuanListActivity.this.pinglun.getVisibility() ==View.VISIBLE)
//					LaodaoQuanListActivity.this.pinglun.setVisibility(View.INVISIBLE);
// 
//			}
//
//			@Override
//			public void onScrollStateChanged(AbsListView arg0, int arg1) {
//				if(LaodaoQuanListActivity.this.pinglun.getVisibility() ==View.VISIBLE)
//					LaodaoQuanListActivity.this.pinglun.setVisibility(View.INVISIBLE);
//
//			}});
		mPullToRefreshView.setOnHeaderRefreshListener(this);
		mPullToRefreshView.setOnFooterRefreshListener(this);
		adapter = new LaoDaoQuanAdapter(this);

		adapter.setReflash(this);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Toast.makeText(LaodaoQuanListActivity.this, "positon = "+position, 1000).show();
				Session.getSession().put("contentid",((Map<String, Object>)adapter.getItem(position)).get("content_id"));
				LaodaoQuanListActivity.this.startActivity(CommentDetailActivity.class);
			}});
		ConnectionManager.getInstance().requestDownloadMoon(LaodaoQuanListActivity.this.circleid, "", adapter.param++,5, true, LaodaoQuanListActivity.this);
	}

	private OnScrollListener scrollerListener = new OnScrollListener() {

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {

				if (!adapter.isloading && view.getLastVisiblePosition() == view.getCount() - 1) {
					adapter.isloading = true;
					ConnectionManager.getInstance().requestDownloadMoon(LaodaoQuanListActivity.this.circleid, "", adapter.param,5, true, LaodaoQuanListActivity.this);
					adapter.param++;
				}
			}
		}
	};
	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{ 
		super.onActivityResult(requestCode, resultCode, data);  
		if (resultCode == Activity.RESULT_OK) { 
			if(requestCode == TAKE_BIG_PICTURE){
				if(null!=file){
					Session.getSession().put("imagepath", file.getPath());
					LaodaoQuanListActivity.this.startActivity(CameraPicActivity.class);
				}
			}
		}
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))return;

		JsonObject jo = JsonUtil.parseJson(data);
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		//		Integer  interfaceId = Integer.parseInt(jo.get("interfaceId").toString()); 
		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.DOWN_MOON.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				Log.i("resList", "  resList   --> " +reList);
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				if(list.size()>0)
				{
					for(int i = 0;i< list.size();i++)
					{
						List<HashMap<String, Object>> comments =JsonUtil.getList(list.get(i).get("comments").toString());
						for(int j = 0;j<comments.size();j++)
						{
							CommentsBean   cb = new CommentsBean();
							String content = comments.get(j).get("comments_content").toString();
							String name = comments.get(j).get("user_nikename").toString();
							cb.setName(name);
							cb.setContent(content);
							cb.setPostion(((adapter.param-1)*5)+i);
							//							Log.i("position ", "   position--- > " + cb.getPostion());
							recordCommentsString.add(cb);
						}
						adapter.add(i, recordCommentsString);
					}
					if(this.isHeader) 
						adapter.getList().clear();

					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					adapter.param --;
					this.showToast("亲,没有更多心情了哦.");  
				}
				if(this.isHeader)
				{
					mPullToRefreshView.onHeaderRefreshComplete();
					this.isHeader = false;
					PreferencesService.getInstance(this).savaPushCircle(0);
				}
				else if(this.isFooter)
				{
					mPullToRefreshView.onFooterRefreshComplete();
					this.isFooter = false;
				}
				else 
				{
					PreferencesService.getInstance(this).savaPushCircle(0);
				}
			} 
			else if(Command.MOON_COMMENTS.equals(cmd))
			{
				this.showToast("评论成功");  
				String pingluncontent = etComments.getText().toString();
				CommentsBean   cb = new CommentsBean();
				cb.setPostion(LaodaoQuanListActivity.this.positon);
				cb.setContent(pingluncontent);
				cb.setName(pservice.getUserInfoPerferences().get("nickname").toString());
				recordCommentsString.add(0, cb);
				//				recordCommentsString.add(cb);
				adapter.add(LaodaoQuanListActivity.this.positon, recordCommentsString);
				adapter.notifyDataSetChanged();
				pinglun.setVisibility(View.GONE);
				etComments.setText("");
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
	/**
	 * 网络异常处理
	 */
	protected void onErrorAction(int cmd,HandlerException e){
		if(cmd ==Command.DOWN_MOON){
			adapter.isloading = false;
		}
		super.onErrorAction(cmd,e);
	}

	@Override
	public void onComments(Integer contentId ,int positon) {
		this.positon = positon;
		this.contentId = contentId;
		this.pinglun.setVisibility(View.VISIBLE);
	}

	@Override
	public void onFooterRefresh(PullToRefreshView view) {
		adapter.isloading = true;
		this.isFooter = true;
		ConnectionManager.getInstance().requestDownloadMoon(LaodaoQuanListActivity.this.circleid, "", adapter.param,5, false, LaodaoQuanListActivity.this);
		adapter.param++;
	}

	@Override
	public void onHeaderRefresh(PullToRefreshView view) {
		isHeader = true;
		adapter.param = 0;
		ConnectionManager.getInstance().requestDownloadMoon(LaodaoQuanListActivity.this.circleid, "", adapter.param++,5, false, LaodaoQuanListActivity.this);
	}

}
