package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.R.color;
import com.yskj.ldm.activity.adapter.ShareMoonPicAdapter;
import com.yskj.ldm.activity.home.ContainerHomeActivity;
import com.yskj.ldm.activity.view.MyGridView;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.CircleBean;
import com.yskj.ldm.beans.FileBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class UploadMoonActivity extends NavigationActivity{


	EditText edit_content;
	MyGridView  photo_moon_grid;
	ArrayList<FileBean> filebean = null;
	LinearLayout btnSeeCircle;

	TextView seeCircleTxt;
	int startIndex ,endIndex;
	String circleIds="";
	String circleName="";
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.upload_moon_view);
		registerHeadComponent();
		setHeadTitle("心情分享");
		getRightButton().setBackgroundResource(R.drawable.rectangle);
		btnSeeCircle = (LinearLayout)this.findViewById(R.id.btn_seecircle);
		edit_content = (EditText)this.findViewById(R.id.edit_content);
		photo_moon_grid  = (MyGridView)this.findViewById(R.id.photo_moon_grid);

		//		seeCircle  = (LinearLayout)this.findViewById(R.id.see_circle);
		seeCircleTxt = (TextView)this.findViewById(R.id.see_circle_txt);
		filebean = (ArrayList<FileBean>)Session.getSession().get("imagemap");
		ShareMoonPicAdapter smp = new ShareMoonPicAdapter(this);
		smp.setDataSource(filebean);
		photo_moon_grid.setAdapter(smp);
		btnSeeCircle.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				//				Session.getSession().put("see_circle", "");
				UploadMoonActivity.this.startActivityForResult(FamilyCircleActivity.class, 1); 
			}});
	}

	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{ 
		super.onActivityResult(requestCode, resultCode, data);  
		if (resultCode == Activity.RESULT_OK)
		{ 
			List<CircleBean> recordlist = (List<CircleBean>)Session.getSession().get("see_circle");
			circleIds="";
			circleName="";
			for(int i = 0;i<recordlist.size();i++)
			{      	
				circleIds +=recordlist.get(i).getCircle_id()+",";
				circleName +="@"+recordlist.get(i).getCircle_name()+" ";
			}
			if(!StringUtil.isNull(circleIds))
			{
				circleIds  = circleIds.substring(0, circleIds.length()-1);
			}
			else 
			{
				circleIds = this.circleid;
			}
			seeCircleTxt.setText(circleName);	
			Log.i("whb ", "   circleIds  =  "+circleIds);
		}
	}
	/**
	 * 
	 */
	protected void onRightAction()
	{

		String content = edit_content.getText().toString();
		if(StringUtil.isNull(circleIds))
		{
			this.showToast("圈子不能为空,请选择圈子.");	
			return ;
		}
		if(StringUtil.isNull(content)&&filebean.size()==0)
		{
			this.showToast("亲,图片和文字至少分享一项内容");	
			return ;
		}
		List<Map<String,Object>> attachs = new ArrayList<Map<String,Object>> ();

		for(int i = 0;i<filebean.size();i++)
		{
			Map<String,Object> att2=new HashMap<String,Object>();
			att2.put("format",filebean.get(i).getFormat());
			att2.put("name",filebean.get(i).getName());
			att2.put("size", filebean.get(i).getSize());
			att2.put("album_id", 0);
			attachs.add(att2);
		}

		Log.i("circleIds", " circleIds  ---> " +circleIds);
		ConnectionManager.getInstance().requestShareDoMoon(this.userid,circleIds,pservice.getUserInfoPerferences().get("nickname").toString(), content, 1f, 1f, attachs, true, this);

	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response)
	{
		switch(cmd)
		{
		case Command.PHO_SHARE:
			String data = ((AllResponse)response).getMsg();
			Log.i("data", "data -->  "+data);
			if(StringUtil.isNull(data))
			{
				return ;
			}
			JsonObject jo = JsonUtil.parseJson(data);
			String reList = jo.get("resList").toString();
			String errorNo = jo.get("errorNo").toString();
			String errorInfo = jo.get("errorInfo").toString();
			Integer interfaceId =  Integer.parseInt(jo.get("interfaceId").toString());
			List<HashMap<String, Object>> list = 	JsonUtil.getList(reList);
			if(errorNo.equals(Command.RESULT_OK))
			{
				if(interfaceId.equals(Command.PHO_SHARE))
				{
					startIndex = list.size();
					for(int i = 0 ;i<list.size();i++)
					{
						Map<String, Object> oMap = (Map<String, Object>)list.get(i);
						ConnectionManager.getInstance().requestUploadFile(oMap.get("urltoken").toString(),filebean.get(i).getLocalpath(),true,this);
					}
					if(list.size()==0)
					{
						this.showToast("分享成功");
						Intent intent =new Intent();
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
						intent.setClass(this, ContainerHomeActivity.class);
						this.startActivity(intent);
//						this.finish();
					}
				}
			}
			else
			{
				this.showToast(errorInfo);
			}
			break;
		case Command.PHO_UPLOAD:
			endIndex++;
			if(startIndex==endIndex)
			{
				this.showToast("分享第"+endIndex+"图片成功");
				ConnectionManager.getInstance().requestTransformCircle(2011, this.circleid, true, this);
				startIndex = 0;
				endIndex= 0;
				Intent intent =new Intent();
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
				intent.setClass(this, ContainerHomeActivity.class);
				this.startActivity(intent);
//				this.finish();
			}
			break;
		}
	}
}
