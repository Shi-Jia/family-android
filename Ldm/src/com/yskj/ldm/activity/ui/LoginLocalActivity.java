package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.commu.parse.Response;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.home.ContainerHomeActivity;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.UserBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.Encrypt;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

public class LoginLocalActivity extends NavigationActivity {

	EditText localName,localPsw;
	Button localLoginBtn;
	TextView wangjimima;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.login_local_screen);
		registerHeadComponent();
		getRightButton().setText("注册");
		setHeadTitle("登陆");
		this.registerComponent();
	}
	protected void onRightAction()
	{
		this.startActivityForResult(RegistActivity.class,1);
		getRightButton().setTextColor(Color.WHITE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode ==Activity.RESULT_OK)
		{
			getRightButton().setTextColor(Color.WHITE);
		}
	}
	public void registerComponent()
	{
		localName = (EditText)this.findViewById(R.id.login_local_username);
		localPsw = (EditText)this.findViewById(R.id.login_local_username_pwd);
		localLoginBtn = (Button)this.findViewById(R.id.login_local_btn_login);
		wangjimima  = (TextView)this.findViewById(R.id.wangjimima);
		wangjimima.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				LoginLocalActivity.this.startActivity(ResetPasswordActivity.class);
			}});
		localLoginBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {

				if(StringUtil.isNull(localName.getText().toString())||StringUtil.isNull(localPsw.getText().toString()) )
				{
					LoginLocalActivity.this.showToast("请输入有效的用户名密码!");
				}
				else
				{
					String user_name = localName.getText().toString().trim();
					String password =localPsw.getText().toString().trim();
					if(!LoginLocalActivity.this.password.equals(password))
						password = Encrypt.md5(password) ;
					ConnectionManager.getInstance().requestLogin( user_name,password,DeviceUtil.getMobileID(LoginLocalActivity.this),
							true, LoginLocalActivity.this);
				}
			}});
		if(!StringUtil.isNull(phone))
		{
			localName.setText(phone);
		}
		if(!StringUtil.isNull(password))
		{
			localPsw.setText(this.password);
		}
	}

	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))
		{
			return ;
		}
		Log.i("whb", "data -->  "+data);
		try {
			JSONObject jo = JsonUtil.getJSON(data);
			String	errorNo = jo.get("errorNo").toString();
			String errorInfo = jo.getString("errorInfo");

			if(Command.USER_LOGIN.equals(cmd))
			{
				if(errorNo.equals(Command.RESULT_OK)){
					LoginLocalActivity.this.showToast("登陆成功");
					PreferencesService.getInstance(LoginLocalActivity.this).savaLoginType(4);
					String token =  jo.getString("token");
					String time =  jo.getString("time");
					String reList = jo.get("resList").toString();
					List<HashMap<String, Object>> list = JsonUtil.getList(reList);
					HashMap<String, Object> pramasMap = list.get(0);

					UserBean ub = new UserBean();
					ub.setAREA_ID(pramasMap.get("area_id").toString());
					ub.setCIRCLE_ID(pramasMap.get("circle_id").toString());
					ub.setUSER_PASSWORLD(pramasMap.get("user_passworld").toString());
					ub.setUSER_NIKENAME(pramasMap.get("user_nikename").toString());
					ub.setUSER_ADRESS(pramasMap.get("user_adress").toString());
					ub.setUSER_BIRTH(pramasMap.get("user_birth").toString());
					ub.setUSER_EMAIL(pramasMap.get("user_email").toString());
					ub.setUSER_HEADPORTTAIT(pramasMap.get("user_headporttait").toString());
					ub.setREG_DATE(pramasMap.get("reg_date").toString());
					ub.setUSER_STATE(pramasMap.get("user_state").toString());
					ub.setUSER_NAME(pramasMap.get("user_name").toString());
					ub.setUSER_QQ(pramasMap.get("user_qq").toString());
					ub.setUSER_PHONE(pramasMap.get("user_phone").toString());
					ub.setUSER_ID(pramasMap.get("user_id").toString());
					ub.setUSER_QQ(pramasMap.get("user_sex").toString());
					Integer versionno  = Integer.parseInt(StringUtil.isNull(pramasMap.get("versioncode").toString())?"0":pramasMap.get("versioncode").toString());

					pservice.saveUserInfo(ub.getUSER_ID(), ub.getCIRCLE_ID(), ub.getUSER_PHONE(), ub.getUSER_PASSWORLD(),
							ub.getUSER_NIKENAME(),ub.getUSER_ADRESS(),ub.getUSER_BIRTH(),
							ub.getUSER_QQ(),ub.getUSER_EMAIL(),ub.getAREA_ID(),ub.getREG_DATE(),
							ub.getUSER_SEX(),ub.getUSER_HEADPORTTAIT(),ub.getUSER_STATE());
					pservice.savaVersion(versionno);
					ConnectionManager.getInstance().setToken(ub.getUSER_ID(),DeviceUtil.getMobileID(LoginLocalActivity.this), token, time);
					pservice.savaVersionTag(false);
					//					if(!this.pservice.getPushTag())
					//						LoginLocalActivity.this.stopBaiduPush();
					LoginLocalActivity.this.startActivity(ContainerHomeActivity.class);
					LoginLocalActivity.this.finish();
				}
				else
					LoginLocalActivity.this.showToast(errorInfo);	
			}
		} catch (JSONException e) {

			e.printStackTrace();
		}
	}
}
