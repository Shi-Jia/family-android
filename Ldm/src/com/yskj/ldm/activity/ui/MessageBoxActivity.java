package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.android.commu.net.ICommuDataListener;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.ChattingAdapter;
import com.yskj.ldm.activity.adapter.MassageBoxAdapter;
import com.yskj.ldm.activity.impl.PushUpdata;
import com.yskj.ldm.activity.view.JListView;
import com.yskj.ldm.activity.view.JListView.IXListViewListener;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.services.BCSPushMessageReceiver;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

public class MessageBoxActivity  extends NavigationActivity   implements  IXListViewListener{

	private MassageBoxAdapter adapter;

	private JListView  messageList;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.messagebox_list);
		registerHeadComponent();
		//		getRightButton().setBackgroundResource(R.drawable.add_normal);
		setHeadTitle("消息盒子");
		this.getBackButton().setVisibility(View.INVISIBLE);
		//		BCSPushMessageReceiver.setPushUpdata(this);
		registerComponent();
	}
	private void onLoad() {
		messageList.stopRefresh();
		messageList.stopLoadMore();
		messageList.setRefreshTime("刚刚");
	}
	protected void onRightAction()
	{

	}
	public void registerComponent()
	{
		messageList = (JListView)findViewById(R.id.message_listview);

 
		messageList.setPullLoadEnable(true);
		messageList.setXListViewListener(this);
		adapter =  new MassageBoxAdapter(this,this);
		messageList.setAdapter(adapter);

		ConnectionManager.getInstance().requestMsgboxQueryByUserid(this.userid, 0, 10, true, this);
	}

	//	protected void onMessageBox(String type) {
	//
	//	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.QUERY_MASSAGEBOX.equals(cmd))
			{
				//				this.showToast("创建成功");  
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				if(list.size()>0)
				{  
					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					adapter.param --;
					this.showToast("亲,没有更多消息了哦.");  
				}
				this.onLoad();
			}
			else if(Command.CIRC_APPLY_RESPONSE.equals(cmd))
			{
				this.showToast("添加成功");  
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
	@Override
	public void onRefresh() {
		adapter.getList().clear();
 
		adapter.param= 0;
		ConnectionManager.getInstance().requestMsgboxQueryByUserid(this.userid, adapter.param++, 10, true, this);

	}
	@Override
	public void onLoadMore() {
		ConnectionManager.getInstance().requestMsgboxQueryByUserid(this.userid, adapter.param,10, false,this);
		adapter.param++;
	}
}
