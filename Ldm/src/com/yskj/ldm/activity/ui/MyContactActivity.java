package com.yskj.ldm.activity.ui;

import java.util.ArrayList;
import java.util.HashMap;

import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.MyContactAdapter;
import com.yskj.ldm.activity.view.LetterView;
import com.yskj.ldm.activity.view.LetterView.OnTouchingLetterListener;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.ContactElement;
import com.yskj.ldm.util.DeviceContactManager;
import com.yskj.ldm.util.Session;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

 

public class MyContactActivity extends NavigationActivity {

	
	private WindowManager mWindowManager;
	private TextView mDialogText;
	
	private ListView mListView;
	private ArrayList<ContactElement> elements;
	private MyContactAdapter adapter;
	
	private HashMap<String,ContactElement> contactHash;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_contact_search);
		
		contactHash = (HashMap<String,ContactElement>)Session.getSession().get("contact");

		registerHeadComponent();
		setHeadTitle("通讯录");
		getRightButton().setVisibility(View.GONE);
		
		registerComponent();
		
	}

	
	private void registerComponent(){
		
		elements = DeviceContactManager.getInstance(this).getContactArr();

		mListView = (ListView) this.findViewById(R.id.contact_list);
		mListView.setOnItemClickListener(listener);
		
		adapter = new MyContactAdapter(this);
		adapter.setDataSource(elements);
		
		mListView.setAdapter(adapter);
		
		LetterView letterView = (LetterView) findViewById(R.id.letter_view);
		letterView.setOnTouchLetterListener(new OnTouchingLetterListener(){

			@Override
			public void onTouchingLetter(String s) {
				// TODO Auto-generated method stub
				onLetterAction(s);
			}
			
		} );
		
		mDialogText = (TextView) LayoutInflater.from(this).inflate(R.layout.list_position, null);
		mDialogText.setVisibility(View.INVISIBLE);
		
		mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_APPLICATION,
				WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
						| WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
				PixelFormat.TRANSLUCENT);
		
		mWindowManager.addView(mDialogText, lp);
		
		
		Button btn = (Button) findViewById(R.id.btn_add);
		btn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				onSureAction();
			}
		});
	}
	
	
	private OnItemClickListener listener = new OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			// TODO Auto-generated method stub
			
			ContactElement contactEl = (ContactElement)adapter.getItem(position);
			contactEl.checked = !contactEl.checked;
			
			adapter.notifyDataSetChanged();
		}
	};
	
	
	
	private void onLetterAction(String c){
		
		ArrayList<ContactElement> arr = adapter.getDataSource();
		
		if(arr != null){
			
			int size = arr.size();
			for(int i=0;i<size;i++){
				
				ContactElement item = (ContactElement)arr.get(i);
				
				if(item.letter.startsWith(c.toLowerCase())){
					mListView.setSelection(i);
					return ;
				}
			}
		}
	}
	
	
	private void onSureAction(){
		
		for(ContactElement el :elements){
			
			if(el.checked){
				contactHash.put(el.getPhoneNum(), el);
			}else{
				contactHash.remove(el.getPhoneNum());
			}
		}

		this.setResult(200);
		onBackAction();
	}
	
	
	
	/*
	
	public void getContact(Context context) {
		String[] projection = new String[] { StructuredName.GIVEN_NAME,
				StructuredName.MIDDLE_NAME, StructuredName.FAMILY_NAME,
				ContactsContract.Data.RAW_CONTACT_ID, Phone.NUMBER };
		Cursor cursor = context.getContentResolver().query(
				ContactsContract.Data.CONTENT_URI,
				projection,
				ContactsContract.Data.MIMETYPE + " = '"
						+ StructuredName.CONTENT_ITEM_TYPE + "'", null,
				"sort_key asc");
		int i = 0;
		int total = cursor.getCount();
		String userRawId[] = new String[total];
		elements = new ArrayList<ContactElement>();
		for (cursor.moveToFirst(); !(cursor.isAfterLast()); cursor.moveToNext()) {
			userRawId[i] = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID));
			ContactElement element = new ContactElement();
			element.setName(cursor.getString(cursor
					.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
			element.setPhoneNum(getPhoneNum(userRawId[i])[0]);
			getFavorite(userRawId[i]);
			elements.add(element);
			i++;
		}

		cursor.close();
	}

	private String[] getPhoneNum(String userID) {
		String[] phoneNum = null;
		String[] projection = { ContactsContract.CommonDataKinds.Phone.NUMBER };
		Cursor cursor = this.getContentResolver().query(
				ContactsContract.Data.CONTENT_URI,
				projection,
				ContactsContract.Data.MIMETYPE + " = '"
						+ Phone.CONTENT_ITEM_TYPE + "' and "
						+ ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID
						+ " = '" + userID + "'", null, null);
		int total = cursor.getCount();
		phoneNum = new String[total];
		int i = 0;
		for (cursor.moveToFirst(); !(cursor.isAfterLast()); cursor.moveToNext()) {
			phoneNum[i] = cursor.getString(cursor.getColumnIndex(Phone.NUMBER));

			i++;
		}
		return phoneNum;
	}

	private int getFavorite(String userId) {
		int currentStarrede = 0;

		String[] projection = new String[] { ContactsContract.Contacts.STARRED };

		ContentResolver cResolver = this.getContentResolver();

		Cursor startCursorWith = cResolver.query(
				ContactsContract.Data.CONTENT_URI, projection,
				ContactsContract.Data.RAW_CONTACT_ID + " = " + userId, null,
				null);

		if (startCursorWith.getCount() > 0) {
			startCursorWith.moveToFirst();
			currentStarrede = startCursorWith.getInt(0);
		}
		return currentStarrede;
	}


	private void backAction() {

		List<ContactElement> contents = new ArrayList<ContactElement>();
		for (int i = 0; i < list.size(); i++) {
			Content content = list.get(i);

			if (content.isSelected()) {
				contents.add(elements.get(i));
			}
		}
		mWekApplication.mTransferObject = contents;
		setResult(RESULT_OK);
		finish();
	}*/
}
