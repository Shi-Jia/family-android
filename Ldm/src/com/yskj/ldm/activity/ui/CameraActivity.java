package com.yskj.ldm.activity.ui;

 
import java.util.Map;

 
import android.os.Bundle;
import android.os.Message;
 
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.android.commu.parse.Response;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.view.CameraView;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.util.Session;
 
public class CameraActivity extends NavigationActivity
{
	ImageView back,lvjing,paizhao;
	CameraView mCameraView;
	String imagePath;
	boolean isFlash = false;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.camera_view);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.homeshezhi);
		getBackButton().setBackgroundResource(R.drawable.noblink_normal);
		setHeadTitle("觅照");
		back  = (ImageView)this.findViewById(R.id.camera_back);
		lvjing   = (ImageView)this.findViewById(R.id.lvjing);
		paizhao = (ImageView)this.findViewById(R.id.paizhao);
		mCameraView = (CameraView)this.findViewById(R.id.mSurfaceView1);
		mCameraView.setActivity(this);

		paizhao.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				mCameraView.tackPicture();
			}
		});
		back.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				CameraActivity.this.finish();
			}
		});
	}
	protected void onBackAction()
	{
		if(isFlash)
		{
			mCameraView.openFlash();
			isFlash =false;
		}
		else
		{
			mCameraView.closeFlash();
			isFlash = true;

		}
	}
	protected void onSubHandleAction(Message msg)
	{
		imagePath = msg.obj.toString();
		Session.getSession().put("imagepath", imagePath);
		CameraActivity.this.startActivity(CameraPicActivity.class);
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){
		//		Log.e("token", "TOKEN   = " +CommuConst.Request_Token);
		//		this.showToast(((AllResponse)response).getMsg());
		//		if(cmd == CommuConst.Request_Token)
		//		{
		//			AllResponse resp = (AllResponse)response;
		//			Log.e("token", "getStatusCode   = " +resp.getStatusCode());
		//			if(resp.getStatusCode() == CommuConst.Result_OK)
		//			{
		//
		//				this.showToast(resp.getMsg());
		//			}
		//			else if (cmd == CommuConst.Request_UploadFile)
		//			{
		//				this.showToast("Request_UploadFile");
		//			}
		//			else  
		//			{
		//				this.showToast(resp.getMsg());
		//				Map JsonParams = JsonUtil.toMap(resp.getMsg());
		//				String data = JsonParams.get("token").toString();
		//				ConnectionManager.getInstance().requestUploadFile(data, imagePath, true, this);
		//
		//			}
		//		}
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
