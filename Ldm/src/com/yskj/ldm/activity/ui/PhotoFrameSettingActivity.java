package com.yskj.ldm.activity.ui;

import android.os.Bundle;
import android.view.View;

import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;

public class PhotoFrameSettingActivity extends NavigationActivity{
	
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.photoframe_setting_screen);
		registerHeadComponent();
		getRightButton().setVisibility(View.INVISIBLE);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("相框设置");	 
	}

}
