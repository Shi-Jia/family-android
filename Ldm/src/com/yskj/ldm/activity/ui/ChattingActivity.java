package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.android.commu.net.HandlerException;
import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.ChattingAdapter;
import com.yskj.ldm.activity.view.JListView;
import com.yskj.ldm.activity.view.JListView.IXListViewListener;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

/**
 * @author haibo.wang
 * 聊天窗口类*/
public class ChattingActivity extends NavigationActivity implements  IXListViewListener
{

	private ChattingAdapter adapter = null;
	private JListView  chatList;
	private EditText chatEditComments ;
	private Button chatBtnSendComments ;
	private Integer belongUserId;
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.chatting_screen);
		registerHeadComponent();
		setHeadTitle("对话");
		belongUserId = Integer.parseInt(Session.getSession().get("belong_user").toString()); 
		registerComponent();
	}
	private void onLoad() {
		chatList.stopRefresh();
		chatList.stopLoadMore();
		chatList.setRefreshTime("刚刚");
	}
	public void registerComponent()
	{
		chatList = (JListView)findViewById(R.id.listview);
		chatList.setPullLoadEnable(false);
		adapter = new ChattingAdapter(this);
		chatList.setAdapter(adapter);
		chatList.setXListViewListener(this);
		chatEditComments = (EditText)this.findViewById(R.id.chat_edit_comments);
		chatBtnSendComments = (Button)this.findViewById(R.id.chat_btn_send_comments);
		chatBtnSendComments.setOnClickListener(new OnClickListener(){
			public void onClick(View view) {

				if(StringUtil.isNull(chatEditComments.getText().toString()))
				{
					ChattingActivity.this.showToast("请输入内容");
				}
				else
				{
					ConnectionManager.getInstance().requestuserSendmsgToUser(ChattingActivity.this.userid, belongUserId, chatEditComments.getText().toString(), true, ChattingActivity.this); 
				}
			}});
		ConnectionManager.getInstance().requestMsgboxQueryBygroupid(this.userid, this.belongUserId, adapter.param++, 10, true, this); 
	}


	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))return;
		Log.i("data", "  data -->  "+data);
		JsonObject jo = JsonUtil.parseJson(data);
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		if(errorNo.equals(Command.RESULT_OK))
		{
			String reList = jo.get("resList").toString();
			List<HashMap<String, Object>> list = JsonUtil.getList(reList);
			if(Command.QUERY_CHAT_MESSAGE.equals(cmd))
			{
				if(list.size()>0)
				{  
					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					adapter.param --;
					this.showToast("亲,没有更多对话了哦.");  
				}
				this.onLoad();
			} 
			else if(Command.SEND_CHAT_MESSAGE.equals(cmd))
			{
				this.showToast("发送成功");
				adapter.addNetworkItems(list, adapter.param);
				//				Map<String, Object> contentMap = new HashMap<String, Object>();
				//				contentMap.put("message_content", "");
				//				contentMap.put("user_headporttait", arg1);
				//				contentMap.put("belong_user_headporttait", arg1);
			}
		}

		else
		{
			this.showToast(errorInfo+"");
		}
	}

	/**
	 * 网络异常处理
	 */
	protected void onErrorAction(int cmd,HandlerException e){
		if(cmd ==Command.DOWN_MOON){
			adapter.isloading = false;
		}
		super.onErrorAction(cmd,e);
	}
//	@Override
//	public void onFooterRefresh(PullToRefreshView view) {
//
//	}

//	@Override
//	public void onHeaderRefresh(PullToRefreshView view) {
//		adapter.param++;
//		ConnectionManager.getInstance().requestMsgboxQueryBygroupid(this.userid, this.belongUserId, adapter.param, 6, true, this); 
//	}

	@Override
	public void onRefresh() {
 
		adapter.param++;
		ConnectionManager.getInstance().requestMsgboxQueryBygroupid(this.userid, this.belongUserId, adapter.param, 10, true, this); 
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		
	}
}
