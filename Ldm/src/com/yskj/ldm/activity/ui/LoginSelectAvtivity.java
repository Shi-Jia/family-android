package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;

import com.android.commu.net.HandlerException;
import com.android.commu.parse.Response;
import com.tencent.connect.UserInfo;
import com.tencent.connect.auth.QQAuth;


import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.home.ContainerHomeActivity;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.UserBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.StringUtil;

/**
 * @author haibo.wang
 * @category 登陆选择入口
 * */
public class LoginSelectAvtivity  extends NavigationActivity  {


	private UserInfo mInfo = null;

	private RelativeLayout btnQq,btnLocal;

	private PopupWindow topDialog;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.login_select_screen);
		this.registerLoadingPanel();
		this.registerComponent();
	}
	public void registerComponent()
	{
		View view = (View)LayoutInflater.from(this).inflate(R.layout.waiting_dialog, null);
		topDialog = new PopupWindow(view,LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		btnQq = (RelativeLayout)this.findViewById(R.id.layout_btn_q);
		btnLocal = (RelativeLayout)this.findViewById(R.id.layout_btn_d);

		btnQq.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				onLogin();
			}});
		btnLocal.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				LoginSelectAvtivity.this.startActivity(LoginLocalActivity.class);
				//				LoginSelectAvtivity.this.finish();
			}});
	}
	public static boolean ready(Context context) {
		if (mQQAuth == null) {
			return false;
		}
		boolean ready = mQQAuth.isSessionValid()
				&& mQQAuth.getQQToken().getOpenId() != null;
		if (!ready)
			Toast.makeText(context, "login and get openId first, please!",
					Toast.LENGTH_SHORT).show();
		return ready;
	}
	//	public void isLogin()
	//	{
	//		if (mQQAuth != null && mQQAuth.isSessionValid()) 
	//		{
	//			if(StringUtil.isNull(PreferencesService.getInstance(LoginSelectAvtivity.this).getQqopenId()))
	//			{
	//				ConnectionManager.getInstance().requestUnionLogin(DeviceUtil.getMobileID(LoginSelectAvtivity.this), PreferencesService.getInstance(LoginSelectAvtivity.this).getQqopenId(), true, LoginSelectAvtivity.this);
	//			}
	//		}
	//	}
	public void onLogin()
	{
		if (!mQQAuth.isSessionValid()) {
			IUiListener listener = new BaseUiListener( ) {
				@Override
				protected void doComplete(JSONObject values) {
					//					Log.i("login", " qqlogin --> "+ values.toString());
					try {
						String openid = values.getString("openid");
						PreferencesService.getInstance(LoginSelectAvtivity.this).savaQqopenId(openid);

						topDialog.showAtLocation(LoginSelectAvtivity.this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					mInfo = new UserInfo(LoginSelectAvtivity.this, LoginSelectAvtivity.mQQAuth.getQQToken());
					if (ready(LoginSelectAvtivity.this)) {
						mInfo.getUserInfo(new BaseUiListener( ) {
							@Override
							protected void doComplete(JSONObject values) {
								//								Log.i("login", " 登陆 --> "+ values.toString());
								try {

									ConnectionManager.getInstance().requestUnionLogin(DeviceUtil.getMobileID(LoginSelectAvtivity.this), PreferencesService.getInstance(LoginSelectAvtivity.this).getQqopenId(),
											values.getString("gender"),values.getString("nickname"),values.getString("province")+values.getString("city"),values.getString("figureurl_qq_1"),
											false, LoginSelectAvtivity.this);

								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						});
					}
				}
			};
			//mQQAuth.login(this, "all", listener);
			mTencent.loginWithOEM(this, "all", listener,"10000144","10000144","xxxx");

		} else {
			mQQAuth.logout(this);
		}
	}
	private class BaseUiListener implements IUiListener {


		@Override
		public void onComplete(Object response) {
			//			LoginSelectAvtivity.this.showToast(response.toString());
			doComplete((JSONObject)response);
		}

		protected void doComplete(JSONObject values) {

		}

		@Override
		public void onError(UiError e) {
			topDialog.dismiss();
			LoginSelectAvtivity.this.showToast(e.errorDetail);
		}

		@Override
		public void onCancel() {
			topDialog.dismiss();
			LoginSelectAvtivity.this.showToast("onCancel");

		}
	}
	/**
	 * 网络异常处理
	 */
	protected void onErrorAction(int cmd,HandlerException e){

		super.onErrorAction(cmd,e);
		topDialog.dismiss();
	}
	protected void onNetworkAction(int cmd,Response response){
		String data = ((AllResponse)response).getMsg();
		if(StringUtil.isNull(data))
		{
			topDialog.dismiss();
			return ;
		}
		Log.i("whb", "data -->  "+data);
		try {
			JSONObject jo = JsonUtil.getJSON(data);
			String	errorNo = jo.get("errorNo").toString();
			String errorInfo = jo.getString("errorInfo");

			if(Command.USER_UNION_LOGIN.equals(cmd))
			{
				if(errorNo.equals(Command.RESULT_OK)){

					LoginSelectAvtivity.this.showToast("登陆成功");
					PreferencesService.getInstance(LoginSelectAvtivity.this).savaLoginType(1);
					String token =  jo.getString("token");
					String time =  jo.getString("time");
					String reList = jo.get("resList").toString();
					List<HashMap<String, Object>> list = JsonUtil.getList(reList);
					HashMap<String, Object> pramasMap = list.get(0);
					UserBean ub = new UserBean();
					ub.setAREA_ID(pramasMap.get("area_id").toString());
					ub.setCIRCLE_ID(pramasMap.get("circle_id").toString());
					ub.setUSER_PASSWORLD(pramasMap.get("user_passworld").toString());
					ub.setUSER_NIKENAME(pramasMap.get("user_nikename").toString());
					ub.setUSER_ADRESS(pramasMap.get("user_adress").toString());
					ub.setUSER_BIRTH(pramasMap.get("user_birth").toString());
					ub.setUSER_EMAIL(pramasMap.get("user_email").toString());
					ub.setUSER_HEADPORTTAIT(pramasMap.get("user_headporttait").toString());
					ub.setREG_DATE(pramasMap.get("reg_date").toString());
					ub.setUSER_STATE(pramasMap.get("user_state").toString());
					ub.setUSER_NAME(pramasMap.get("user_name").toString());
					ub.setUSER_QQ(pramasMap.get("user_qq").toString());
					ub.setUSER_PHONE(pramasMap.get("user_phone").toString());
					ub.setUSER_ID(pramasMap.get("user_id").toString());
					ub.setUSER_QQ(pramasMap.get("user_sex").toString());
					Integer versionno  = Integer.parseInt(StringUtil.isNull(pramasMap.get("versioncode").toString())?"0":pramasMap.get("versioncode").toString());

					pservice.saveUserInfo(ub.getUSER_ID(), ub.getCIRCLE_ID(), ub.getUSER_PHONE(), ub.getUSER_PASSWORLD(),
							ub.getUSER_NIKENAME(),ub.getUSER_ADRESS(),ub.getUSER_BIRTH(),
							ub.getUSER_QQ(),ub.getUSER_EMAIL(),ub.getAREA_ID(),ub.getREG_DATE(),
							ub.getUSER_SEX(),ub.getUSER_HEADPORTTAIT(),ub.getUSER_STATE());
					pservice.savaVersion(versionno);

					ConnectionManager.getInstance().setToken(pramasMap.get("user_id").toString(),DeviceUtil.getMobileID(this), token, time);

					pservice.savaVersionTag(false);
					//					if(!this.pservice.getPushTag())
					//						LoginSelectAvtivity.this.stopBaiduPush();
					topDialog.dismiss();
					LoginSelectAvtivity.this.startActivity(ContainerHomeActivity.class);
					LoginSelectAvtivity.this.finish();
				}
				else
				{
					topDialog.dismiss();
					LoginSelectAvtivity.this.showToast(errorInfo);	
				}
			}
		} catch (JSONException e) {
			topDialog.dismiss();
			e.printStackTrace();
		}
	}
}
