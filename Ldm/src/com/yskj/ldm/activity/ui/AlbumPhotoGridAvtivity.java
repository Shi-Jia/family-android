package com.yskj.ldm.activity.ui;

 
import java.util.HashMap;
import java.util.List;
 
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout.LayoutParams;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.AlbumGridAdapter;
import com.yskj.ldm.activity.adapter.AlbumPhotoGridAdapter;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class AlbumPhotoGridAvtivity  extends NavigationActivity{
	private GridView gridView;
	private AlbumPhotoGridAdapter  adapter;
	private PopupWindow topDialog;
//	private List<HashMap<String, Object>> filebean = new ArrayList<HashMap<String, Object>>();

	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.albumphoto_screen);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.add_normal);
//		this.getBlackRelativeLayout().setBackgroundResource(R.drawable.add_bg_normal);
		setHeadTitle("图片预览");
		gridView =  (GridView)this.findViewById(R.id.album_select_grid);
		adapter = new AlbumPhotoGridAdapter(this);
//		adapter.setDataSource(filebean);
		gridView.setAdapter(adapter);
		gridView.setOnScrollListener(scrollerListener);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Session.getSession().put("images",adapter.getList());
				Session.getSession().put("postion", position);
				AlbumPhotoGridAvtivity.this.startActivity(TouchImageActivity.class);	
			}
		});
		View view = (View)LayoutInflater.from(this).inflate(R.layout.topdialog_item, null);
		topDialog = new PopupWindow(view,LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		Button addalbumBtn = (Button)view.findViewById(R.id.toppaizhao);
		addalbumBtn.setText("添加照片");
		((Button)view.findViewById(R.id.piliangshangchuan)).setVisibility(View.GONE);
		((Button)view.findViewById(R.id.cancel)).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				topDialog.dismiss();
			}});
		addalbumBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				topDialog.dismiss();
				Session.getSession().put("jump_type", "AlbumPhotoGridAvtivity");
				AlbumPhotoGridAvtivity.this.startActivity(PhotoSelectActivity.class);
				AlbumPhotoGridAvtivity.this.finish();
			}});

		Integer albumId = Integer.parseInt(Session.getSession().get("album_id").toString());
		ConnectionManager.getInstance().requestPhotoQueryByAlbumId(albumId, adapter.param++, 20, true, this);
	}

	private OnScrollListener scrollerListener = new OnScrollListener() {
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
		}
		public void onScrollStateChanged(AbsListView view, int scrollState) {
 
			if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {

				if (!adapter.isloading && view.getLastVisiblePosition() == view.getCount() - 1) {
					adapter.isloading = true;
					Integer albumId = Integer.parseInt(Session.getSession().get("album_id").toString());
					ConnectionManager.getInstance().requestPhotoQueryByAlbumId(albumId, adapter.param, 20, true, AlbumPhotoGridAvtivity.this);
					adapter.param++;
				}
			}
		}
	};
	/**
	 * 
	 */
	protected void onRightAction()
	{
		topDialog.showAtLocation(AlbumPhotoGridAvtivity.this.getWindow().getDecorView(), Gravity.BOTTOM, 0, 0);
	}
	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);
		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();
		if(errorNo.equals(Command.RESULT_OK))
		{

			if(Command.ALBL_QUERY_PHOTO.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				if(list.size()>0)
				{
					adapter.addNetworkItems(list, adapter.param);
				}
				else
				{
					adapter.param --;
					this.showToast("亲,没有更多照片了哦.");  
				}
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
