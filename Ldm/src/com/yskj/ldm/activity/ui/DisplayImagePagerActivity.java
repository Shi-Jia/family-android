package com.yskj.ldm.activity.ui;
import java.util.ArrayList;
import java.util.HashMap;

import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.DisplayImagePagerAdapter;
import com.yskj.ldm.activity.view.ScaleGestureDetector;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.FileBean;
import com.yskj.ldm.util.*;

import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.yskj.ldm.activity.view.ImageViewTouch;
public class DisplayImagePagerActivity extends NavigationActivity {

 
	private static final int PAGER_MARGIN_DP = 40;
 
	ViewPager pager;
	private DisplayImagePagerAdapter diA = null ;
	ArrayList<HashMap<String, Object>> fileList = null;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.display_image_screen);
		//		registerHeadComponent();
		pager = (ViewPager) findViewById(R.id.pager);
		 
		fileList  = (ArrayList<HashMap<String, Object>>)Session.getSession().get("images");
		int positon = (Integer)Session.getSession().get("postion");
		//		Log.i("positon " ,positon +"   <--positon");
		diA = new DisplayImagePagerAdapter(this);
		if(null!=fileList)
			diA.setDataSource(fileList);
		if(null!=diA)
			pager.setAdapter(diA);
		pager.setCurrentItem(positon);

 
	}
	 
}
