package com.yskj.ldm.activity.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yskj.ldm.R;
import com.yskj.ldm.R.drawable;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.activity.view.GestureDetector;
import com.yskj.ldm.activity.view.ImageViewTouch;
import com.yskj.ldm.activity.view.PagerAdapter;
import com.yskj.ldm.activity.view.ScaleGestureDetector;
import com.yskj.ldm.activity.view.ViewPager;
import com.yskj.ldm.util.Session;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ZoomControls;

public class TouchImageActivity extends Activity implements OnClickListener {

	private static final String TAG = TouchImageActivity.class.getSimpleName();

	private static final int REQUIRED_BITMAP_SIZE = 400;
	private static final int SHOW_HIDE_CONTROL_ANIMATION_TIME = 500;

	private static final int PAGER_MARGIN_DP = 40;
	private ImageLoader imageLoader = ImageLoader.getInstance();
	private RelativeLayout mRootLayout;
	private ViewPager mViewPager;
	private ViewGroup mHeader;
	//	private ViewGroup mBottom;
	private TextView mPageShwo;
	//	private TextView mPicName;
	private Button mNext;
	private Button mPrevious;
	//	private Button mOpen;
	//	private Button mMore;
	private ZoomControls mZoomButtons;
	private AlertDialog mMoreDialog;

	private ImagePagerAdapter mPagerAdapter;
	private DisplayImageOptions options; //配置图片加载及显示选项
	private GestureDetector mGestureDetector;
	private ScaleGestureDetector mScaleGestureDetector;
	private boolean mPaused;
	private boolean mOnScale = false;
	private boolean mOnPagerScoll = false;
	private boolean mControlsShow = false;

	// 传入参数
	//	private List<String> fileList;
	private int mPosition;
	ArrayList<HashMap<String, Object>> fileList = null;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewpager);
		//配置图片加载及显示选项（还有一些其他的配置，查阅doc文档吧）
		options = new DisplayImageOptions.Builder()
		//		.showStubImage(R.drawable.head_photo)    //在ImageView加载过程中显示图片
		//		.showImageForEmptyUri(R.drawable.head_photo)  //image连接地址为空时
		//		.showImageOnFail(R.drawable.head_photo)  //image加载失败
		.cacheInMemory(true)  //加载图片时会在内存中加载缓存
		.cacheOnDisc(true)   //加载图片时会在磁盘中加载缓存
		//			.displayer(new RoundedBitmapDisplayer(20))  //设置用户加载图片task(这里是圆角图片显示)
		.build();
		mRootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
		mViewPager = (ViewPager) findViewById(R.id.viewPager);
		mHeader = (ViewGroup) findViewById(R.id.ll_header);
		//		mBottom = (ViewGroup) findViewById(R.id.ll_bottom);
		mPageShwo = (TextView) findViewById(R.id.tv_page);
		//		mPicName = (TextView) findViewById(R.id.tv_pic_name);
		mNext = (Button) findViewById(R.id.btn_next);
		mNext.setOnClickListener(this);
		mPrevious = (Button) findViewById(R.id.btn_pre);
		mPrevious.setOnClickListener(this);
		//		mOpen = (Button) findViewById(R.id.btn_open);
		//		mOpen.setOnClickListener(this);
		//		mMore = (Button) findViewById(R.id.btn_dialog);
		//		mMore.setOnClickListener(this);
		mZoomButtons = (ZoomControls) findViewById(R.id.zoomButtons);
		mZoomButtons.setZoomSpeed(100);
		mZoomButtons.setOnZoomInClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getCurrentImageView().zoomIn();
				updateZoomButtonsEnabled();
			}
		});
		mZoomButtons.setOnZoomOutClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getCurrentImageView().zoomOut();
				updateZoomButtonsEnabled();
			}
		});

		final float scale = getResources().getDisplayMetrics().density;
		int pagerMarginPixels = (int) (PAGER_MARGIN_DP * scale + 0.5f);
		mViewPager.setPageMargin(pagerMarginPixels);
		mViewPager.setPageMarginDrawable(new ColorDrawable(Color.BLACK));

		mPagerAdapter = new ImagePagerAdapter();
		mViewPager.setAdapter(mPagerAdapter);
		mViewPager.setOnPageChangeListener(mPageChangeListener);
		setupOnTouchListeners(mViewPager);

		// 参数传入
		//		fileList = new ArrayList<String>();
		//		fileList = new ArrayList<HashMap<String, Object>>();
		//		String imagesDir = Environment.getExternalStorageDirectory().getAbsolutePath()+ImageRoot;
		//		File dirFile = new File(imagesDir);
		//		if (dirFile.exists()) {
		//			for (File file : dirFile.listFiles()) {
		//				fileList.add(file.getPath());
		//			}
		//		}
		//		if (fileList.size() == 0) {
		//			AlertDialog.Builder builder = new Builder(this);
		//			builder.setMessage("请确保/sdcard/DCIM/Camera/目录下有图片，或者将imagesDir更换为有图片的路径。");
		//			builder.setNeutralButton("退出",
		//					new DialogInterface.OnClickListener() {
		//						@Override
		//						public void onClick(DialogInterface dialog, int which) {
		//							TouchImageActivity.this.finish();
		//							return;
		//						}
		//					});
		//			builder.show();
		//		}
		fileList  = (ArrayList<HashMap<String, Object>>)Session.getSession().get("images");
		mPosition =  Integer.parseInt(Session.getSession().get("postion").toString());
		mViewPager.setCurrentItem(mPosition, false);
		updateShowInfo();
		updatePreNextButtonEnable();
		hideControls();
	}

	private void updateShowInfo() {
		if (fileList.size() > 0) {
			mPageShwo.setText(String.format("%d/%d", mPosition + 1,
					fileList.size()));
			//			mPicName.setText(getPositionFileName(mPosition));
		}
	}

	private void updatePreNextButtonEnable() {
		mPrevious.setEnabled(mPosition > 0);
		mNext.setEnabled(mPosition < fileList.size() - 1);
	}

	private void updateZoomButtonsEnabled() {
		ImageViewTouch imageView = getCurrentImageView();
		if (imageView != null) {
			float scale = imageView.getScale();
			mZoomButtons.setIsZoomInEnabled(scale < imageView.mMaxZoom);
			mZoomButtons.setIsZoomOutEnabled(scale > imageView.mMinZoom);
		}
	}

	private void showControls() {
		AlphaAnimation animation = new AlphaAnimation(0f, 1f);
		animation.setFillAfter(true);
		animation.setDuration(SHOW_HIDE_CONTROL_ANIMATION_TIME);
		//		mZoomButtons.startAnimation(animation);
		mHeader.startAnimation(animation);
		//		mNext.startAnimation(animation);
		//		mPrevious.startAnimation(animation);
		//		mBottom.startAnimation(animation);

		//		mControlsShow = true;
		//		mZoomButtons.setVisibility(View.VISIBLE);
		//		mNext.setVisibility(View.VISIBLE);
		//		mPrevious.setVisibility(View.VISIBLE);
		mHeader.setVisibility(View.VISIBLE);
		//		mBottom.setVisibility(View.VISIBLE);
	}

	private void hideControls() {
		AlphaAnimation animation = new AlphaAnimation(1f, 0f);
		animation.setFillAfter(true);
		animation.setDuration(SHOW_HIDE_CONTROL_ANIMATION_TIME);
		mZoomButtons.startAnimation(animation);
		//		mHeader.startAnimation(animation);
		mNext.startAnimation(animation);
		mPrevious.startAnimation(animation);
		//		mBottom.startAnimation(animation);

		mControlsShow = false;
		mZoomButtons.setVisibility(View.GONE);
		mNext.setVisibility(View.GONE);
		mPrevious.setVisibility(View.GONE);
		//		mHeader.setVisibility(View.GONE);
		//		mBottom.setVisibility(View.GONE);
	}

	private String getPositionFileName(int position) {
		String path = (fileList.get(position)).get("downurl").toString();
		String[] splits = path.split("/");
		String name = "";
		if (splits.length > 0) {
			name = splits[splits.length - 1];
		}

		return name;
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = REQUIRED_BITMAP_SIZE;

			// Find the correct scale value. It should be the power of 2.
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
		}
		return null;
	}

	@Override
	public void onStart() {
		super.onStart();
		mPaused = false;
	}

	@Override
	public void onStop() {
		super.onStop();
		mPaused = true;
	}

	private void setupOnTouchListeners(View rootView) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR_MR1) {
			mScaleGestureDetector = new ScaleGestureDetector(this,
					new MyOnScaleGestureListener());
		}
		mGestureDetector = new GestureDetector(this, new MyGestureListener());

		OnTouchListener rootListener = new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				// NOTE: gestureDetector may handle onScroll..
				if (!mOnScale) {;
				if (!mOnPagerScoll) {
					mGestureDetector.onTouchEvent(event);
				}
				}

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR_MR1) {
					if (!mOnPagerScoll) {
						mScaleGestureDetector.onTouchEvent(event);
					}
				}

				ImageViewTouch imageView = getCurrentImageView();
				if (imageView == null) {
					return true;
				}
				if (!mOnScale&&imageView!=null&&imageView.mBitmapDisplayed.getBitmap()!=null) {
					Matrix m = imageView.getImageViewMatrix();
					RectF rect = new RectF(0, 0, imageView.mBitmapDisplayed
							.getBitmap().getWidth(), imageView.mBitmapDisplayed
							.getBitmap().getHeight());
					m.mapRect(rect);
					// 图片超出屏幕范围后移动
					if (!(rect.right > imageView.getWidth() + 0.1 && rect.left < -0.1)) {
						try {
							mViewPager.onTouchEvent(event);
						} catch (ArrayIndexOutOfBoundsException e) {
							// why?
						}
					}
				}

				// We do not use the return value of
				// mGestureDetector.onTouchEvent because we will not receive
				// the "up" event if we return false for the "down" event.
				return true;
			}
		};

		rootView.setOnTouchListener(rootListener);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent m) {
		if (mPaused)
			return true;
		return super.dispatchTouchEvent(m);
	}

	@Override
	protected void onDestroy() {
		ImageViewTouch imageView = getCurrentImageView();
		if (imageView != null) {
			imageView.mBitmapDisplayed.recycle();
			imageView.clear();
		}
		super.onDestroy();
	}

	private ImageViewTouch getCurrentImageView() {
		return (ImageViewTouch) mPagerAdapter.views.get((mViewPager
				.getCurrentItem()));
	}

	ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
		@Override
		public void onPageSelected(int position, int prePosition) {
			// Log.d(TAG, "onPageSelected" + position + ", prePosition: "
			// + prePosition);
			ImageViewTouch preImageView = mPagerAdapter.views.get(prePosition);
			if (preImageView != null) {
				preImageView.setImageBitmapResetBase(
						preImageView.mBitmapDisplayed.getBitmap(), true);
			}
			mPosition = position;

			updateZoomButtonsEnabled();
			updateShowInfo();
			updatePreNextButtonEnable();
		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
			// Log.d(TAG, "onPageScrolled");
			mOnPagerScoll = true;
		}

		@Override
		public void onPageScrollStateChanged(int state) {
			// Log.d(TAG, "onPageScrollStateChanged: " + state);
			if (state == ViewPager.SCROLL_STATE_DRAGGING) {
				mOnPagerScoll = true;
			} else if (state == ViewPager.SCROLL_STATE_SETTLING) {
				mOnPagerScoll = false;
			} else {
				mOnPagerScoll = false;
			}
		}
	};

	private class MyGestureListener extends
	GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			// Log.d(TAG, "gesture onScroll");
			if (mOnScale) {
				return true;
			}
			if (mPaused) {
				return false;
			}
			ImageViewTouch imageView = getCurrentImageView();
			if (imageView == null) {
				return true;
			}
			imageView.panBy(-distanceX, -distanceY);
			imageView.center(true, true);

			// 超出边界效果去掉这个
			imageView.center(true, true);

			return true;
		}

		@Override
		public boolean onUp(MotionEvent e) {
			return super.onUp(e);
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			//			if (mControlsShow) {
			//				// delayHideControls();
			//				hideControls();
			//			} else {
			//				updateZoomButtonsEnabled();
			//				showControls();
			//			}
			TouchImageActivity.this.finish();
			return true;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			if (mPaused) {
				return false;
			}
			ImageViewTouch imageView = getCurrentImageView();
			if (imageView == null) {
				return true;
			}
			// Switch between the original scale and 3x scale.
			if (imageView.mBaseZoom < 1) {
				if (imageView.getScale() > 2F) {
					imageView.zoomTo(1f);
				} else {
					imageView.zoomToPoint(3f, e.getX(), e.getY());
				}
			} else {
				if (imageView.getScale() > (imageView.mMinZoom + imageView.mMaxZoom) / 2f) {
					imageView.zoomTo(imageView.mMinZoom);
				} else {
					imageView.zoomToPoint(imageView.mMaxZoom, e.getX(),
							e.getY());
				}
			}

			updateZoomButtonsEnabled();
			return true;
		}
	}

	private class MyOnScaleGestureListener extends
	ScaleGestureDetector.SimpleOnScaleGestureListener {

		float currentScale;
		float currentMiddleX;
		float currentMiddleY;

		@Override
		public void onScaleEnd(ScaleGestureDetector detector) {

			updateZoomButtonsEnabled();

			final ImageViewTouch imageView = getCurrentImageView();
			if (imageView == null) {
				return;
			}

			Log.d(TAG, "currentScale: " + currentScale + ", maxZoom: "
					+ imageView.mMaxZoom);
			if (currentScale > imageView.mMaxZoom) {
				imageView
				.zoomToNoCenterWithAni(currentScale
						/ imageView.mMaxZoom, 1, currentMiddleX,
						currentMiddleY);
				currentScale = imageView.mMaxZoom;
				imageView.zoomToNoCenterValue(currentScale, currentMiddleX,
						currentMiddleY);
			} else if (currentScale < imageView.mMinZoom) {
				imageView.zoomToNoCenterWithAni(currentScale,
						imageView.mMinZoom, currentMiddleX, currentMiddleY);
				currentScale = imageView.mMinZoom;
				imageView.zoomToNoCenterValue(currentScale, currentMiddleX,
						currentMiddleY);
			} else {
				imageView.zoomToNoCenter(currentScale, currentMiddleX,
						currentMiddleY);
			}

			imageView.center(true, true);

			// NOTE: 延迟修正缩放后可能移动问题
			imageView.postDelayed(new Runnable() {
				@Override
				public void run() {
					mOnScale = false;
				}
			}, 300);
			// Log.d(TAG, "gesture onScaleEnd");
		}

		@Override
		public boolean onScaleBegin(ScaleGestureDetector detector) {
			// Log.d(TAG, "gesture onScaleStart");
			mOnScale = true;
			return true;
		}

		@Override
		public boolean onScale(ScaleGestureDetector detector, float mx, float my) {
			// Log.d(TAG, "gesture onScale");
			ImageViewTouch imageView = getCurrentImageView();
			if (imageView == null) {
				return true;
			}
			float ns = imageView.getScale() * detector.getScaleFactor();

			currentScale = ns;
			currentMiddleX = mx;
			currentMiddleY = my;

			if (detector.isInProgress()) {
				imageView.zoomToNoCenter(ns, mx, my);
			}
			return true;
		}
	}

	private class ImagePagerAdapter extends PagerAdapter {
		public Map<Integer, ImageViewTouch> views = new HashMap<Integer, ImageViewTouch>();

		@Override
		public int getCount() {

			return fileList.size();
		}

		@Override
		public Object instantiateItem(View container, int position) {
			//			View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
			//			assert imageLayout != null;
			//			ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
			//			final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
			ImageViewTouch imageView = new ImageViewTouch(
					TouchImageActivity.this);
			imageView.setLayoutParams(new LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			imageView.setBackgroundColor(Color.BLACK);
			imageView.setFocusableInTouchMode(true);
			//			Bitmap b = decodeFile(new File((fileList.get(position)).get("downurl").toString()));
			Bitmap b =	imageLoader.loadImageSync(fileList.get(position).get("downurl").toString());
			//			Log.i("查看图片", " 查看图片1  ----》》》   "+b);
			if(null ==b)
			{
				b = imageLoader.loadImageSync(RecordsPath.UriDrawable+drawable.loading_pic);
			}
			//			Log.i("查看图片", " 查看图片2  ----》》》   "+b);
			imageView.setImageBitmapResetBase(b, true);
			((ViewPager) container).addView(imageView);
			views.put(position, imageView);
			return imageView;
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			// Log.d(TAG, "destroyItem");
			ImageViewTouch imageView = (ImageViewTouch) object;
			imageView.mBitmapDisplayed.recycle();
			imageView.clear();
			((ViewPager) container).removeView(imageView);
			views.remove(position);
		}

		@Override
		public void startUpdate(View container) {
			// Log.d(TAG, "startUpdate");
		}

		@Override
		public void finishUpdate(View container) {
			// Log.d(TAG, "finishUpdate");
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			// Log.d(TAG, "isViewFromObject");
			return view == ((ImageViewTouch) object);
		}

		@Override
		public Parcelable saveState() {
			// Log.d(TAG, "saveState");
			return null;
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
			// Log.d(TAG, "restoreState");
		}
	}

	private Uri getCurrentImageUri() {
		File file = new File((fileList.get(mPosition)).get("downurl").toString());
		return Uri.fromFile(file);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_next:
			if (mPosition < fileList.size() - 1) {
				mViewPager.setCurrentItem(++mPosition);
			}
			updatePreNextButtonEnable();
			break;
		case R.id.btn_pre:
			if (mPosition > 0) {
				mViewPager.setCurrentItem(--mPosition);
			}
			updatePreNextButtonEnable();
			break;
			//		case R.id.btn_open: {
			//			Intent intent = new Intent();
			//			intent.setAction(android.content.Intent.ACTION_VIEW);
			//			intent.setDataAndType(getCurrentImageUri(), "image/*");
			//			startActivity(intent);
			//		}
			//			break;
			//		case R.id.btn_dialog:
			//			break;
		}
	}
}
