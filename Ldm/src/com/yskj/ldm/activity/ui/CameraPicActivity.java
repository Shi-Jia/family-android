package com.yskj.ldm.activity.ui;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;








import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;




import com.yskj.ldm.R;
import com.yskj.ldm.activity.adapter.ImageFilterAdapter;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.FileBean;

import com.yskj.ldm.util.SDCardStoreManager;
import com.yskj.ldm.util.Session;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.imagefilter.*;
public class CameraPicActivity extends NavigationActivity{

	ImageView  disView = null;
	ImageView  mizhaosave_delete_normal = null;
	ImageView  mizhaosave_back_normal = null;
	ImageView  mizhaosave_save_normal =null;

	private  ImageLoader imageLoader = ImageLoader.getInstance();
	private  Bitmap bitmap = null;
	private ArrayList<FileBean> recordFileBean = new ArrayList<FileBean>();
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.camera_pic);
		//		registerHeadComponent();
		disView = (ImageView)this.findViewById(R.id.display_pic);
		mizhaosave_delete_normal = (ImageView)this.findViewById(R.id.mizhaosave_delete_normal);
		mizhaosave_back_normal = (ImageView)this.findViewById(R.id.mizhaosave_back_normal);
		mizhaosave_save_normal = (ImageView)this.findViewById(R.id.mizhaosave_save_normal);
		FileBean fb = new FileBean();
		fb.setFileUri(RecordsPath.UriSDCard+Session.getSession().get("imagepath").toString());
		fb.setLocalpath(Session.getSession().get("imagepath").toString());
		fb.setFormat("image/jpeg");
		fb.setName(System.currentTimeMillis()+"");
		fb.setSize("0");
		recordFileBean.add(fb);
		imageLoader.displayImage(recordFileBean.get(0).getFileUri(), disView);
		imageLoader.loadImageSync(recordFileBean.get(0).getFileUri());
		mizhaosave_save_normal.setOnClickListener(new  OnClickListener(){

			public void onClick(View arg0) {
				try {
					File file =  SDCardStoreManager.getInstance().getFilePath(SDCardStoreManager.getInstance().getRootPath()+RecordsPath.ImageFilterRoot,System.currentTimeMillis()+".jpg");
					Log.i("file", " filePath --> "+ file.getPath());
					BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos); // 保存图片
					bos.flush();
					bos.close();
					recordFileBean.get(0).setFileUri(RecordsPath.UriSDCard+file.getPath());
					recordFileBean.get(0).setLocalpath(file.getPath());
				} catch (Exception e) {
					e.printStackTrace();
				}
				Session.getSession().put("imagemap", recordFileBean);
				CameraPicActivity.this.startActivity(UploadMoonActivity.class);
			}});
		mizhaosave_back_normal.setOnClickListener(new  OnClickListener(){
			public void onClick(View arg0) {
				CameraPicActivity.this.finish();
			}});
		mizhaosave_delete_normal.setOnClickListener(new  OnClickListener(){
			public void onClick(View arg0) {
				Log.i("sdcardpath", " deletePath --->  "+ recordFileBean.get(0).getLocalpath());
				if(SDCardStoreManager.getInstance().delete(recordFileBean.get(0).getLocalpath()))
				{
					CameraPicActivity.this.showToast("删除成功");
					CameraPicActivity.this.finish();
				}
				else
				{
					CameraPicActivity.this.showToast("删除失败");
				}
			}});

		LoadImageFilter();
	}

	/**
	 * 加载图片filter
	 */
	private void LoadImageFilter() {
		Gallery gallery = (Gallery) findViewById(R.id.galleryFilter);
		final ImageFilterAdapter filterAdapter = new ImageFilterAdapter(
				CameraPicActivity.this);
		gallery.setAdapter(new ImageFilterAdapter(CameraPicActivity.this));
		gallery.setSelection(2);
		gallery.setAnimationDuration(3000);
		gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				IImageFilter filter = (IImageFilter) filterAdapter.getItem(position);
				new processImageTask(CameraPicActivity.this, filter).execute();
			}
		});
	}

	public class processImageTask extends AsyncTask<Void, Void, Bitmap> {
		private IImageFilter filter;

		public processImageTask(Activity activity, IImageFilter imageFilter) {
			this.filter = imageFilter;

		}
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			//			textView.setVisibility(View.VISIBLE);
		}

		public Bitmap doInBackground(Void... params) {
			Image img = null;
			try
			{
				Bitmap bitmap =	imageLoader.loadImageSync(recordFileBean.get(0).getFileUri());

				img = new Image(bitmap);
				if (filter != null) {
					img = filter.process(img);
					img.copyPixelsFromBuffer();
				}
				return img.getImage();
			}
			catch(Exception e){
				if (img != null && img.destImage.isRecycled()) {
					img.destImage.recycle();
					img.destImage = null;
					System.gc(); // 提醒系统及时回收
				}
			}
			finally{
				if (img != null && img.image.isRecycled()) {
					img.image.recycle();
					img.image = null;
					System.gc(); // 提醒系统及时回收
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			if(result != null){
				super.onPostExecute(result);
				bitmap = result;
				disView.setImageBitmap(result);	
			}
			//			textView.setVisibility(View.GONE);
		}
	}
}
