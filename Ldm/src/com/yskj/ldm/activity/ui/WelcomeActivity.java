package com.yskj.ldm.activity.ui;

import java.util.ArrayList;

import com.yskj.ldm.R;
import com.yskj.ldm.base.NavigationActivity;

 


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

public class WelcomeActivity extends NavigationActivity {
	private boolean isFirstLogin;
	private MyPagerAdapter myPagerAdapter;
	private ViewPager mViewPager;
	private ArrayList<View> mViews;
	private PopupWindow mPopupWindow;
	private View mPopupView;
	private LinearLayout mIndicatorLayout; // ����װСԲ���Linearlayout
	private ArrayList<ImageView> mIndicatorList;// װСԲ��ļ���
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 9:
				break;
			case 10:
				break;
			default:
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		WindowManager wm = getWindowManager();
//		int width = wm.getDefaultDisplay().getWidth();
//		int hight = wm.getDefaultDisplay().getHeight();
		new Thread() {
			public void run() {
				mHandler.postDelayed(new Runnable() {
					public void run() {
						if (isFirstLogin) {
 
							showGuide();
						} else {
							goMainActvity();
							finish();
						}
					}
				}, 3000);
			};
		}.start();

	}

	private void showGuide() {

		mIndicatorList = new ArrayList<ImageView>();
		iniView();
		iniViewPager();  
//
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				mHandler.sendEmptyMessage(9);
//			}
//		}).start();

	}

	private void iniViewPager() {
		
		View v1 = getLayoutInflater().inflate(R.layout.welcome_guide_one,
				null);
//		View v2 = getLayoutInflater().inflate(R.layout.welcome_guide_layout_2,
//				null);
//		View v3 = getLayoutInflater().inflate(R.layout.welcome_guide_layout_3,
//				null);
//		View v4 = getLayoutInflater().inflate(R.layout.welcome_guide_layout_4,
//				null);

		ImageView img1 = (ImageView) v1.findViewById(R.id.img1);
//		ImageView img2 = (ImageView) v2.findViewById(R.id.img2);
//		ImageView img3 = (ImageView) v3.findViewById(R.id.img3);
//		ImageView img4 = (ImageView) v4.findViewById(R.id.img4);

 
		img1.setAlpha(255);
//		img2.setAlpha(255);
//		img3.setAlpha(255);
//		img4.setAlpha(255);

 
		ImageView start = (ImageView) v1.findViewById(R.id.img1);
		start.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Toast.makeText(WelcomeActivity.this, "��ʼ����", Toast.LENGTH_SHORT).show();
				mPopupWindow.dismiss();
				goMainActvity();
				finish();
			}
		});

		mViews = new ArrayList<View>();
		mViews.add(v1);
//		mViews.add(v2);
//		mViews.add(v3);
//		mViews.add(v4);

	 
		myPagerAdapter = new MyPagerAdapter();
		mViewPager.setAdapter(myPagerAdapter);
		 
		mViewPager.setOnPageChangeListener(new MyPagerChangeListener());

	 
		for (int i = 0; i < mViews.size(); i++) {
			ImageView indicator = new ImageView(this);
//			if (i == 0) {
//				indicator.setImageResource(R.drawable.dot_selected);
//			} else {
//				indicator.setImageResource(R.drawable.dot_unselected);
//			}
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(10, 0, 0, 0);
			indicator.setLayoutParams(params);

			mIndicatorLayout.addView(indicator);
		 
			mIndicatorList.add(indicator);
		}

	}

	private void iniView() {

		mPopupView = getLayoutInflater()
				.inflate(R.layout.welcome_pop_win, null);
		mViewPager = (ViewPager) mPopupView.findViewById(R.id.pager);

		 
		mIndicatorLayout = (LinearLayout) mPopupView
				.findViewById(R.id.indicatorLayout);

		mPopupWindow = new PopupWindow(mPopupView, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, true);
	}

	private class MyPagerAdapter extends PagerAdapter {

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(mViews.get(arg1));
		}

		@Override
		public void finishUpdate(View arg0) {
		}

		@Override
		public int getCount() {
			return mViews.size();
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(mViews.get(arg1));
			return mViews.get(arg1);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
			for (int i = 0; i < mViews.size(); i++) {
			}
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
		}
	}

	private class MyPagerChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int position) {
			Log.i("zj", "onPagerChange position=" + position);
//			for (int i = 0; i < mViews.size(); i++) {
//				mIndicatorList.get(i).setImageResource(
//						R.drawable.dot_unselected);
//			}
//			mIndicatorList.get(position).setImageResource(
//					R.drawable.dot_selected);
		}
	}

	private void goMainActvity() {
//		Intent intent = new Intent(this, MainActivity.class);
//		startActivity(intent);
//		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
