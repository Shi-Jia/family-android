package com.yskj.ldm.activity.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import com.android.commu.parse.Response;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.R.color;
import com.yskj.ldm.activity.adapter.FamilyCirclePeopleAdapter;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.base.NavigationActivity;
import com.yskj.ldm.beans.CircleBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.SDCardStoreManager;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;

public class FamilyPeopleActivity  extends NavigationActivity {
	private LinearLayout  circle;
	TextView circleSelectOne;
	private RelativeLayout  []circleSelect;
	private RelativeLayout  allCircleSelect;
	private FamilyCirclePeopleAdapter adapter;
	private boolean showRight = false;

	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.family_people_sreen);
		registerHeadComponent();
		getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);
		setHeadTitle("家人");
		this.getBackButton().setVisibility(View.INVISIBLE);
		registerComponent();
	}
	protected void onRightAction()
	{
		if(!showRight)
		{
			getRightButton().setBackgroundResource(R.drawable.family_add_icon_normal);
			circledialogItemView.setVisibility(View.VISIBLE);
			showRight= true;
		}
		else
		{
			getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);
			circledialogItemView.setVisibility(View.GONE);
			showRight = false;
		}

	}
	public void registerComponent()
	{
		ListView list = (ListView)findViewById(R.id.listview);
		adapter = new FamilyCirclePeopleAdapter(this);
		//		ChattingActivity
		list.setAdapter(adapter);
		//		circledialogItemView = this.findViewById(R.id.topdialog_layout);
		RelativeLayout dialogCramre=(RelativeLayout)circledialogItemView.findViewById(R.id.family_dialog_cramre_layout);
		TextView dialogLeft= (TextView)dialogCramre.findViewById(R.id.family_dialog_cramre);
		dialogLeft.setText("创建圈子");
		ImageView imageleft = (ImageView)dialogCramre.findViewById(R.id.family_dialog_cramre_img);
		imageleft.setBackgroundResource(R.drawable.creat_circle);

		RelativeLayout dialogPicupload=(RelativeLayout)circledialogItemView.findViewById(R.id.family_dialog_picupload_layout);
		TextView dialogRight= (TextView)dialogPicupload.findViewById(R.id.family_dialog_picupload);
		dialogRight.setText("加入圈子");
		ImageView imageright = (ImageView)dialogPicupload.findViewById(R.id.family_dialog_picupload_img);
		if(null!=imageright)
			imageright.setBackgroundResource(R.drawable.find_circle);

		dialogCramre.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				circledialogItemView.setVisibility(View.GONE);
				getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);
				showRight = false;
				FamilyPeopleActivity.this.startActivityForResult(CreaterCircleActivity.class, 1);
				//				FamilyPeopleActivity.this.startActivity(CreaterCircleActivity.class);
			}});
		dialogPicupload.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				circledialogItemView.setVisibility(View.GONE);
				getRightButton().setBackgroundResource(R.drawable.family_add_icon_press);
				showRight = false;
				FamilyPeopleActivity.this.startActivityForResult(AddCircleActivity.class,2);
				//				FamilyPeopleActivity.this.startActivity(AddCircleActivity.class);
			}});
		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				Map<String, Object> peopleMap = (HashMap<String, Object>)adapter.getList().get(position);
				Session.getSession().put("belong_user", peopleMap.get("user_id"));

				FamilyPeopleActivity.this.startActivity(ChattingActivity.class);
			}});
		ConnectionManager.getInstance().requestQueryCircle(this.userid, true, this);
		ConnectionManager.getInstance().requestCircleQueryUserBycircleid(this.circleid, 0, 1000, true, this);
	}

	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{ 
		super.onActivityResult(requestCode, resultCode, data);  
		if (resultCode == Activity.RESULT_OK)
		{ 
			ConnectionManager.getInstance().requestQueryCircle(this.userid, true, this);
		}
	}
	public void addAllCircle(LinearLayout ll,LinearLayout.LayoutParams rlparams)
	{
		circle = (LinearLayout)this.findViewById(R.id.family_circle_layout);
		allCircleSelect = (RelativeLayout)this.inflate(R.layout.select_button_item);
		allCircleSelect.setLayoutParams(rlparams);
		TextView tv_name = (TextView)allCircleSelect.findViewById(R.id.select_btn_tv);
		final View view_bg = allCircleSelect.findViewById(R.id.select_btn_bg);
		tv_name.setText("所有圈子");
		allCircleSelect.setOnClickListener(  new OnClickListener(){
			public void onClick(View view) {
				clearCircleBlack(circleSelect.length);
				view_bg.setVisibility(View.VISIBLE);
				adapter.getList().clear();
				ConnectionManager.getInstance().requestCircleQueryUserBycircleid(FamilyPeopleActivity.this.circleid, 0, 1000, true, FamilyPeopleActivity.this);
			}
		});
		//		View view = new View(this);
		//		view.setBackgroundResource(color.white);
		//		LinearLayout.LayoutParams viewparams = new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		//		viewparams.width = 1;
		//		viewparams.height = 60;
		//		view.setLayoutParams(viewparams);
		circle.addView(allCircleSelect);
		//		circle.addView(view);
		allCircleSelect.findViewById(R.id.select_btn_bg).setVisibility(View.VISIBLE);
	}
	public void creatCircle(List<HashMap<String, Object>> list)
	{
		circle = (LinearLayout)this.findViewById(R.id.family_circle_layout);
		circle.removeAllViews();
		circleSelect = new RelativeLayout[list.size()];
		LinearLayout.LayoutParams rlparams = new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		if(list.size()<=1)
		{
			rlparams.width = this.mWidth/2;
		}
		else
		{
			rlparams.width = this.mWidth/3;
		}
		this.addAllCircle(circle, rlparams);
		for(int i = 0;i<list.size();i++)
		{
			circleSelect [i] = (RelativeLayout)this.inflate(R.layout.select_button_item);
			circleSelect [i].setLayoutParams(rlparams);
			TextView tv_name = (TextView)circleSelect [i].findViewById(R.id.select_btn_tv);
			final View view_bg = circleSelect [i].findViewById(R.id.select_btn_bg);
			final HashMap<String, Object> mapList=(HashMap<String, Object>) list.get(i);
			tv_name.setText(mapList.get("circle_name").toString());

			circleSelect [i].setOnClickListener(new OnClickListener(){
				public void onClick(View view) {
					clearCircleBlack(circleSelect.length);
					view_bg.setVisibility(View.VISIBLE);
					adapter.getList().clear();
					ConnectionManager.getInstance().requestCircleQueryUserBycircleid(mapList.get("circle_id").toString(), 0, 1000, true, FamilyPeopleActivity.this);
				}
			});
			//			View view = new View(this);
			//			view.setBackgroundResource(color.white);
			//			LinearLayout.LayoutParams viewparams = new  LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			//			viewparams.width = 1;
			//			viewparams.height = 60;
			//			view.setLayoutParams(viewparams);

			circle.addView(circleSelect[i]);
			//			circle.addView(view);
		}
	}

	public void clearCircleBlack(int size)
	{
		allCircleSelect.findViewById(R.id.select_btn_bg).setVisibility(View.INVISIBLE);
		for(int i =0;i<size;i++)
		{
			circleSelect [i].findViewById(R.id.select_btn_bg).setVisibility(View.INVISIBLE);
		}

	}

	/*****************************************************************
	 * 网络请求回调
	 */
	protected void onNetworkAction(int cmd,Response response){

		String data = ((AllResponse)response).getMsg();
		Log.i("data", "  data -->  "+data);
		if(StringUtil.isNull(data))return;
		JsonObject jo = JsonUtil.parseJson(data);

		String errorNo = jo.get("errorNo").toString();
		String errorInfo = jo.get("errorInfo").toString();

		if(errorNo.equals(Command.RESULT_OK))
		{
			if(Command.CIRC_QUERY.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List<HashMap<String, Object>> list = JsonUtil.getList(reList);
				Session.getSession().put("headlist", list);
//				this.circleListData = list;
				creatCircle(list);
				Log.i("onresume", list.size() +"  <<-----circleListData22");
			}
			else if(Command.CIRC_QUERY_PEOPLE.equals(cmd))
			{
				String reList = jo.get("resList").toString();
				List< HashMap<String, Object>> list = JsonUtil.getList(reList);
				adapter.setDataSource(list);
			}
		}
		else
		{
			this.showToast(errorInfo+"");
		}
	}
}
