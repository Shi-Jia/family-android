package com.yskj.ldm.db;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * <p>
 * Title:数据库建构及初始化类
 * 更改数据库需注意步骤(采取不删除表更新)：
 * 1，将数据库版本号加1
 * 2，将数据库表结构变化除了在createTable创建表中更新外，还要在方法updateTableChange里进行更改
 * 3，将更新后需要进行的逻辑处理写在upgradeConfig
 * @author haibo.wang
 * @version 1.0
 * @date 2012-8-18
 */
public class DatabaseHelper extends SQLiteOpenHelper
{

	private static final String DATABASE_NAME = "LAODAOMI.db"; // 数据库名称

	private static final int DATABASE_VERSION =1; // 数据库版本(改变版本号进行数据库更新需注意类上的注意步骤)

	private static final String TAG = "DatabaseHelper";

	private Context context;

	private String tableExtName = "";
	public DatabaseHelper(Context context,String databseName)
	{
		super(context, databseName, null, DATABASE_VERSION);
		this.context = context;

	}

	public String getTableExtName() {
		return tableExtName;
	}

	public void setTableExtName(String tableExtName) {
		this.tableExtName = tableExtName;
	}

	/**
	 * 数据库表创建
	 */
	public void onCreate(SQLiteDatabase db)
	{
		createTable(db, tableExtName);
	}


	/**
	 * 创建表
	 * 
	 * @param db
	 * @param tableExtName
	 * @author ：haibo.wang
	 * @date : 2012-8-18
	 */
	public void createTable(SQLiteDatabase db, String tableExtName)
	{
		// 用户信息表
		exeSQL(db,
				"CREATE TABLE IF NOT EXISTS USER_BASE (USER_ID text, CATEGORY_ID text,AREA_ID text,REG_DATE text,"
						+ "USER_NAME text, USER_PASSWORLD text,"
						+ "USER_SEX text,USER_NIKENAME text,USER_PHONE text,"
						+ "USER_EMAIL text,USER_QQ text,USER_BIRTH text,USER_GROUP text,"
						+ "USER_ADRESS text,USER_STATE text,USER_HEADPORTTAIT text,"
						+ "CLOUD_USERID text,CLOUD_CHANNELID text,CLOUD_DEVICE text,DEVICE_ID text"
						+ ")");
		if(db.isOpen())
			db.close();
	}
 

	//	/**
	//	 * 初初化系统需配置的数据库
	//	 * 
	//	 * @param db
	//	 */
	public void iniConfigData(SQLiteDatabase db)
	{
		//		db.execSQL("delete from TD_PH_DATA_SYN_TIME ");
		//		db.execSQL("insert into TD_PH_DATA_SYN_TIME(DATA_CODE,LAST_SYN_TIME)values(?,?)", new Object[] { "BASE_DATA", Constant.SYNTIME });

	}

	/**
	 * 执行SQL
	 * 
	 * @param db
	 * @param sql
	 */
	public void exeSQL(SQLiteDatabase db, String sql)
	{
		try
		{
			Log.i(TAG, sql);
			db.execSQL(sql);
		}
		catch (Exception e)
		{
			Log.e(TAG, e.getMessage());
		}
	}


	/**
	 * 数据库表更新
	 */
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		try
		{
			//1,改变数据库版本号，更改表结构执行
			updateTableChange(db);
			//2,改变数据库版本号后，需要处理的逻辑执行（包括对数据的处理等）
			upgradeConfig(db);
			if(db.isOpen())
				db.close();
		}
		catch (Exception e)
		{
			Log.e(TAG + ":onUpgrade",   e.getMessage());
		}

	}



	/**
	 * 表结构变化更新(改变数据库版本号，更改表结构执行)
	 * 
	 * @param db
	 * @author ：haibo.wang
	 * @date : 2012-6-27
	 */
	public void updateTableChange(SQLiteDatabase db)
	{
		try
		{
			// TEST表增加TESTNAME字段
			// exeSQL(db,"alter table TEST add TESTNAME TEXT");
			// 数据库升级时，清除内容并重新创建数据表
			db.execSQL("DROP TABLE IF EXISTS USER_BASE"+ this.tableExtName);

			//			onCreate(db);
		}
		catch (Exception e)
		{
			Log.e(TAG + ":updateTableChange", e.getMessage());
		}
	}

	/**
	 * 改变数据库版本号后，需要处理的逻辑执行（包括对数据的处理等）
	 * 
	 * @param db
	 */
	public void upgradeConfig(SQLiteDatabase db)
	{
		// 用户信息表
		exeSQL(db,
				"CREATE TABLE IF NOT EXISTS USER_BASE (USER_ID text, CATEGORY_ID text,AREA_ID text,REG_DATE text,"
						+ "USER_NAME text, USER_PASSWORLD text,"
						+ "USER_SEX text,USER_NIKENAME text,USER_PHONE text,"
						+ "USER_EMAIL text,USER_QQ text,USER_BIRTH text,USER_GROUP text,"
						+ "USER_ADRESS text,USER_STATE text,USER_HEADPORTTAIT text,"
						+ ")");
	}

}
