package com.yskj.ldm.db;

 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

 

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * @description：升级帮助类
 * @author: haibo.wang
 * @date : 2012-6-14
 */
public class UpgradeHelper {
	
	private static final String TAG = "UpgradeHelper";
	
	private Context context;
	
	private SQLiteDatabase db;
	
	public UpgradeHelper(Context context,SQLiteDatabase db){
		this.context = context;
		this.db = db;
	}

	/**
	 * 将指定的脚本文件执行
	 * @param fileName
	 * @author ：haibo.wang
	 * @date : 2012-6-14
	 */
	public void initDataFromAssets(String fileName) {
		InputStreamReader inputReader = null;
		BufferedReader bufReader = null;
		try {
			inputReader = new InputStreamReader(
					context.getResources().getAssets().open(fileName),"GBK");
			bufReader = new BufferedReader(inputReader);
			String line = "";
			StringBuffer sql = new StringBuffer();
			sql.append("");
			while ((line = bufReader.readLine()) != null){
				if(line.startsWith("insert")){
					if(!"".equals(sql.toString().trim())){//当前一次sql拼接有值时，执行
						db.execSQL(sql.toString());
					}
					sql.setLength(0);
					sql.append(line);
				}else if(!"".equals(line)){
					sql.append(line);
				}
			}
			
			if(!"".equals(sql.toString().trim())){//当最后一次sql拼接有值时，执行
				db.execSQL(sql.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				if(bufReader!=null){
					bufReader.close();
				}
				if(inputReader!=null){
					inputReader.close();
				}
			}catch(IOException e){
				Log.e(TAG, "关闭连接异常"+e.getMessage());
			}
		}
		Log.i(TAG, "初始化执行"+fileName+"脚本完成");
	}
	
	/**
	 * 是否是第一次安装系统
	 * @return
	 * @author ：haibo.wang
	 * @date : 2012-6-14
	 */
	public boolean isFirstInstall(){
//		Cursor cursor = db.rawQuery("select DATA_CODE from TD_PH_DATA_SYN_TIME where LAST_SYN_TIME = '"+Constant.SYNTIME+"' and DATA_CODE = 'BASE_DATA'  ", null);
//		if(cursor.getCount()>0){
//			return false;
//		}
//		cursor.close();
		return true;
	}
}

