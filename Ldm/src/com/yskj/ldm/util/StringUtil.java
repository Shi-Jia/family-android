package com.yskj.ldm.util;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

 


import com.yskj.ldm.activity.cache.RecordsPath;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class StringUtil {

	public static boolean isMobile(String mobile) {

		return Pattern.matches("^1\\d{10}$", mobile);
	}

	public static boolean isEmail(String email) {

		return email.contains("@");
	}

	public static boolean isNull(String str) {

		if (str == null) {
			return true;
		}
        if(str.equals("null"))
        {
        	return true;
        }
		if (str.trim().length() == 0) {
			return true;
		}

		return false;
	}

	public static float parseToFloat(String str, float defValue) {

		if (isNull(str)) {
			return defValue;
		}
		try {
			return Float.parseFloat(str);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return defValue;
	}

	public static int parseToInt(String str, int defValue) {

		if (isNull(str)) {
			return defValue;
		}
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return defValue;
	}

	public static String parseToString(String str, String defValue) {

		if (isNull(str)) {
			return defValue;
		}
		return str;
	}

	public static String getValidUrl(String host, String url) {
		String uri = "";
		if (url.startsWith("http://")) {
			return url;
		} else {
			if (url.startsWith("/")) {

				uri = host + url;
			} else {
				uri = host + "/" + url;
			}
		}
		System.out.println("uri \\" + uri);
		return uri;
	}

	public static String getValidationUrl(String url)
	{
		String uri = "";
		if(url.endsWith(RecordsPath.UriAssets))
		{
			uri = url;
		}
		else if(url.endsWith(RecordsPath.UriContent))
		{
			uri = url;
		}
		else if(url.endsWith(RecordsPath.UriDrawable))
		{
			uri = url;
		}
		else if(url.endsWith(RecordsPath.UriWeb))
		{
			uri = url;
		}
		else 
		{
			uri = RecordsPath.UriSDCard+SDCardStoreManager.getInstance().getRootPath()+RecordsPath.ImageRoot+url;
		}
		return uri;
	}


	public static String spell(String chinese) {
		if (chinese == null) {
			return null;
		}

		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.LOWERCASE); 
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE); 
		format.setVCharType(HanyuPinyinVCharType.WITH_V); 
		try {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < chinese.length(); i++) {
				String[] array = PinyinHelper.toHanyuPinyinStringArray(
						chinese.charAt(i), format);
				if (array == null || array.length == 0) {
					continue;
				}
				String s = array[0];// ���ܶ�����,ֻȡ��һ��
				char c = s.charAt(0);// ��д��һ����ĸ
				String pinyin = String.valueOf(c).toUpperCase()
						.concat(s.substring(1));
				sb.append(pinyin);
			}

			if (isNull(sb.toString())) {
				return chinese.charAt(0) + "";
			}

			return sb.toString();
		} catch (BadHanyuPinyinOutputFormatCombination e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getFirstLetter(String s) {
		String resutlStr = spell(s);
		return resutlStr.length() == 0 ? s.substring(0, 1) : resutlStr
				.substring(0, 1);
	}

}
