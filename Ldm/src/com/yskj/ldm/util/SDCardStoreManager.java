package com.yskj.ldm.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
 

import com.yskj.ldm.activity.cache.RecordsPath;

import android.os.Environment;
import android.os.Handler;

public class SDCardStoreManager {

	private static SDCardStoreManager instance;

	public static SDCardStoreManager getInstance() {

		if (instance == null) {
			instance = new SDCardStoreManager();
		}
		return instance;
	}

	public String getRootPath() {
		return Environment.getExternalStorageDirectory().getAbsolutePath();
	}
 
	public boolean checkSDCardState() {

		String sState = Environment.getExternalStorageState();

		return sState.equals(Environment.MEDIA_MOUNTED);
	}

	public boolean isExsit(String path) {

		File file = new File(getRootPath() + "" + path);

		return file.exists();
	}

	public boolean checkParentExsit(String path, boolean create) {

		File file = new File(getRootPath() + "" + path);

		File parent = file.getParentFile();

		if (parent.exists()) {
			return true;
		}

		if (create) {
			try {
				if (parent.mkdirs()) {
					return createFile(parent.getAbsolutePath() + "/.nomedia");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	/**
	 * �����ļ�
	 * 
	 * @param path
	 * @return
	 */
	public boolean createFile(String path) {

		File file = new File(path);
		if (!file.exists()) {
			try {
				return file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean createDirectory(String path) {

		File file = new File(getRootPath() + path);

		if (!file.exists()) {
			return file.mkdirs();
		}

		return true;
	}

	public File createNewFile(String path) {

		File file = new File(getRootPath() + path);
		if (file.exists()) {
			file.delete();
		}

		try {
			file.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}

	/**************************************************************************************
	 * 
	 * @param path
	 * @return
	 */
	public InputStream read(String path) {
		File file = new File( path);

		if (file.exists()) {

			try {
				return new FileInputStream(file);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public boolean write(String path, byte[] body) {

		//		File file = new File(getRootPath() + "" + path);
		File file = SDCardStoreManager.getInstance().getFilePath(getRootPath() , path);
		try {

			if (!file.exists()) {

				if (checkParentExsit(path, true)) {
					file.createNewFile();
				}
			}

			FileOutputStream output = new FileOutputStream(file);
			output.write(body);

			output.flush();
			output.close();

			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}



	public byte[] readBytes(InputStream in) {
		ByteArrayOutputStream baos = null;
		int c = 0;
		try {
			baos = new ByteArrayOutputStream();
			while ((c = in.read()) != -1) {
				baos.write((byte) c);
			}

			return baos.toByteArray();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				baos.close();
				in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			baos = null;
			in = null;
		}	 

		return null;
	}

	public String[] getFileList(String path){
		File file = new File(getRootPath() + path);
		if(file.exists() && file.isDirectory()){
			return file.list();
		}

		return null;
	}


	public boolean delete(String path){

		File file = new File(path);

		if(file.exists()){
			return file.delete();
		}

		return false;
	}


	public   File getFilePath(String filePath,
			String fileName) {
		File file = null;
		makeRootDirectory(filePath);
		try {
			file = new File(filePath + fileName);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return file;
	}
	public   void makeRootDirectory(String filePath) {
		File file = null;
		try {
			file = new File(filePath);
			if (!file.exists()) {
				file.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * ���ͼƬ����
	 * 
	 * @return
	 */
	public boolean clearCache(final Handler handler) {

		String dirPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + RecordsPath.ImageRoot;

		File dirFile = new File(dirPath);
		final File[] files = dirFile.listFiles();

		if (files.length == 0) {
			handler.sendMessage(handler.obtainMessage(2, "Ŀǰû�п�����Ļ���ͼƬ"));
		}

		final DecimalFormat dcmFmt = new DecimalFormat("0.00");

		new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < files.length; i++) {
					File file = files[i];
					boolean b = file.delete();
					float n = (float) (i + 1) / (float) files.length;
					String convertStr = dcmFmt.format(n);
					n = Float.parseFloat(convertStr);
					handler.sendMessage(handler.obtainMessage(1, n));
					if (!b) {
					}
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		return true;

	}
}
