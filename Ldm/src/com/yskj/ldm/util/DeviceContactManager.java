package com.yskj.ldm.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.yskj.ldm.beans.ContactElement;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.TextUtils;

 

/**
 * 
 
 * @author wanghaibo
 * @date 2013-4-4
 */

public class DeviceContactManager {
	
	
	private static final int PHONES_DISPLAY_NAME_INDEX = 0;
	private static final int PHONES_NUMBER_INDEX = 1;

	private static final String[] PHONES_PROJECTION = new String[]{
		Phone.DISPLAY_NAME,Phone.NUMBER
	};

	
	
	private Context context;
	
	private static DeviceContactManager instance;
	
	private HashMap<String,ContactElement> contacts = new HashMap<String,ContactElement>();
	
	private ArrayList<ContactElement> contactArr = new ArrayList<ContactElement>();
	
	private DeviceContactManager(Context context){
		this.context = context;
	}
	
	
	public static DeviceContactManager getInstance(Context context){
		
		if(instance == null){
			instance = new DeviceContactManager(context);
		}
		return instance;
	}
	
	
	public ContactElement  get(String phone){
		
		return getContacts().get(phone);
	}
	
	
	public HashMap<String,ContactElement> getContacts(){
		
		if(contacts.size() ==0){
			loadContacts();
		}
		return contacts;
	}
	
	
	public ArrayList<ContactElement> getContactArr(){
		
		if(contactArr.size()==0){
			loadContacts();
		}
		
		return contactArr;
	}

	
	public void reset(){
		
		for(ContactElement el:contactArr){
			el.checked = false;
		}
		
	}
	
	
	public void loadContacts(){
		
		contactArr.clear();
		contacts.clear();
		
		ContentResolver resolver = context.getContentResolver();    
	    //��ȡ�ֻ���ϵ��  
	    Cursor phoneCursor = resolver.query(Phone.CONTENT_URI,PHONES_PROJECTION, null, null, "sort_key asc");
	    
	    if (phoneCursor != null) {   
	    	
	    	
	        while (phoneCursor.moveToNext()) {   
	   
	        	 
		        String phoneNumber = phoneCursor.getString(PHONES_NUMBER_INDEX);   
		  
		        if (TextUtils.isEmpty(phoneNumber))  {
		            continue;   
		        }
		        if(!phoneNumber.startsWith("+86") && !phoneNumber.startsWith("1")){
		        	continue; 
		        }
		        
		        phoneNumber = phoneNumber.replace("+86", "");
		        
		        if(StringUtil.isMobile(phoneNumber)){
		           
		 
			        String contactName = phoneCursor.getString(PHONES_DISPLAY_NAME_INDEX); 
			        
			        ContactElement item = new ContactElement(contactName,phoneNumber);
			        item.letter = CharacterUtil.getSpells(contactName.substring(0, 1));
			        
			        if(!contacts.containsKey(phoneNumber)){
			        	
			        	contactArr.add(item);
				        contacts.put(phoneNumber,item);
			        }
		        }
		        
		    }   
	        phoneCursor.close();   
	    }   
	}
	
}


