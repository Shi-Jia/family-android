package com.yskj.ldm.util;

 


import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;





import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * 使用Gson把json字符串转成Map
 * @author  haibo.wang
 * @date 2014/06/12
 */
public class JsonUtil {


	public static String object2json(Object obj) {
		StringBuilder json = new StringBuilder();
		if (obj == null) {
			json.append("\"\"");
		} else if (obj instanceof String || obj instanceof Integer || obj instanceof Float
				|| obj instanceof Boolean || obj instanceof Short || obj instanceof Double
				|| obj instanceof Long || obj instanceof BigDecimal || obj instanceof BigInteger
				|| obj instanceof Byte) {
			json.append("\"").append(string2json(obj.toString())).append("\"");
		} else if (obj instanceof Object[]) {
			json.append(array2json((Object[]) obj));
		} else if (obj instanceof List) {
			json.append(list2json((List<?>) obj));
		} else if (obj instanceof Map) {
			json.append(map2json((Map<?, ?>) obj));
		} else if (obj instanceof Set) {
			json.append(set2json((Set<?>) obj));
		} else {
			//          json.append(bean2json(obj));
		}
		return json.toString();
	}
	//public static String bean2json(Object bean) {
	//        StringBuilder json = new StringBuilder();
	//        json.append("{");
	//        PropertyDescriptor[] props = null;
	//        try {
	//          props = Introspector.getBeanInfo(bean.getClass(), Object.class).getPropertyDescriptors();
	//        } catch (IntrospectionException e) {}
	//        if (props != null) {
	//          for (int i = 0; i < props.length; i++) {
	//            try {
	//              String name = object2json(props[i].getName());
	//              String value = object2json(props[i].getReadMethod().invoke(bean));
	//              json.append(name);
	//              json.append(":");
	//              json.append(value);
	//              json.append(",");
	//            } catch (Exception e) {}
	//          }
	//          json.setCharAt(json.length() - 1, '}');
	//        } else {
	//          json.append("}");
	//        }
	//        return json.toString();
	//}
	public static String list2json(List<?> list) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (list != null && list.size() > 0) {
			for (Object obj : list) {
				json.append(object2json(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		return json.toString();
	}
	public static String array2json(Object[] array) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (array != null && array.length > 0) {
			for (Object obj : array) {
				json.append(object2json(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		return json.toString();
	}
	public static String map2json(Map<?, ?> map) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		if (map != null && map.size() > 0) {
			for (Object key : map.keySet()) {
				json.append(object2json(key));
				json.append(":");
				json.append(object2json(map.get(key)));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, '}');
		} else {
			json.append("}");
		}
		return json.toString();
	}
	public static String set2json(Set<?> set) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (set != null && set.size() > 0) {
			for (Object obj : set) {
				json.append(object2json(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		return json.toString();
	}
	public static String string2json(String s) {
		if (s == null)
			return "";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			switch (ch) {
			case '"':
				sb.append("\\\"");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '/':
				sb.append("\\/");
				break;
			default:
				if (ch >= '\u0000' && ch <= '\u001F') {
					String ss = Integer.toHexString(ch);
					sb.append("\\u");
					for (int k = 0; k < 4 - ss.length(); k++) {
						sb.append('0');
					}
					sb.append(ss.toUpperCase());
				} else {
					sb.append(ch);
				}
			}
		}
		return sb.toString();
	}

	/**
	 * 获取JsonObject
	 * @param json
	 * @return
	 */
	public static JsonObject parseJson(String json){
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = parser.parse(json).getAsJsonObject();
		return jsonObj;
	}

	/**
	 * 根据json字符串返回Map对象
	 * @param json
	 * @return
	 */
	public static Map<String,Object> toMap(String json){
		return  toMap(parseJson(json));
	}

	/**
	 * 将JSONObjec对象转换成Map-List集合
	 * @param json
	 * @return
	 */
	public static Map<String, Object> toMap(JsonObject json){
		Map<String, Object> map = new HashMap<String, Object>();
		Set<Entry<String, JsonElement>> entrySet = json.entrySet();
		for (Iterator<Entry<String, JsonElement>> iter = entrySet.iterator(); iter.hasNext(); ){
			Entry<String, JsonElement> entry = iter.next();
			String key = entry.getKey();
			Object value = entry.getValue();
			if(value instanceof JsonArray)
				map.put((String) key, toList((JsonArray) value));
			else if(value instanceof JsonObject)
				map.put((String) key, toMap((JsonObject) value));
			else
				map.put((String) key, value);
		}
		return map;
	}
	public static JsonArray toJsonArray(String jstring)
	{
		JsonParser parser = new JsonParser(); 
		JsonArray Jarray = parser.parse(jstring).getAsJsonArray();
		return Jarray;
	}

	public static List<Object> toList(String json)
	{
		return  toList(toJsonArray(json));
	}
	/**
	 * 将JSONArray对象转换成List集合
	 * @param json
	 * @return
	 */
	public static List<Object> toList(JsonArray json){
		List<Object> list = new ArrayList<Object>();
		for (int i=0; i<json.size(); i++){
			Object value = json.get(i);
			if(value instanceof JsonArray){
				list.add(toList((JsonArray) value));
			}
			else if(value instanceof JsonObject){
				list.add(toMap((JsonObject) value));
			}
			else{
				list.add(value);
			}
		}
		return list;
	}
	/**
	 * getJSON(将用json格式字符串转JSONObject对象)
	 * @param jsonString
	 * @return
	 */
	public static JSONObject getJSON(String jsonString) {
		JSONObject obj = new JSONObject();

		try {
			obj = new JSONObject(jsonString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	/**
	 * getJsonArray(将用json格式字符串转JSONArray对象)
	 * @param jsonString
	 * @return
	 */
	public static JSONArray getJsonArray(String jsonString){
		JSONArray jsonArray = new JSONArray();
		try {
			jsonArray = new JSONArray(jsonString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArray;
	}

	/**
	 * getMap(将用json格式字符串转换为 map对象)
	 * @param jsonString
	 * @return
	 */
	public static HashMap<String, Object> getMap(String jsonString) {
		JSONObject jsonObject = getJSON(jsonString);
		HashMap<String, Object> valueMap = new HashMap<String, Object>();
		try {
			@SuppressWarnings("unchecked")
			Iterator<String> keyIter = jsonObject.keys();
			String key;
			Object value;
			while (keyIter.hasNext()) {
				key = (String) keyIter.next();
				value = jsonObject.get(key);
				valueMap.put(key, value);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return valueMap;
	}

	/**
	 * getList(把json 转换为 ArrayList 形式)
	 * @param jsonString
	 * @return
	 */
	public static List<HashMap<String, Object>> getList(String jsonString) {
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		JSONArray jsonArray = getJsonArray(jsonString);
		try {
			JSONObject jsonObject;
			for (int i = 0; i < jsonArray.length(); i++) {
				jsonObject = jsonArray.getJSONObject(i);
				list.add(getMap(jsonObject.toString()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * getKeyValue(从JSONObject中获取指定key的值)
	 * @param jsonobj
	 * @param name
	 * @return
	 */
	public static String getKeyValue(JSONObject jsonobj, String name) {
		try {
			return jsonobj.getString(name);
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 把对象封装为JSON格式
	 * 
	 * @param o
	 *            对象
	 * @return JSON格式
	 */
	@SuppressWarnings("unchecked")
	public static String toJson(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		if (o instanceof String) // String
		{
			return string2Json((String) o);
		}
		if (o instanceof Boolean) // Boolean
		{
			return boolean2Json((Boolean) o);
		}
		if (o instanceof Number) // Number
		{
			return number2Json((Number) o);
		}
		if (o instanceof Map) // Map
		{
			return map2Json((Map<String, Object>) o);
		}
		if (o instanceof Collection) // List Set
		{
			return collection2Json((Collection) o);
		}
		if (o instanceof Object[]) // 对象数组
		{
			return array2Json((Object[]) o);
		}
		if (o instanceof int[])// 基本类型数组
		{
			return intArray2Json((int[]) o);
		}
		if (o instanceof boolean[])// 基本类型数组
		{
			return booleanArray2Json((boolean[]) o);
		}
		if (o instanceof long[])// 基本类型数组
		{
			return longArray2Json((long[]) o);
		}
		if (o instanceof float[])// 基本类型数组
		{
			return floatArray2Json((float[]) o);
		}
		if (o instanceof double[])// 基本类型数组
		{
			return doubleArray2Json((double[]) o);
		}
		if (o instanceof short[])// 基本类型数组
		{
			return shortArray2Json((short[]) o);
		}
		if (o instanceof byte[])// 基本类型数组
		{
			return byteArray2Json((byte[]) o);
		}
		if (o instanceof Object) // 保底收尾对象
		{
			return object2Json(o);
		}
		throw new RuntimeException("不支持的类型: " + o.getClass().getName());
	}

	/**
	 * 将 String 对象编码为 JSON格式，只需处理好特殊字符
	 * 
	 * @param s
	 *            String 对象
	 * @return JSON格式
	 */
	static String string2Json(final String s)
	{
		final StringBuilder sb = new StringBuilder(s.length() + 20);
		sb.append('\"');
		for (int i = 0; i < s.length(); i++)
		{
			final char c = s.charAt(i);
			switch (c)
			{
			case '\"':
				sb.append("\\\"");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '/':
				sb.append("\\/");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			default:
				sb.append(c);
			}
		}
		sb.append('\"');
		return sb.toString();
	}

	/**
	 * 将 Number 表示为 JSON格式
	 * 
	 * @param number
	 *            Number
	 * @return JSON格式
	 */
	static String number2Json(final Number number)
	{
		return number.toString();
	}

	/**
	 * 将 Boolean 表示为 JSON格式
	 * 
	 * @param bool
	 *            Boolean
	 * @return JSON格式
	 */
	static String boolean2Json(final Boolean bool)
	{
		return bool.toString();
	}

	/**
	 * 将 Collection 编码为 JSON 格式 (List,Set)
	 * 
	 * @param c
	 * @return
	 */
	static String collection2Json(final Collection<Object> c)
	{
		final Object[] arrObj = c.toArray();
		return toJson(arrObj);
	}

	/**
	 * 将 Map<String, Object> 编码为 JSON 格式
	 * 
	 * @param map
	 * @return
	 */
	static String map2Json(final Map<String, Object> map)
	{
		if (map.isEmpty())
		{
			return "{}";
		}
		final StringBuilder sb = new StringBuilder(map.size() << 4); // 4次方
		sb.append('{');
		final Set<String> keys = map.keySet();
		for (final String key : keys)
		{
			final Object value = map.get(key);
			sb.append('\"');
			sb.append(key); // 不能包含特殊字符
			sb.append('\"');
			sb.append(':');
			sb.append(toJson(value)); // 循环引用的对象会引发无限递归
			sb.append(',');
		}
		// 将最后的 ',' 变为 '}':
		sb.setCharAt(sb.length() - 1, '}');
		return sb.toString();
	}

	/**
	 * 将数组编码为 JSON 格式
	 * 
	 * @param array
	 *            数组
	 * @return JSON 格式
	 */
	static String array2Json(final Object[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4); // 4次方
		sb.append('[');
		for (final Object o : array)
		{
			sb.append(toJson(o));
			sb.append(',');
		}
		// 将最后添加的 ',' 变为 ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	static String intArray2Json(final int[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4);
		sb.append('[');
		for (final int o : array)
		{
			sb.append(Integer.toString(o));
			sb.append(',');
		}
		// set last ',' to ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	static String longArray2Json(final long[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4);
		sb.append('[');
		for (final long o : array)
		{
			sb.append(Long.toString(o));
			sb.append(',');
		}
		// set last ',' to ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	static String booleanArray2Json(final boolean[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4);
		sb.append('[');
		for (final boolean o : array)
		{
			sb.append(Boolean.toString(o));
			sb.append(',');
		}
		// set last ',' to ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	static String floatArray2Json(final float[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4);
		sb.append('[');
		for (final float o : array)
		{
			sb.append(Float.toString(o));
			sb.append(',');
		}
		// set last ',' to ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	static String doubleArray2Json(final double[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4);
		sb.append('[');
		for (final double o : array)
		{
			sb.append(Double.toString(o));
			sb.append(',');
		}
		// set last ',' to ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	static String shortArray2Json(final short[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4);
		sb.append('[');
		for (final short o : array)
		{
			sb.append(Short.toString(o));
			sb.append(',');
		}
		// set last ',' to ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	static String byteArray2Json(final byte[] array)
	{
		if (array.length == 0)
		{
			return "[]";
		}
		final StringBuilder sb = new StringBuilder(array.length << 4);
		sb.append('[');
		for (final byte o : array)
		{
			sb.append(Byte.toString(o));
			sb.append(',');
		}
		// set last ',' to ']':
		sb.setCharAt(sb.length() - 1, ']');
		return sb.toString();
	}

	public static String object2Json(final Object bean)
	{
		// 数据检查
		if (bean == null)
		{
			return "{}";
		}
		final Method[] methods = bean.getClass().getMethods(); // 方法数组
		final StringBuilder sb = new StringBuilder(methods.length << 4); // 4次方
		sb.append('{');
		for (final Method method : methods)
		{
			try
			{
				final String name = method.getName();
				String key = "";
				if (name.startsWith("get"))
				{
					key = name.substring(3);
					// 防死循环
					final String[] arrs = { "Class" };
					boolean bl = false;
					for (final String s : arrs)
					{
						if (s.equals(key))
						{
							bl = true;
							continue;
						}
					}
					if (bl)
					{
						continue; // 防死循环
					}
				}
				else if (name.startsWith("is"))
				{
					key = name.substring(2);
				}
				if (key.length() > 0 && Character.isUpperCase(key.charAt(0)) && method.getParameterTypes().length == 0)
				{
					if (key.length() == 1)
					{
						key = key.toLowerCase();
					}
					else if (!Character.isUpperCase(key.charAt(1)))
					{
						key = key.substring(0, 1).toLowerCase() + key.substring(1);
					}
					final Object elementObj = method.invoke(bean);
					// System.out.println("###" + key + ":" + elementObj.toString());
					sb.append('\"');
					sb.append(key); // 不能包含特殊字符
					sb.append('\"');
					sb.append(':');
					sb.append(toJson(elementObj)); // 循环引用的对象会引发无限递归
					sb.append(',');
				}
			}
			catch (final Exception e)
			{
				// e.getMessage();
				throw new RuntimeException("在将bean封装成JSON格式时异常：" + e.getMessage(), e);
			}
		}
		if (sb.length() == 1)
		{
			return bean.toString();
		}
		else
		{
			sb.setCharAt(sb.length() - 1, '}');
			return sb.toString();
		}
	}
	/**
	 * 将Map转换为JSON格式数据
	 * 从Map中抽取数据返回JSON格式数据
	 * @param map Map<String, String[]>
	 * @return JSONObject
	 * @throws JSONException 
	 */

	public static JSONObject mapToJson(Map<String, String[]> map) throws JSONException
	{
		if (null == map)
		{
			return null;
		}
		Set<Map.Entry<String, String[]>> set = map.entrySet();
		JSONObject jObject = new JSONObject();

		for (Iterator<Map.Entry<String, String[]>> it = set.iterator(); it.hasNext();)
		{
			Map.Entry<String, String[]> entry = it.next();
			jObject.put(entry.getKey(), entry.getValue()[0]);
		}
		return jObject;
	}
	/**
	 * map转json
	 * @param map
	 * @return
	 */
	public static  String mapToJson1(Map<String,String> map){
		if(map==null||map.size()<1){
			return null;
		}
		StringBuffer buffer=new StringBuffer();
		buffer.append("{\"");
		for (Iterator<String> iterator = map.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			buffer.append(key);
			buffer.append("\":\"");
			buffer.append(map.get(key));
			buffer.append("\"");
			if(iterator.hasNext()){
				buffer.append(",\"");
			}
		}
		buffer.append("}"); 
		return buffer.toString(); 
	}
}
