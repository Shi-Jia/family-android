package com.yskj.ldm.util;

import java.util.HashMap;

public class Session {

	private static Session instance;
	
	private HashMap<String,Object> map = new HashMap<String,Object>();
	
	
	public static Session getSession(){
		
		if(instance == null){
			instance = new Session();
		}
		
		return instance;
	}
	
	public void put(String key,Object obj){
		map.put(key, obj);
	}
	
	
	public Object get(String key){
		
		if(map.containsKey(key)){
			
			return map.get(key);
		}
	
		return null;
	}
	
	public Object getAndRemove(String key){
		
		try{
			if(map.containsKey(key)){
				
				return map.get(key);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			map.remove(key);
		}
		
		return null;
	}
	
}
