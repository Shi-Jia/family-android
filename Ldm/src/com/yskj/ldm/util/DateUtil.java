package com.yskj.ldm.util;

 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <p> Title: </p>
 * <p> Description: 日期工具类  </p>
 * <p> Copyright:Copyright (c) 2012  </p>
 * <p> Company:湖南科创  </p> 
 * @author  haibo.wang
 * @version 1.0
 * @date 2012-5-9
 */

public class DateUtil {

	/**
	 * main方法
	 * @param args
	 */
	public static void main(String[] args) {
		
	}
	
	/**
	 * 根据时间段，得出当前时间是 白天还是晚上
	 * @param String begTime (08:00) 
	 * @param String endTime (20:00) 
	 * @return 白天/黑夜
	 */
	public static String getIsNight(String begTime,String endTime) {
		String isNight = "";
		String nowTime = getCurrentDateTime();
		String begdate = getCurrentDate()+" "+begTime+":00";
		long i = DateUtil.getDiff(nowTime,begdate);
		
		String dateEnd = getCurrentDate()+" "+endTime+":00";
		long j = DateUtil.getDiff(nowTime,dateEnd);
		
		if(i<=0 && j>=0) {
			isNight = "day";
		} else {
			isNight = "night";
		}
		return isNight;
	}

	
	/**
	 * 返回当前时间字符串。 格式：yyyy-MM-dd
	 * @return String 指定格式的日期字符串.
	 */
	public static String getCurrentDate() {
		return getFormatDateTime(new Date(), "yyyy-MM-dd");
	}

	/**
	 * 返回当前指定的时间戳。格式为yyyy-MM-dd HH:mm:ss
	 * @return 格式为yyyy-MM-dd HH:mm:ss，总共19位。
	 */
	public static String getCurrentDateTime() {
		return getFormatDateTime(new Date(), "yyyy-MM-ddHH:mm:ss");
	}
	
	/**
	 * 根据给定的格式与时间(Date类型的)，返回时间字符串。最为通用。
	 * @param date 指定的日期
	 * @param format 日期格式字符串
	 * @return String 指定格式的日期字符串.
	 */
	public static String getFormatDateTime(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	/**
	 * 
	 * 获取两个时间串时间的差值，单位为秒
	 * @param startTime  开始时间 yyyy-MM-dd HH:mm:ss
	 * @param endTime 结束时间 yyyy-MM-dd HH:mm:ss
	 * @return 两个时间的差值(秒)
	 */
	public static long getDiff(String startTime, String endTime) {
		long diff = 0;
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date startDate = ft.parse(startTime);
			Date endDate = ft.parse(endTime);
			diff = endDate.getTime() - startDate.getTime();
			diff = diff / 1000;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return diff;
	}
	
	/**
	 * 获取当前日期的后一天
	 * @return 当前日期+1
	 */
	
	public static String nextDate() {
		
		Calendar cal = Calendar.getInstance();//使用默认时区和语言环境获得一个日历。    
		cal.add(Calendar.DAY_OF_MONTH, +1);//取当前日期的后一天.    
		//通过格式化输出日期    
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");    
 
		return format.format(cal.getTime());
	}
	
	/**
	 * 获取当前日期的前一天
	 * @return 当前日期+1
	 */
    public static String preDay(int dayNums)
    {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, dayNums);    //得到前一天
		Date date = calendar.getTime();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(date);
    }
}
