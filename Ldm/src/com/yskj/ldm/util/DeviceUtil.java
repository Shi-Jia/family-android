package com.yskj.ldm.util;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;

public class DeviceUtil {
	/***
	 * 当前版本名
	 */
	
	public static int getVersionCode(Context c) {

		try {
			PackageManager packageManager = c.getPackageManager();
			PackageInfo packInfo = packageManager.getPackageInfo(c.getPackageName(), 0);
			return packInfo.versionCode;
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	
	/**
	 * 当前的版本名
	 * @param c
	 * @return
	 */
	public static String getVersionName(Context c) {
		try {
			PackageManager packageManager = c.getPackageManager();
			PackageInfo packInfo = packageManager.getPackageInfo(c.getPackageName(), 0);
			return packInfo.versionName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 手机IMEI号
	 * @param c
	 * @return
	 */
	public static String getMobileID(Context c) {
		
		TelephonyManager telephonemanage = (TelephonyManager)c.getSystemService(Context.TELEPHONY_SERVICE);
		
		String phoneID = telephonemanage.getDeviceId();
		if(phoneID == null || "".equals(phoneID)) 
		{
			phoneID = String.valueOf(System.currentTimeMillis()); 
		}

		return phoneID;
	}
	
	
}
