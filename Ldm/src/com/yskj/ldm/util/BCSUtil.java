package com.yskj.ldm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;
 
 
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
 


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class BCSUtil {
	public static final String TAG = "PushDemoActivity";
	public static final String RESPONSE_METHOD = "method";
	public static final String RESPONSE_CONTENT = "content";
	public static final String RESPONSE_ERRCODE = "errcode";
	protected static final String ACTION_LOGIN = "com.baidu.pushdemo.action.LOGIN";
	public static final String ACTION_MESSAGE = "com.baiud.pushdemo.action.MESSAGE";
	public static final String ACTION_RESPONSE = "bccsclient.action.RESPONSE";
	public static final String ACTION_SHOW_MESSAGE = "bccsclient.action.SHOW_MESSAGE";
	protected static final String EXTRA_ACCESS_TOKEN = "access_token";
	public static final String EXTRA_MESSAGE = "message";

	public static String logStringCache = "";

	// 获取ApiKey
	public static String getMetaValue(Context context, String metaKey) {
		Bundle metaData = null;
		String apiKey = null;
		if (context == null || metaKey == null) {
			return null;
		}
		try {
			ApplicationInfo ai = context.getPackageManager()
					.getApplicationInfo(context.getPackageName(),
							PackageManager.GET_META_DATA);
			if (null != ai) {
				metaData = ai.metaData;
			}
			if (null != metaData) {
				apiKey = metaData.getString(metaKey);
			}
		} catch (NameNotFoundException e) {

		}
		return apiKey;
	}


	public static boolean hasBind(Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		String flag = sp.getString("bind_flag", "");
		if ("ok".equalsIgnoreCase(flag)) {
			return true;
		}
		return false;
	}

	public static void setBind(Context context, boolean flag) {
		String flagStr = "not";
		if (flag) {
			flagStr = "ok";
		}
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sp.edit();
		editor.putString("bind_flag", flagStr);
		editor.commit();
	}

	public static List<String> getTagsList(String originalText) {
		if (originalText == null || originalText.equals("")) {
			return null;
		}
		List<String> tags = new ArrayList<String>();
		int indexOfComma = originalText.indexOf(',');
		String tag;
		while (indexOfComma != -1) {
			tag = originalText.substring(0, indexOfComma);
			tags.add(tag);

			originalText = originalText.substring(indexOfComma + 1);
			indexOfComma = originalText.indexOf(',');
		}

		tags.add(originalText);
		return tags;
	}

	public static String getLogText(Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString("log_text", "");
	}

	public static void setLogText(Context context, String text) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sp.edit();
		editor.putString("log_text", text);
		editor.commit();
	}



	public static long getFileSizes(File f) throws Exception{//ȡ���ļ���С
		long s=0;
		if (f.exists()) {
			FileInputStream fis = null;
			fis = new FileInputStream(f);
			s= fis.available();
		} else {
			f.createNewFile();
			System.out.println("�ļ�������");
		}
		return s;
	}
	/**
	 * ��Ƭ�����ϴ�
	 * @param url ��ǩ������Ƭ���ƶ˵�ַURL
	 * @param fileName ������Ƭ����
	 * @return
	 */
	public static boolean putObject(String url,String fileName) throws Exception{
		HttpClient httpclient = new DefaultHttpClient();
		HttpPut httpput=new HttpPut(url);
		File file = new File(fileName);
		boolean flag=true;
		try
		{
			HttpEntity entity=new FileEntity(file,"binary/octet-stream");

			httpput.setEntity(entity);
			httpput.addHeader("Cache-Control", "no-cache");//���軺��
			httpput.addHeader("Content-Type", "binary/octet-stream"); //�ļ�����
			HttpResponse response=httpclient.execute(httpput);
			System.out.println("�ϴ���Ƭ��"+response.getStatusLine());
		}catch(Exception e){
			e.printStackTrace();
			flag=false;
		}
		return flag;
	}

     public static void main(String args[])
     {
    	 try {
			putObject("http://bcs.duapp.com/laodaomi/%2F201475%2F1404543050783?sign=MBO:uOrXDZ2ojGbfTG8QXFniRkw4:rt0%2Bp9R%2B7Vs4X3OOmp9e0EXTxxg%3D","H://ǧ����//���//backclick.png");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     }
	/**
	 * ��Ƭɾ����ֻ�й���Ȩ�޵Ĳ��д˹��ܣ�
	 * @param url ��ǩ������Ƭ���ƶ˵�ַURL 
	 * @return
	 */
	public static boolean deleteObject(String url) throws Exception{
		HttpClient httpclient = new DefaultHttpClient();
		HttpDelete httpdel=new HttpDelete(url);
		boolean flag=true;
		try
		{
			HttpResponse response=httpclient.execute(httpdel);
			System.out.println("ɾ����Ƭ��"+response.getStatusLine());			
		}catch(Exception e){
			e.printStackTrace();
			flag=false;
		}
		return flag;
	}
	public static String getString (String url) throws Exception
	{
		//��URL�����ƴ��
		HttpGet getMethod = new HttpGet(url );
		HttpClient httpClient = new DefaultHttpClient();

		HttpResponse response = httpClient.execute(getMethod); //����GET����
		Log.i("TAG", "result = " +EntityUtils.toString(response.getEntity(), "utf-8") );//��ȡ��������Ӧ����
		return EntityUtils.toString(response.getEntity(), "utf-8");
	}
	/**
	 * ������Ƭ
	 * @param url ��ȡ��ַURL
	 * @param saveTo ���ر��浽���ص�ַ�������ļ�����
	 * @return
	 */
	public static boolean getObject(String url,String saveTo){
		boolean flag=true;
		OutputStream out=null;
		InputStream is=null;
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget=new HttpGet(url);
			httpget.addHeader("Cache-Control", "no-cache");//���軺��
			httpget.addHeader("Content-Type", "binary/octet-stream"); //�ļ�����
			HttpResponse response=httpclient.execute(httpget);
			System.out.println("������Ƭ:"+response.getStatusLine());
			is=response.getEntity().getContent();
			//			String SDCard=Environment.getExternalStorageDirectory()+"";  
			//            String pathName=SDCard+"/"+path+"/"+fileName;//�ļ��洢·��  
			File target=new File(saveTo);
			//			if(target.exists()){  
			System.out.println("exits");  
			//			}else{ 
			//				new File(saveTo).mkdir();//�½��ļ���  
			target.createNewFile();//�½��ļ�  
			//			}
			out=new FileOutputStream(target);
			byte[] byts=new byte[512];
			int r=0;
			while((r=is.read(byts))>-1){
				out.write(byts, 0, r);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			flag=false;
		}finally{
			try{
				out.close();
				is.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return flag;
	}	
}
