package com.yskj.ldm.beans;

public class CircleBean {

	Integer circle_id;
	String circle_name;
	String  circle_num;
	String user_count;
	Integer position;
	
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public Integer getCircle_id() {
		return circle_id;
	}
	public void setCircle_id(Integer circle_id) {
		this.circle_id = circle_id;
	}
	public String getCircle_name() {
		return circle_name;
	}
	public void setCircle_name(String circle_name) {
		this.circle_name = circle_name;
	}
	public String getCircle_num() {
		return circle_num;
	}
	public void setCircle_num(String circle_num) {
		this.circle_num = circle_num;
	}
	public String getUser_count() {
		return user_count;
	}
	public void setUser_count(String user_count) {
		this.user_count = user_count;
	}
	
}
