package com.yskj.ldm.beans;

public class GalleryBean {

	Integer id;
	String path;
	Integer gallery_id;
	String gallery_name;
	Integer count;
   
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getGallery_id() {
		return gallery_id;
	}
	public void setGallery_id(Integer gallery_id) {
		this.gallery_id = gallery_id;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
 
	public String getGallery_name() {
		return gallery_name;
	}
	public void setGallery_name(String gallery_name) {
		this.gallery_name = gallery_name;
	}
 
}
