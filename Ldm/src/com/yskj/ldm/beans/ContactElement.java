package com.yskj.ldm.beans;

 

public class ContactElement {
	
	private String name;
	private String phoneNum;
	
	public String letter;
	
	public boolean checked;
	public int status = 0; // 0：未发送， -1：失败 ; 1:完成
	
	
	public ContactElement(){
		
	}

	
	public ContactElement(String name,String mobile){
		this.name = name;
		this.phoneNum = mobile;
	}
	
	public void reset(){
		checked = false;
		status = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPhoneNum() {
		return phoneNum;
	}
	
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	
}
