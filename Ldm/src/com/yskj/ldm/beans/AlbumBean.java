package com.yskj.ldm.beans;

public class AlbumBean {

	String user_id;	
	String album_name;	
	String album_instructions;	
	String path;	
	String album_size;
	String create_time;
	
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAlbum_name() {
		return album_name;
	}
	public void setAlbum_name(String album_name) {
		this.album_name = album_name;
	}
	public String getAlbum_instructions() {
		return album_instructions;
	}
	public void setAlbum_instructions(String album_instructions) {
		this.album_instructions = album_instructions;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getAlbum_size() {
		return album_size;
	}
	public void setAlbum_size(String album_size) {
		this.album_size = album_size;
	}	
}
