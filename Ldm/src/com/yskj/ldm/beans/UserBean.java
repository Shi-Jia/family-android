package com.yskj.ldm.beans;

public class UserBean {

	String   USER_ID ;        
	String   CATEGORY_ID ;      
	String   AREA_ID  ;       
	String   REG_DATE  ;      
	String   USER_NAME ;       
	String   USER_PASSWORLD ;      
	String   USER_SEX ;          
	String   USER_NIKENAME ;   
	String   USER_PHONE ;          
	String   USER_EMAIL ;          
	String   USER_QQ ;           
	String   USER_BIRTH ;        
	String   USER_GROUP ;     
	String   USER_ADRESS ;      
	String   USER_STATE  ;    
	String   USER_HEADPORTTAIT;    
	String   CLOUD_USERID ;      
	String   CLOUD_CHANNELID  ;     
	String   CLOUD_DEVICE ;    
	String   DEVICE_ID ;
	String   CIRCLE_ID;
	
	
	public String getCIRCLE_ID() {
		return CIRCLE_ID;
	}
	public void setCIRCLE_ID(String cIRCLE_ID) {
		CIRCLE_ID = cIRCLE_ID;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getCATEGORY_ID() {
		return CATEGORY_ID;
	}
	public void setCATEGORY_ID(String cATEGORY_ID) {
		CATEGORY_ID = cATEGORY_ID;
	}
	public String getAREA_ID() {
		return AREA_ID;
	}
	public void setAREA_ID(String aREA_ID) {
		AREA_ID = aREA_ID;
	}
	public String getREG_DATE() {
		return REG_DATE;
	}
	public void setREG_DATE(String rEG_DATE) {
		REG_DATE = rEG_DATE;
	}
	public String getUSER_NAME() {
		return USER_NAME;
	}
	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}
	public String getUSER_PASSWORLD() {
		return USER_PASSWORLD;
	}
	public void setUSER_PASSWORLD(String uSER_PASSWORLD) {
		USER_PASSWORLD = uSER_PASSWORLD;
	}
	public String getUSER_SEX() {
		return USER_SEX;
	}
	public void setUSER_SEX(String uSER_SEX) {
		USER_SEX = uSER_SEX;
	}
	public String getUSER_NIKENAME() {
		return USER_NIKENAME;
	}
	public void setUSER_NIKENAME(String uSER_NIKENAME) {
		USER_NIKENAME = uSER_NIKENAME;
	}
	public String getUSER_PHONE() {
		return USER_PHONE;
	}
	public void setUSER_PHONE(String uSER_PHONE) {
		USER_PHONE = uSER_PHONE;
	}
	public String getUSER_EMAIL() {
		return USER_EMAIL;
	}
	public void setUSER_EMAIL(String uSER_EMAIL) {
		USER_EMAIL = uSER_EMAIL;
	}
	public String getUSER_QQ() {
		return USER_QQ;
	}
	public void setUSER_QQ(String uSER_QQ) {
		USER_QQ = uSER_QQ;
	}
	public String getUSER_BIRTH() {
		return USER_BIRTH;
	}
	public void setUSER_BIRTH(String uSER_BIRTH) {
		USER_BIRTH = uSER_BIRTH;
	}
	public String getUSER_GROUP() {
		return USER_GROUP;
	}
	public void setUSER_GROUP(String uSER_GROUP) {
		USER_GROUP = uSER_GROUP;
	}
	public String getUSER_ADRESS() {
		return USER_ADRESS;
	}
	public void setUSER_ADRESS(String uSER_ADRESS) {
		USER_ADRESS = uSER_ADRESS;
	}
	public String getUSER_STATE() {
		return USER_STATE;
	}
	public void setUSER_STATE(String uSER_STATE) {
		USER_STATE = uSER_STATE;
	}
	public String getUSER_HEADPORTTAIT() {
		return USER_HEADPORTTAIT;
	}
	public void setUSER_HEADPORTTAIT(String uSER_HEADPORTTAIT) {
		USER_HEADPORTTAIT = uSER_HEADPORTTAIT;
	}
	public String getCLOUD_USERID() {
		return CLOUD_USERID;
	}
	public void setCLOUD_USERID(String cLOUD_USERID) {
		CLOUD_USERID = cLOUD_USERID;
	}
	public String getCLOUD_CHANNELID() {
		return CLOUD_CHANNELID;
	}
	public void setCLOUD_CHANNELID(String cLOUD_CHANNELID) {
		CLOUD_CHANNELID = cLOUD_CHANNELID;
	}
	public String getCLOUD_DEVICE() {
		return CLOUD_DEVICE;
	}
	public void setCLOUD_DEVICE(String cLOUD_DEVICE) {
		CLOUD_DEVICE = cLOUD_DEVICE;
	}
	public String getDEVICE_ID() {
		return DEVICE_ID;
	}
	public void setDEVICE_ID(String dEVICE_ID) {
		DEVICE_ID = dEVICE_ID;
	}          
	
	
}
