package com.yskj.ldm.network;







import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.android.commu.agent.Manager;
import com.android.commu.net.ICommuDataListener;
import com.android.commu.net.RequestTask;
import com.android.commu.parse.IResponseParser;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.yskj.ldm.util.Base64;
import com.yskj.ldm.util.Encrypt;
import com.yskj.ldm.util.StringUtil;




/**
 * @description:
 * @author: wanghaibo
 * @create: 2013-4-3
 */

public class ConnectionManager extends Manager {

	private static ConnectionManager instance;
	//		public static final String SERVER_HOST = "http://115.29.77.64:8081/api";
	public static final String SERVER_HOST = "http://115.29.77.64:8081/api";

	//	public static final String SERVER_HOST = "http://192.168.1.107:8081/api";

	public static final String IMAGE_HOST = "http://192.168.1.118:8080/aliyun";

	public static final int Transport_Stream = 1;
	public static final int Transport_No_Form_Upload_File = 3;
	public static  String TOKEN_SERVER_HOST = "";
	private IResponseParser parse = null;

	public ConnectionManager() {
		parse = new ResponseParseFactory();
	}

	public static ConnectionManager getInstance() {

		if (instance == null) {
			instance = new ConnectionManager();
		}
		return instance;
	}
	public boolean setToken(String userid,String device_id,String token,String time)
	{
		if(StringUtil.isNull(userid)||StringUtil.isNull(token)||StringUtil.isNull(token)||StringUtil.isNull(device_id))
			return false;
		else
			TOKEN_SERVER_HOST = "?token="+token+"&time="+time+"&userid="+userid+"&device_id="+device_id;
		return true;
	}
	/**
	 * 
	 * @param userCode
	 * @param userPwd
	 * @param verify
	 * @param real
	 * @param email
	 * @param progress
	 * @param listener
	 */
	public void requestUserRegister(String phone,String nickname,String password,String smscode,String deviceId,  boolean progress,
			ICommuDataListener listener) {
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/userRegisterUpdate");
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_phone", phone);
		jsonMap.put("user_nikeName", nickname);
		jsonMap.put("user_password", Encrypt.md5(password));
		jsonMap.put("smscode", smscode);
		jsonMap.put("app_type", 3);
		jsonMap.put("device_id", deviceId);
		//		jsonMap.put("cloud_appid", appid);
		//		jsonMap.put("cloud_channelId", cloud_channelId);
		//		jsonMap.put("cloud_requestId", cloud_requestId);
		//		jsonMap.put("cloud_userId", cloud_userId);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.USER_REGISTER, progress,
				listener);
	}

	public void requestUploadFile(String URL,String filePath, boolean progress,
			ICommuDataListener listener){
		RequestTask task = new RequestTask();
		task.setUrl(URL);
		task.setTransport(ConnectionManager.Transport_No_Form_Upload_File);
		task.setFilepath(filePath);
		task.setHeadProperties("Cache-Control", "no-cache"); 
		task.setHeadProperties("Content-Type", "binary/octet-stream");  
		this.sendRequest(task, Command.PHO_UPLOAD, progress,
				listener);
	}


	public void requestuserSendmsgToUser(Integer userId,Integer belongUser,String content,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/userSendmsgToUser"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_id", userId);
		jo.addProperty("belong_user", belongUser);
		jo.addProperty("message_title", "");
		jo.addProperty("message_content", content);
		jo.addProperty("remarks", "");
		task.setBodyStream(jo.toString().getBytes());  
		//		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.SEND_CHAT_MESSAGE, progress,
				listener);
	}
	public void requestMsgboxQueryBygroupid(Integer userId,Integer belongUser,Integer startnum,Integer pagesize,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/msgboxQueryBygroupid"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_id", userId);
		jo.addProperty("belong_user", belongUser);
		jo.addProperty("startnum", startnum);
		jo.addProperty("pagesize", pagesize);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.QUERY_CHAT_MESSAGE, progress,
				listener);
	}
	public void requestAllbyContentid(Integer contentId,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/allbycontentid"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("content_id", contentId);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.CIRC_QUERY_CONTENTID, progress,
				listener);
	}
	public void requestDownloadMoon(String circleId,String cacheIds,Integer pageno,Integer pagesize,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/circleRefresh"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("circleId", circleId);
		jsonMap.put("cacheIds",cacheIds);
		jsonMap.put("pageno",pageno);
		jsonMap.put("pagesize",pagesize);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.DOWN_MOON, progress,
				listener);
	}

	public void requestResetPswVerfy(String user_phone, boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/resetPswVerfy"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_phone", user_phone);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.RESET_VERIFY_CODE,   progress,
				listener);
	}

	public void requestCommentsInsert(Integer user_id,Integer content_id,Integer comments_parentid,String comments_content,boolean progress,ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/commentsInsert"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_id",user_id);
		jsonMap.put("content_id",content_id);
		jsonMap.put("comments_parentid",comments_parentid);
		jsonMap.put("comments_content",comments_content);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.MOON_COMMENTS, progress,
				listener);
	}

	public void requestCommentsQueryByContentid(Integer content_id, boolean progress,ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/commentsQueryByContentid"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("content_id",content_id);

		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.PHO_SHARE, progress,
				listener);
	}

	public void requestShareDoMoon(Integer userid,String circleId,String title,String content,Float longitude,Float latitude, List<Map<String,Object>> attachs,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/photoshare"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_id",userid);
		jsonMap.put("title",title);
		jsonMap.put("circleId",circleId);
		jsonMap.put("content",content);
		jsonMap.put("longitude",longitude);
		jsonMap.put("latitude",latitude);
		jsonMap.put("attachs",attachs);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.PHO_SHARE, progress,
				listener);
	}

	public void requestUnionLogin(String device_id ,String openid,String user_sex,String  user_nikeName,String address,String figureurl_qq_1,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/unionLogin");
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("device_id",device_id);
		jsonMap.put("openid",openid);
		jsonMap.put("user_sex",Base64.encode(user_sex));
		jsonMap.put("user_nikename",Base64.encode(user_nikeName));
		jsonMap.put("user_adress",Base64.encode(address));
		jsonMap.put("user_headporttait",figureurl_qq_1);
		jsonMap.put("regapptype", 3);
		jsonMap.put("account_type", 1);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.USER_UNION_LOGIN, progress,
				listener);
	}

	public RequestTask requestUserUpdataCloud(Integer user_id,String appid, String cloud_channelId, String cloud_requestId,String cloud_userId, boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/userupdatecloud"+TOKEN_SERVER_HOST);
		Log.i("whb", " url  ==>  "+task.getUrl());

		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();		
		jsonMap.put("user_id", user_id);
		jsonMap.put("cloud_appid", appid);
		jsonMap.put("cloud_channelId", cloud_channelId);
		jsonMap.put("cloud_requestId", cloud_requestId);
		jsonMap.put("cloud_userId", cloud_userId);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		return task;
		//		Log.e("whb", "jo --->" +jo.toString());
		//		this.sendRequest(task, Command.UPLOAD_CLOUD_PARAMS, progress,
		//				listener);
	}

	public void requestMsgboxQueryByUserid(Integer user_id,Integer startnum,Integer pagesize,boolean progress,ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/msgboxQueryByUserid"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("belong_user", user_id);
		jo.addProperty("startnum", startnum);
		jo.addProperty("pagesize", pagesize);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.QUERY_MASSAGEBOX, progress,
				listener);
	}

	public void requestLogin(String name ,String password,String device_id ,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/login");
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_name",name);
		jsonMap.put("user_password",password);
		jsonMap.put("app_type", 3);
		jsonMap.put("device_id", device_id);

		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.USER_LOGIN, progress,
				listener);
	}

	//	"pageno": 0,
	//    "pagesize": 5,
	//    "circleId": "11,15",
	//    "cacheIds": ""

	public void requestCircleRefresh(String circleId,String cacheIds,Integer pageno, Integer pagesize,boolean progress,
			ICommuDataListener listener)
	{

	}


	public void requestUploadPhoto(boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/findDownUrlById"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_id", "108");
		jo.addProperty("album_id", "0");
		task.setBodyStream(jo.toString().getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.SHARE_MOON, progress,
				listener);
	}

	public void requestTransformCircle(Integer cmd,String circleId,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/msgCommand"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("cmd", cmd);
		jo.addProperty("tagname",circleId);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.UPLOAD_SUECESS, progress,
				listener);
	}

	public void requestVerify(String user_phone,String user_nikeName,String user_password,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/register");
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_phone", user_phone);
		jo.addProperty("user_nikeName", user_nikeName);
		jo.addProperty("user_password", Encrypt.md5(user_password));
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.VERIFY_CODE, progress,
				listener);
	}

	public void requestUpdatePassword(String user_name,String user_password,String new_password,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/updatePsw"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_name", user_name);
		jo.addProperty("user_password", Encrypt.md5(user_password));
		jo.addProperty("new_password", Encrypt.md5(new_password));
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.USER_UPDATEPSW, progress,
				listener);
	}

	public void requestResetPassword(String user_phone,String smscode,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/resetPsw");
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_phone", user_phone);
		jo.addProperty("smscode", smscode);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.USER_RESETPSW, progress,
				listener);
	}

	public void requestAddAlbum(Integer user_id,String album_name,String album_instructions,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/album_insert"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_id", user_id);
		jo.addProperty("album_name", album_name);
		jo.addProperty("album_instructions", album_instructions);
		jo.addProperty("path", "");
		jo.addProperty("album_size", "");
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.ALBL_CREA, progress,
				listener);
	} 

	public void requestPhotoQueryByAlbumId(Integer album_id,Integer startnum,Integer pagesize,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/photoQueryByAlbumId"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("album_id", album_id);
		jo.addProperty("attachment_state", "0");
		jo.addProperty("startnum", startnum);
		jo.addProperty("pagesize", pagesize);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.ALBL_QUERY_PHOTO, progress,
				listener);
	}

	public void requestQueryAlbum(Integer user_id,Integer startnum,Integer pagesize,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/album_query"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_id", user_id);
		jo.addProperty("startnum", startnum);
		jo.addProperty("pagesize", pagesize);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.ALBL_QUERY, progress,
				listener);
	} 

	public void requestAddCircle(Integer user_id,String circle_name,
			String circle_interduction, 
			Float circle_longitude,Float circle_latitude,String circle_type, boolean progress,ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/circle_add"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_id", user_id);
		jo.addProperty("circle_name", circle_name);
		jo.addProperty("circle_interduction", circle_interduction);

		//		jo.addProperty("circle_num", circle_num);
		//		jo.addProperty("circle_interduction", circle_interduction);

		jo.addProperty("circle_longitude", circle_longitude);
		jo.addProperty("circle_latitude", circle_latitude);

		jo.addProperty("attachment_path", "");
		jo.addProperty("circle_type", circle_type);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.CIRC_CREA, progress,
				listener);
	}

	public void requestMassageBox(Integer user_id,Integer startnum,Integer pagesize,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/msgboxQueryByUserid"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("user_id", user_id);
		jo.addProperty("startnum", startnum);
		jo.addProperty("pagesize", pagesize);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.QUERY_MASSAGE, progress,
				listener);
	}

	public void requestUserApplyCricleResponse(Integer message_id,Integer message_state,String circle_num,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/userApplyCricleResponse"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("message_id", message_id);
		jo.addProperty("message_state", message_state);
		jo.addProperty("circle_num",circle_num);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.CIRC_APPLY_RESPONSE, progress,
				listener);
	}
	public void requestQueryCircle(Integer user_id,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/circle_query"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_id",user_id);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.CIRC_QUERY, progress,
				listener);
	}

	public void requestInvitedToCircle(String circle_id,String user_phone,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/invitedToCircle"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("circle_id",circle_id);
		jsonMap.put("user_phone",user_phone);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.CIRC_INVITED, progress,
				listener);
	}

	public void requestuserApplyCricle(Integer user_id,String circle_num,String message_content,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/userApplyCricleRequest"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_id",user_id);
		jsonMap.put("circle_num",circle_num);
		jsonMap.put("message_content",message_content);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.CIRC_APPLY, progress,
				listener);
	}

	public void requestCircleQueryUserBycircleid(String circle_ids,Integer startnum,Integer pagesize,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/circleQueryUserBycircleid"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("circle_ids", circle_ids);
		jo.addProperty("startnum", startnum);
		jo.addProperty("pagesize", pagesize);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.CIRC_QUERY_PEOPLE, progress,
				listener);
	}

	public void requestUploadPhotoforAlbum(Integer user_id,List<Map<String,Object>> attachs, boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/photoAddIntoAlbum"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_id",user_id);
		jsonMap.put("attachs",attachs);
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		task.setBodyStream(jo.getBytes());  
		Log.e("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.PHO_ALBUM_UPLOAD, progress,
				listener);
	}

	public void requestUpdataVesion(Integer verno,boolean progress,
			ICommuDataListener listener)
	{
		RequestTask task = new RequestTask();
		task.setUrl(SERVER_HOST+"/appCheckVersion"+TOKEN_SERVER_HOST);
		task.setTransport(ConnectionManager.Transport_Stream);
		JsonObject jo = new JsonObject();
		jo.addProperty("versioncode", verno);
		jo.addProperty("type",3);
		task.setBodyStream(jo.toString().getBytes());  
		Log.i("whb", "jo --->" +jo.toString());
		this.sendRequest(task, Command.VESION_CHECK, progress,
				listener);
	}


	/**
	 * 
	 * @param task
	 */
	public void reloadRequest(RequestTask task) {
		super.addTask(task);
	}

	@Override
	public void sendRequest(RequestTask task, int protocolCode,
			boolean progress, ICommuDataListener listener) {
		task.setResponseParser(parse);
		super.sendRequest(task, protocolCode, progress, listener);
	}

}
