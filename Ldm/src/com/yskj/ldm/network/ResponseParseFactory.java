package com.yskj.ldm.network;

import java.io.InputStream;

import com.android.commu.net.RequestTask;
import com.android.commu.parse.IResponseParser;
import com.android.commu.parse.Response;
import com.yskj.ldm.response.AllResponse;
 
 
/**
 * @description: 请求响应解析器
 * @author: wanghaibo
 * @create: 2013-4-3
 */

public class ResponseParseFactory implements IResponseParser {

	
	public Response parseResponse(InputStream input, RequestTask resource) {

		int cmdId = resource.getCommandId();
		AllResponse resp = new AllResponse();
		resp.setCommandId(cmdId);
		resp.parseBody(resource,input);
	    return resp;
	}

}

