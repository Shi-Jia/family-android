package com.yskj.ldm.network;

 

public class Command {
	public static final String  RESULT_OK = "0";
	/**用户获取短信码**/
	public static final Integer USER_MSGCODE=1000;
	/**用户注册**/
	public static final Integer USER_REGISTER=1001;

	/**用户资料修改*/	
	public static final Integer USER_EDIT=1003;	
	/**创建用户（绑定相框或加人到相框）*/	
	public static final Integer USER_CREATE=1004;	
	/**创建用户（绑定相框或加人到相框）*/	
	public static final Integer USER_UPDATEPSW=1005;	
	/**创建用户（绑定相框或加人到相框）*/	
	public static final Integer USER_RESETPSW=1006;	
	public static final Integer USER_LOGIN=1007;
	/**用户联合登录**/ 
	public static final Integer USER_UNION_LOGIN=1008;
	/**照片分享*/	
	public static final int PHO_SHARE=2001;	
	/**照片删除*/	
	public static final int PHO_DEL=2002;	
	/**照片上传不分享*/	
	public static final int PHO_UPLOAD=2003;	
	
	/**照片上传不分享*/	
	public static final int PHO_ALBUM_UPLOAD=2004;	
	/**新建相册*/	
	public static final Integer ALBL_CREA=2009;
	/**删除相册*/	
	public static final Integer ALBL_EDIT=2005;
	/**查询用户相册*/	
	public static final Integer ALBL_QUERY=2006;
	/**查询用户相册相片*/	
	public static final Integer ALBL_QUERY_PHOTO=2007;
	
	public static final Integer SHARE_MOON=2008;

	/**创建圈子*/	
	public static final Integer CIRC_CREA=3001;
 
	public static final Integer CIRC_EDIT=3002;
	/**圈子查询*/	
	public static final Integer CIRC_QUERY=3003;
	/**邀请加入*/	
	public static final Integer CIRC_INVITED=3005;
	/**申请加入*/	
	public static final Integer CIRC_APPLY=3006;
	/**申请返回*/	
	public static final Integer CIRC_APPLY_RESPONSE=3007;

	/**圈子查询*/	
	public static final Integer CIRC_QUERY_PEOPLE=3008;
	
	/**圈子查询*/	
	public static final Integer CIRC_QUERY_CONTENTID=3009;

	
	public static final Integer VERIFY_CODE=4001;
	
	public static final Integer VESION_CHECK=4002;
	
	public static final Integer RESET_VERIFY_CODE=4003;
	
	
	public static final Integer QUERY_MASSAGE=5001;
	
	public static final Integer UPLOAD_CLOUD_PARAMS=5002;
	
	public static final Integer QUERY_MASSAGEBOX=5003;
	
	public static final Integer QUERY_CHAT_MESSAGE=5004;
	public static final Integer SEND_CHAT_MESSAGE=5005;
	
	public static final Integer DOWN_MOON=6001;
 
	public static final Integer UPLOAD_SUECESS=6002;
	
	public static final Integer MOON_COMMENTS=6003;




	public static final String Message_User_Validation  = "1";

	public static final String Message_System = "2";

	public static final String Message_Photoshare = "3";
	
	public static final String Message_Moon_Comments = "4";
	
	
	public static final String Message_Chat = "5";
	
	// 1：小包2：中包 3:大包
	public static final int Room_Type_Small = 1;

	public static final int Room_Type_Medium = 2;

	public static final int Room_Type_Large = 3;

	public static final int Room_Type_CardSite = 4;

	public static final int Room_Type_Platform = 5;

	public static String getRoomBigStr(int bigType){

		switch(bigType){

		case Room_Type_Small:
			return "小包";
		case Room_Type_Medium:
			return "中包";
		case Room_Type_Large:
			return "大包";
		case Room_Type_CardSite:
			return "卡座";
		case Room_Type_Platform:
			return "散台";

		}
		return "未知";
	}

}
