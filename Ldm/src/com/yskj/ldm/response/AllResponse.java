package com.yskj.ldm.response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.util.Log;

import com.android.commu.net.RequestTask;
import com.android.commu.parse.Response;

public class AllResponse extends Response {

	@Override
	public void parseBody(RequestTask task, InputStream in) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			StringBuilder sb = new StringBuilder();
			Object line;
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
			}
//			Log.e("2123", sb.toString() +"  <---string");
			this.setMsg(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
