package com.yskj.ldm.global;

 

import java.util.ArrayList;
import java.util.List;

import com.baidu.frontia.FrontiaApplication;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.widget.Toast;

 

public class LaoDaoMiApplication extends FrontiaApplication {
	
	private List<Activity> list = new ArrayList<Activity>();
	public boolean m_bKeyRight = true;
	
//	public BMapManager mBMapManager = null;

//	public static final String strKey = "Eazr3crh5YWWmyLNA5CgO4b4";
	
//	protected LocationClient mLocationClient;
	
	/**
	 * 传输对象，用于两个Activity中数据的传输
	 */
	public Object mTransferObject = new Object();
	
	/**
	 * user信息
	 */
//	public UserElement userEl;
	

	@Override
	public void onCreate() {
		// 程序入口
		super.onCreate();
//		mInstance = this;
//		initEngineManager(this);
		this.initImageConfig();
	}
	
	public void addActivity(Activity activity) {
		if (list == null) {
			list = new ArrayList<Activity>();
		}
		list.add(activity);
	}

	public void finishAll() {
		for (Activity activity : list) {
			if (!activity.isFinishing()) {
				activity.finish();
			}
		}
		list = null;
	}

	/**
	 * 获取集合大小
	 * 
	 * @author haibo.wang
	 * @return
	 */
	public int getListSize() {
		if (list == null) {
			return 0;
		}
		return list.size();
	}

	/**
	 * 移除指定activity
	 * 
	 * @author haibo.wang
	 * @param activity
	 */
	public void removeActivity(Activity activity) {
		if (list != null) {
			list.remove(activity);
		}
	}

	/**
	 * 判断是否有此activity
	 * 
	 * @param activity
	 * @author haibo.wang
	 * @return
	 */
	public boolean isHasActivity(Activity activity) {
		if (list != null) {
			return list.contains(activity);
		}
		return false;
	}

	/**
	 * 结束指定的同类activity
	 * 
	 * @param activity
	 * @author haibo.wang
	 */
	public void finishActivityByName(Activity activity) {
		String className = activity.getClass().getName();
		List<Activity> removeAcList = new ArrayList<Activity>();
		if (list != null) {
			for (Activity ac : list) {
				if (className.equals(ac.getClass().getName())) {
					ac.finish();
					removeAcList.add(ac);
				}
			}
			list.removeAll(removeAcList);
		}

	}
   public void initImageConfig()
   {
	   ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)  
	    .memoryCacheExtraOptions(480, 800)          // default = device screen dimensions  
	    .discCacheExtraOptions(480, 800, CompressFormat.JPEG, 75, null)  
 
	    .threadPoolSize(3)                          // default  
	    .threadPriority(Thread.NORM_PRIORITY - 1)   // default  
	    .tasksProcessingOrder(QueueProcessingType.FIFO) // default  
	    .denyCacheImageMultipleSizesInMemory()  
	    .memoryCache(new LruMemoryCache(2 * 1024 * 1024))  
	    .memoryCacheSize(2 * 1024 * 1024)  
	    .memoryCacheSizePercentage(13)              // default  
//	    .discCache(new UnlimitedDiscCache(cacheDir))// default  
	    .discCacheSize(50 * 1024 * 1024)        // 缓冲大小  
	    .discCacheFileCount(100)                // 缓冲文件数目  
	    .discCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default  
	    .imageDownloader(new BaseImageDownloader(this.getApplicationContext())) // default  
	    .imageDecoder(new BaseImageDecoder(true)) // default  
	    .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default  
	    .writeDebugLogs()  
	    .build();  
	   ImageLoader.getInstance().init(config);
   }
//	public void initEngineManager(Context context) {
//		
//        if (mBMapManager == null) {
//            mBMapManager = new BMapManager(context);
//        }
//
//
//		if (!mBMapManager.init(strKey, new MyGeneralListener())) {
//			Toast.makeText(
//					WekApplication.getInstance().getApplicationContext(),
//					"BMapManager  初始化错误!", Toast.LENGTH_LONG).show();
//		}
//		
//		goToMyLocation();
//	}

//	public static WekApplication getInstance() {
//		return mInstance;
//	}

//	// 常用事件监听，用来处理通常的网络错误，授权验证错误等
//	public static class MyGeneralListener implements MKGeneralListener {
//
//		@Override
//		public void onGetNetworkState(int iError) {
//			if (iError == MKEvent.ERROR_NETWORK_CONNECT) {
//				/*Toast.makeText(
//						WekApplication.getInstance().getApplicationContext(),
//						"您的网络出错啦！", Toast.LENGTH_LONG).show();*/
//			} else if (iError == MKEvent.ERROR_NETWORK_DATA) {
//				/*Toast.makeText(
//						WekApplication.getInstance().getApplicationContext(),
//						"输入正确的检索条件！", Toast.LENGTH_LONG).show();*/
//			}
//		}
//
//		@Override
//		public void onGetPermissionState(int iError) {
//			// 非零值表示key验证未通过
//			if (iError != 0) {
//				// 授权Key错误：
//				/*Toast.makeText(
//						WekApplication.getInstance().getApplicationContext(),
//						"请在 WekApplication.java文件输入正确的授权Key,并检查您的网络连接是否正常！error: "
//								+ iError, Toast.LENGTH_LONG).show();*/
//				WekApplication.getInstance().m_bKeyRight = false;
//			} else {
//				WekApplication.getInstance().m_bKeyRight = true;
////				Toast.makeText(
////						WekApplication.getInstance().getApplicationContext(),
////						"key认证成功", Toast.LENGTH_LONG).show();
//			}
//		}
//	}

	
	
	
	/******************************************************************************************************/
	
//	
//	public  void goToMyLocation(){
//		
//		if (mLocationClient != null && mLocationClient.isStarted()){
//			mLocationClient.requestLocation();
//		}else{ 
//		    //showToast("locClient is null or not started");
//			startLocation();
//		}
//	}

//	private void startLocation(){
//		
//		mLocationClient = new LocationClient(this);
//		mLocationClient.registerLocationListener(new MyLocationListenner());
//		setLocationOption(mLocationClient);
//		
//		mLocationClient.start();
//	}
	
	
//	//设置相关参数
//	private void setLocationOption(LocationClient mLocClient){
//		
//		LocationClientOption option = new LocationClientOption();
//		//option.setOpenGps(true);
//		option.setAddrType("all");//返回的定位结果包含地址信息
//		option.setCoorType("bd09ll");//返回的定位结果是百度经纬度,默认值gcj02
//		//option.setScanSpan(10000);//设置发起定位请求的间隔时间为5000ms
//		option.disableCache(true);//禁止启用缓存定位
//		option.setPoiNumber(5);	//最多返回POI个数	
//		option.setPoiDistance(1000); //poi查询距离		
//		option.setPoiExtraInfo(true); //是否需要POI的电话和地址等详细信息		
//		mLocClient.setLocOption(option);
//	}
	
//	public class MyLocationListenner implements BDLocationListener {
//		
//		@Override
//		public void onReceiveLocation(BDLocation location) {
//			
//			double lat =location.getLatitude();
//			double lon = location.getLongitude();
//			
//			GlobalUtil.lat = lat;
//			GlobalUtil.lon = lon;
//			
//			//GlobalUtil.curPoint = new GeoPoint((int)(lat* 1E6),(int)(lon* 1E6));
//			
//
//		}
//
//		@Override
//		public void onReceivePoi(BDLocation arg0) {
//			// TODO Auto-generated method stub
//		}
//	}

		
}
