package com.yskj.ldm.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.android.commu.net.HandlerException;
import com.android.commu.net.RequestTask;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.util.Encrypt;
import com.yskj.ldm.util.JsonUtil;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class TimerServices extends Service{

	Timer timer;
	PreferencesService ps = new PreferencesService(this);
	@Override
	public IBinder onBind(Intent arg0) {

		return null;
	}
	@Override  
	public void onCreate() {  

		timer = new Timer();
		timer.schedule(new TimerTask(){
			@Override
			public void run() {

				long endtime = Long.parseLong(ps.getTokenPerferences().get("expiretime").toString());

				if(endtime - System.currentTimeMillis()<10000)
				{
					UpdataToken();
				}
			}}, 5000,5000);
	}  

	public void UpdataToken()
	{
		HashMap<String,Object>  jsonMap = new HashMap<String,Object>();
		jsonMap.put("user_id", Integer.parseInt(ps.getUser_id()));
		Gson gs = new Gson();
		String jo = gs.toJson(jsonMap);
		String tokenJson  = null;
		try {
			tokenJson = this.urlConnection(ConnectionManager.SERVER_HOST, jo.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		JsonObject getjo = JsonUtil.parseJson(tokenJson);
		String errorNo = getjo.get("errorNo").toString();
		String errorInfo = getjo.get("errorInfo").toString();
		if(errorNo.equals(Command.RESULT_OK))
		{
			String reList = getjo.get("resList").toString();
			List<HashMap<String, Object>> list = JsonUtil.getList(reList);
			HashMap<String, Object> pramasMap = list.get(0);
			ps.savaToken(pramasMap.get("token").toString(), pramasMap.get("expiretime").toString());
		}
		else
		{
			Toast.makeText(this,errorInfo,
					Toast.LENGTH_SHORT).show();
			return ;
		}
	}

	@Override
	public void onDestroy() {
		timer.cancel();
		super.onDestroy();
	}
	private String  urlConnection(String address,byte [] params) throws Exception
	{
		HttpURLConnection conn;
		InputStream inputStream;
		OutputStream outputStream;
		conn = null;
		inputStream = null;
		outputStream = null;
		try
		{
			URL url = new URL(address);
			conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setConnectTimeout(5000);
			byte data[] = params;
			conn.setRequestProperty("Content-Type", "application/json");
			outputStream = conn.getOutputStream();
			if(data != null)
			{
				outputStream.write(data);
				outputStream.flush();
			}
			int statusCode = conn.getResponseCode();
			System.out.println(statusCode +"  <<---statusCode");
			if(statusCode == 200)
			{
				inputStream = conn.getInputStream();
				try {
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
					StringBuilder sb = new StringBuilder();
					Object line;
					while ((line = bufferedReader.readLine()) != null) {
						sb.append(line);
					}
					Log.i("2123", sb.toString() +"  <---string");
					return sb.toString();
					//					this.setMsg(sb.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else
			{
				throw new  Exception();
			}
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			if(null != inputStream   )
				inputStream.close();
			if(null!=conn)
				conn.disconnect();
			if(null!=outputStream)
				outputStream.close();
		}
		return null;
	}

}
