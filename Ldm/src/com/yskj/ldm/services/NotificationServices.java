package com.yskj.ldm.services;



import com.yskj.ldm.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


/**
 * @description 通知栏消息发送
 * @author haibo.wang
 * @date 2012-2-25
 */
public class NotificationServices {

	private Context mContext; 
	private Notification mNotification;    
	private NotificationManager mNotificationManager;    
	private int notification_id = 1;  //唯一标识ID
	private String title = ""; //通知标题
	private String content = ""; //通知内容 
	private String message_type = ""; //通知内容 
	private Class showclass = null;
	private Bundle bundle = null ; //参数 

	/**
	 * 通知服务构造函数
	 * @param context 
	 * @param showclassp 跳转的Acitvity
	 * @param notificationid 通知ID(唯一标示)
	 * @param contentStr 通知内容
	 * @param Bundle b 复杂的参数传递
	 */
	public NotificationServices(Context context,Class showclassp,int notificationid,String titleStr,String contentStr,String message_type,Bundle b){
		mContext = context;
		title = titleStr;
		content = contentStr;
		bundle = b;
		showclass = showclassp;
		notification_id = notificationid;
		this.message_type = message_type;
	}

	/**
	 * 构造函数，此时没有传入点击通知后的跳转意图对象，所以只适合只有提醒的通知
	 * @param context
	 * @param notificationid
	 * @param contentStr
	 */
	public NotificationServices(Context context,int notificationid,String titleStr,String contentStr){
		mContext = context;
		title = titleStr;
		content = contentStr;
		notification_id = notificationid;
	}

	/**
	 * 发送通知
	 * boolean is_soundNotice 是否铃声提醒
	 */
	public void sendNotify(boolean is_soundNotice){
		//通知管理器
		mNotificationManager = (NotificationManager)mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
		//新增一个通知实体
		mNotification = new Notification(R.drawable.head_photo,title,System.currentTimeMillis());    
		//将使用默认的声音来提醒用户    
		if(is_soundNotice)mNotification.defaults = Notification.DEFAULT_SOUND;

		mNotification.flags =Notification.FLAG_AUTO_CANCEL;
		//点击通知的意图 
		Intent mIntent = new Intent(); 
		mIntent.putExtras(bundle);
		//这里需要设置Intent.FLAG_ACTIVITY_NEW_TASK属性    
		if(bundle.getString("isDocumentNotice")!=null && "Y".equals(bundle.getString("isDocumentNotice"))){
			//是业务数据提醒不要加此设置
		}else{
			//			if(message_type.equals("1"))
			//			{ 
			mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP); 
			//			}
			//			else if(message_type.equals("3"))
			//			{
			//				
			//			}
		}
		//设置要跳转到的Activity   
		if(showclass!=null)mIntent.setClass(mContext,showclass);
		PendingIntent mContentIntent =PendingIntent.getActivity(mContext,notification_id, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);    
		//这里必需要用setLatestEventInfo(上下文,标题,内容,PendingIntent)不然会报错.    
		if(title==null){
			title = "";
		}
		if(content==null){
			content = "";
		}
		mNotification.setLatestEventInfo(mContext, title, content , mContentIntent);    
		//这里发送通知(消息ID,通知对象)    
		mNotificationManager.notify(notification_id, mNotification);     
	}


}
