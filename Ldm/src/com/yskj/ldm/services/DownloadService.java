package com.yskj.ldm.services;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.widget.RemoteViews;
import android.widget.Toast;

 
 


 




import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.RecordsPath;
import com.yskj.ldm.activity.ui.DownloadProgressActivity;
import com.yskj.ldm.util.SDCardStoreManager;

/**
 * @description:
 * @author: wanghaibo
 * @create: 2012-11-10
 */

public class DownloadService extends Service {

	private NotificationManager nm;
	private Notification notification;
	private File tempFile = null;

	private boolean cancelUpdate = false;
	private MyHandler myHandler;
	public static int download_precent = 0;
	private int notificationId = 1234;

	private static boolean isloading = false;

	private RemoteViews views;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notification = new Notification();
		notification.icon = android.R.drawable.stat_sys_download;
		notification.tickerText = "唠叨觅" + "正在下载";
		notification.when = System.currentTimeMillis();
		notification.defaults = Notification.DEFAULT_LIGHTS;

		// 设置任务栏中下载进程显示的views
		views = new RemoteViews(getPackageName(), R.layout.download_status);
		clear();
		notification.contentView = views;

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, DownloadProgressActivity.class), 0);
		notification.setLatestEventInfo(this, "", "", contentIntent);

		// 将下载任务添加到任务栏中
		nm.notify(notificationId, notification);

		myHandler = new MyHandler(Looper.myLooper(), this);

		// 初始化下载任务内容views
		Message message = myHandler.obtainMessage(3, 0);
		myHandler.sendMessage(message);

		// 启动线程开始执行下载任务
		downFile(intent.getStringExtra("url"),intent.getStringExtra("version"));

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	// 下载更新文件
	private void downFile(final String urladdress,final String version) {

		if (isloading) {
			return;
		}

		System.out.println("downloas url:" + urladdress);
		new Thread() {
			public void run() {
				try {

					isloading = true;

					HttpClient client = new DefaultHttpClient();
					// params[0]代表连接的url
					HttpGet get = new HttpGet(urladdress);
					HttpResponse response = client.execute(get);
					HttpEntity entity = response.getEntity();

					long length = entity.getContentLength();
					InputStream is = entity.getContent();
//					URL url=new URL(urladdress);
 
				
//					URLConnection connection =url.openConnection();
//					 if (connection.getReadTimeout()==5) {
//						Log.i("---------->", "当前网络有问题");
//						 return;
//					   }
//			         long length = connection.getContentLength();
//					 InputStream is=connection.getInputStream();

					if (is != null) {

						SDCardStoreManager sdcardManager = SDCardStoreManager
								.getInstance();

						String path = RecordsPath.DownLoadFile
								+ "laodaomi"+version+".apk";
						if (sdcardManager.checkParentExsit(path, true)) {

							tempFile = sdcardManager.createNewFile(path);
							if (tempFile == null) {
								return;
							}
						} else {
							return;
						}

						// 已读出流作为参数创建一个带有缓冲的输出流
						BufferedInputStream bis = new BufferedInputStream(is);

						// 创建一个新的写入流，讲读取到的图像数据写入到文件中
						FileOutputStream fos = new FileOutputStream(tempFile);

						// 已写入流作为参数创建一个带有缓冲的写入流
						BufferedOutputStream bos = new BufferedOutputStream(fos);

						int read;
						long count = 0;
						int precent = 0;
						byte[] buffer = new byte[1024];

						while ((read = bis.read(buffer)) != -1 && !cancelUpdate) {

							bos.write(buffer, 0, read);
							count += read;
							precent = (int) (((double) count / length) * 100);

							// 每下载完成5%就通知任务栏进行修改下载进度
							if (precent - download_precent >= 5) {
								download_precent = precent;
								Message message = myHandler.obtainMessage(3,
										precent);
								myHandler.sendMessage(message);
							}
						}
						bos.flush();
						bos.close();
						fos.flush();
						fos.close();
						is.close();
						bis.close();
					}

					if (!cancelUpdate) {
						Message message = myHandler.obtainMessage(2, tempFile);
						myHandler.sendMessage(message);
					} else {
						tempFile.delete();
					}

				} catch (ClientProtocolException e) {
					e.printStackTrace();
					Message message = myHandler.obtainMessage(4, "下载更新文件失败");
					myHandler.sendMessage(message);
				} catch (IOException e) {
					e.printStackTrace();
					Message message = myHandler.obtainMessage(4, "下载更新文件失败");
					myHandler.sendMessage(message);
				} catch (Exception e) {
					e.printStackTrace();
					Message message = myHandler.obtainMessage(4, "下载更新文件失败");
					myHandler.sendMessage(message);
				} finally {
					isloading = false;
				}
			}
		}.start();
	}

	// 安装下载后的apk文件
	private void instanll(File file, Context context) {

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),
				"application/vnd.android.package-archive");
		context.startActivity(intent);
	}

	/* 事件处理类 */
	class MyHandler extends Handler {

		private Context context;

		public MyHandler(Looper looper, Context c) {
			super(looper);
			this.context = c;
		}

		@Override
		public void handleMessage(Message msg) {

			super.handleMessage(msg);

			if (msg != null) {

				switch (msg.what) {

				case 0:
					Toast.makeText(context, msg.obj.toString(),
							Toast.LENGTH_SHORT).show();
					break;
				case 1:
					break;
				case 2:
					// 下载完成后清除所有下载信息，执行安装提示
					download_precent = 100;
					nm.cancel(notificationId);
					instanll((File) msg.obj, context);
					// 停止掉当前的服务
					stopSelf();
					break;
				case 3:

					// 更新状态栏上的下载进度信息
					views.setTextViewText(R.id.tvProcess, "已下载"
							+ download_precent + "%");
					views.setProgressBar(R.id.pbDownload, 100,
							download_precent, false);
					notification.contentView = views;
					nm.notify(notificationId, notification);

					break;
				case 4:
					nm.cancel(notificationId);
					break;
				}
			}
		}
	}

	private void clear() {

		download_precent = 0;
		views.setTextViewText(R.id.tvProcess, "已下载 0%");
		views.setProgressBar(R.id.pbDownload, 100, 0, false);
	}

}
