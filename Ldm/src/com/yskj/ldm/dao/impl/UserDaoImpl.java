package com.yskj.ldm.dao.impl;

import java.util.ArrayList;
import java.util.List;



import com.yskj.ldm.beans.UserBean;
import com.yskj.ldm.dao.UserDao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UserDaoImpl implements UserDao{

	public void addUser(SQLiteDatabase db,UserBean bean,String mobileNo) throws Exception
	{
		db.execSQL(String
				.format("insert into USER_BASE (USER_ID,CATEGORY_ID,AREA_ID,REG_DATE,USER_NAME,"
						+ "USER_PASSWORLD,USER_SEX,USER_NIKENAME,"
						+ "USER_PHONE,USER_EMAIL,USER_QQ,USER_BIRTH,USER_GROUP,"
						+ "USER_ADRESS,USER_STATE,USER_HEADPORTTAIT,CLOUD_USERID,"
						+ "CLOUD_CHANNELID ,CLOUD_DEVICE,DEVICE_ID  )"
						+ " values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
						bean.getUSER_ID(), bean.getCATEGORY_ID(), bean.getAREA_ID(), bean.getREG_DATE(), bean.getUSER_NAME(),
						bean.getUSER_PASSWORLD(), bean.getUSER_SEX(), bean.getUSER_NIKENAME(), bean.getUSER_PHONE(),
						bean.getUSER_EMAIL(), bean.getUSER_QQ(),bean.getUSER_BIRTH(),
						bean.getUSER_GROUP(),bean.getUSER_ADRESS(),bean.getUSER_STATE(),bean.getUSER_HEADPORTTAIT(),
						bean.getCLOUD_CHANNELID(),bean.getCLOUD_DEVICE(),bean.getCLOUD_DEVICE(),bean.getDEVICE_ID()));
	}
    
	public boolean selectUserForMobile(SQLiteDatabase db,String mobileNo) throws Exception
	{
		StringBuffer sql = new StringBuffer();
		sql.append("select USER_ID from USER_BASE");
		Cursor cursor = db.rawQuery(sql.toString(), new String[] {});
		if(cursor.getCount()>0)
		{
			if (cursor != null && !cursor.isClosed())
			{
				cursor.close();
			}
			return true;
		}

		return false;
	}

	public UserBean selectUserForPassword(SQLiteDatabase db,String mobileNo) throws Exception
	{
		StringBuffer sql = new StringBuffer();
	
		sql.append("select USER_PHONE,USER_PASSWORLD from USER_BASE"+mobileNo);
		Cursor cursor = db.rawQuery(sql.toString(), new String[] {});
		UserBean bean = new UserBean();
		if(cursor.getCount()>0)
		{
			while (cursor.moveToNext())
			{
				bean.setUSER_PHONE(cursor.getString(cursor.getColumnIndex("USER_PHONE")));
				bean.setUSER_PASSWORLD(cursor.getString(cursor.getColumnIndex("USER_PASSWORLD")));
			}
		}
		if (cursor != null && !cursor.isClosed())
		{
			cursor.close();
		}
		return bean;
	}
	public List<UserBean> selectUser(SQLiteDatabase db,String mobileNo) throws Exception
	{
		List<UserBean> beanList = new ArrayList<UserBean>();
		StringBuffer sql = new StringBuffer();
		sql.append("select  USER_ID,CATEGORY_ID,AREA_ID,REG_DATE,USER_NAME,"
				+ "USER_PASSWORLD,USER_SEX,USER_NIKENAME,"
				+ "USER_PHONE,USER_EMAIL,USER_QQ,USER_BIRTH,USER_GROUP,"
				+ "USER_ADRESS,USER_STATE,USER_HEADPORTTAIT,CLOUD_USERID,"
				+ "CLOUD_CHANNELID ,CLOUD_DEVICE,DEVICE_ID from USER_BASE where USER_PHONE =  '" + mobileNo + "' ");
		Cursor cursor = db.rawQuery(sql.toString(), new String[] {});
		UserBean bean = new UserBean();
		while (cursor.moveToNext())
		{
			bean.setUSER_ID(cursor.getString(cursor.getColumnIndex("USER_ID")));
			bean.setCATEGORY_ID(cursor.getString(cursor.getColumnIndex("CATEGORY_ID")));
			bean.setAREA_ID(cursor.getString(cursor.getColumnIndex("AREA_ID")));
			bean.setREG_DATE(cursor.getString(cursor.getColumnIndex("REG_DATE")));
			bean.setUSER_NAME(cursor.getString(cursor.getColumnIndex("USER_NAME")));
			bean.setUSER_PASSWORLD(cursor.getString(cursor.getColumnIndex("USER_PASSWORLD")));

			bean.setUSER_SEX(cursor.getString(cursor.getColumnIndex("USER_SEX")));
			bean.setUSER_NIKENAME(cursor.getString(cursor.getColumnIndex("USER_NIKENAME")));
			bean.setUSER_PHONE(cursor.getString(cursor.getColumnIndex("USER_PHONE")));
			bean.setUSER_EMAIL(cursor.getString(cursor.getColumnIndex("USER_EMAIL")));
			bean.setUSER_QQ(cursor.getString(cursor.getColumnIndex("USER_QQ")));
			bean.setUSER_BIRTH(cursor.getString(cursor.getColumnIndex("USER_BIRTH")));

			bean.setUSER_GROUP(cursor.getString(cursor.getColumnIndex("USER_GROUP")));
			bean.setUSER_ADRESS(cursor.getString(cursor.getColumnIndex("USER_ADRESS")));
			bean.setUSER_STATE(cursor.getString(cursor.getColumnIndex("USER_STATE")));
			bean.setUSER_HEADPORTTAIT(cursor.getString(cursor.getColumnIndex("USER_HEADPORTTAIT")));
			bean.setCLOUD_USERID(cursor.getString(cursor.getColumnIndex("CLOUD_USERID")));
			bean.setCLOUD_CHANNELID(cursor.getString(cursor.getColumnIndex("CLOUD_CHANNELID")));

			bean.setCLOUD_DEVICE(cursor.getString(cursor.getColumnIndex("CLOUD_DEVICE")));
			bean.setDEVICE_ID(cursor.getString(cursor.getColumnIndex("DEVICE_ID")));
			bean.setUSER_PHONE(mobileNo);
			beanList.add(bean);
		}
		if (cursor != null && !cursor.isClosed())
		{
			cursor.close();
		}
		return beanList;
	}
	public void updateUser(SQLiteDatabase db,UserBean bean,String mobileNo) throws Exception
	{
		String sql = "update USER_BASE  set CATEGORY_ID ='" + bean.getCATEGORY_ID() + "',AREA_ID='" + bean.getAREA_ID() + "',REG_DATE='" + bean
				.getREG_DATE() + "',USER_NAME='" + bean.getUSER_NAME() + "' ,USER_PASSWORLD='" + bean.getUSER_PASSWORLD() + "' ,USER_SEX='" + bean
				.getUSER_SEX() + "',USER_NIKENAME ='" + bean.getUSER_NIKENAME() + "',USER_PHONE='" + bean.getUSER_PHONE() + "' ,USER_EMAIL='" + bean
				.getUSER_EMAIL() + "' ,USER_QQ ='" + bean.getUSER_QQ() + "',USER_BIRTH='" + bean
				.getUSER_BIRTH() + "',USER_GROUP='" + bean.getUSER_GROUP() + "' ,USER_ADRESS ='" + bean.getUSER_ADRESS() + "',USER_STATE='" + bean
				.getUSER_STATE() + "' ,USER_HEADPORTTAIT='" + bean.getUSER_HEADPORTTAIT() + "',CLOUD_USERID='" + bean.getCLOUD_USERID()
				+ "' ,CLOUD_CHANNELID='" + bean.getCLOUD_CHANNELID() + "',CLOUD_DEVICE='" + bean.getCLOUD_DEVICE() + "',DEVICE_ID='" + bean.getDEVICE_ID()
				+ "' where USER_ID = '" + bean.getUSER_ID() + "'";
		db.execSQL(sql);
	}	

	public void deleteUser(SQLiteDatabase db,String mobileNo) throws Exception
	{
		String sql = "delete from USER_BASE  where USER_PHONE = '" + mobileNo + "' ";
		db.execSQL(sql);
	}
}
