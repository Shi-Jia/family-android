package com.yskj.ldm.dao;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;

import com.yskj.ldm.beans.UserBean;

public interface UserDao {
     
	public void addUser(SQLiteDatabase db,UserBean bean,String mobileNo) throws Exception;
	public boolean selectUserForMobile(SQLiteDatabase db,String mobileNo) throws Exception;
	public UserBean selectUserForPassword(SQLiteDatabase db,String mobileNo) throws Exception;
	public List<UserBean> selectUser(SQLiteDatabase db,String mobileNo) throws Exception;
	public void updateUser(SQLiteDatabase db,UserBean bean,String mobileNo) throws Exception;
	public void deleteUser(SQLiteDatabase db,String mobileNo) throws Exception;
}
