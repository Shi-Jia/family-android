package com.yskj.ldm.base;



import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
 
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.commu.net.HandlerException;
import com.yskj.ldm.R;
 
 


public class NavigationActivity extends BaseActivity{

	private TextView btnBack;
	private TextView btnRight;
	private RelativeLayout blackRelativeLayout;
	private TextView mTitle;

	protected View loadingPanel;
	protected View loadBar;
	protected View errorBar;
	protected View  circledialogItemView;
	protected TextView errorMsg;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	protected RelativeLayout getBlackRelativeLayout() {
		return blackRelativeLayout;
	}

	/**
	 * 注册头部
	 */
	protected void registerHeadComponent(){
	    circledialogItemView = this.findViewById(R.id.topdialog_layout);
		btnBack = (TextView)findViewById(R.id.head_btn_back);
		btnBack.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				btnBack.setTextColor(Color.GRAY);
				onBackAction();
			}
		});

		btnRight = (TextView)findViewById(R.id.head_btn_right);
		btnRight.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				btnRight.setTextColor(Color.GRAY);
				onRightAction();
			}
		});

		mTitle = (TextView )findViewById(R.id.head_tv_title);
		blackRelativeLayout = (RelativeLayout )findViewById(R.id.head_bar);
		registerLoadingPanel();

	}


	protected void registerLoadingPanel(){

		loadingPanel = findViewById(R.id.loading_layout);
	
		if(loadingPanel != null){

			loadBar = findViewById(R.id.loading_panel);
			loadBar.setVisibility(View.GONE);

			errorBar = findViewById(R.id.error_panel);
			errorBar.setVisibility(View.GONE);

			errorMsg = (TextView)findViewById(R.id.error_msg);
			ImageButton btn =(ImageButton)findViewById(R.id.error_del);
			btn.setOnClickListener(new OnClickListener(){
				public void onClick(View v){
					closerErrorBar();
				}
			});
		}
	}



	/**
	 * 回退按钮事件
	 */
	protected void onBackAction(){
		finish();
	}

	/**
	 * 头部右边按钮监听
	 */
	protected void onRightAction(){

	}

	/**
	 * 设置头部
	 * @param text
	 */
	protected  void setHeadTitle(String text){
		this.mTitle.setText(text);
	}


	protected void setErrorMsg(String msg){

		if(loadingPanel != null){
			loadingPanel.setVisibility(View.VISIBLE);

			if(errorBar != null){

				errorBar.setAnimation(AnimationUtils.loadAnimation(this, R.anim.sacle_up_in));
				errorBar.setVisibility(View.VISIBLE);
				this.errorMsg.setText(msg);
			}
		}

	}

	protected TextView getBackButton(){
		return btnBack;
	}


	protected TextView getRightButton(){
		return this.btnRight;
	}



	protected void showProgressDialog() {

		if(loadingPanel != null){
			loadingPanel.setVisibility(View.VISIBLE);

			if(errorBar != null){
				errorBar.setVisibility(View.GONE);
			}

			if(loadBar != null){
				loadBar.setAnimation(AnimationUtils.loadAnimation(this, R.anim.sacle_up_in));
				loadBar.setVisibility(View.VISIBLE);
			}
		}
	}


	protected void closeProgressDialog(){

		if(isFinishing()){
			return;
		}

		if(loadingPanel != null){
			loadingPanel.setVisibility(View.GONE);

			if(loadBar != null){
				loadBar.setAnimation(AnimationUtils.loadAnimation(this, R.anim.sacle_up_out));
				loadBar.setVisibility(View.GONE);
			}
		}
	}

	protected  void closerErrorBar(){

		if(loadingPanel != null){
			loadingPanel.setVisibility(View.GONE);

			if(errorBar != null){
				errorBar.setVisibility(View.GONE);
			}
		}
	}


	/************************************************************************************/


	protected void onErrorAction(int cmd,HandlerException e){

		this.setErrorMsg(e.getMessage());
	}



}
