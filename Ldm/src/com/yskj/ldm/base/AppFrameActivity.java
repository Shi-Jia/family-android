package com.yskj.ldm.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import com.android.commu.parse.Response;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.google.gson.JsonObject;
import com.yskj.ldm.R;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.home.*;
import com.yskj.ldm.activity.impl.PushUpdata;
import com.yskj.ldm.activity.ui.*;
import com.yskj.ldm.beans.UserBean;
import com.yskj.ldm.network.Command;
import com.yskj.ldm.network.ConnectionManager;
import com.yskj.ldm.response.AllResponse;
import com.yskj.ldm.services.BCSPushMessageReceiver;
import com.yskj.ldm.util.BCSUtil;
import com.yskj.ldm.util.DeviceUtil;
import com.yskj.ldm.util.JsonUtil;
import com.yskj.ldm.util.Session;
import com.yskj.ldm.util.StringUtil;



public class AppFrameActivity extends BaseActivity  {


	private static final int Wait_Time = 3000;

	private static final int Handler_Thread_Code = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.app_frame_screen);
		//		((LaoDaoMiApplication)getApplication()).goToMyLocation();
		//		if(this.userid==null||this.userid==0)
		this.initWithApiKey();
   
		new Thread(new FlashThread()).start();

	}


	protected void onSubHandleAction(Message msg){

		if(msg.what == Handler_Thread_Code){

			if(!isFinishing()){
				Log.i("login", "  logintype --->  "+ 	PreferencesService.getInstance(this).getLoginType());
				onShowHomeScreen();
			}
		}
	}


	private void onShowHomeScreen(){

		if(PreferencesService.getInstance(this).getLoginType().equals(1))
		{
			if(mQQAuth.isSessionValid()){
				Map<String, Object> params = this.pservice.getUserInfoPerferences();
				ConnectionManager.getInstance().requestUnionLogin(DeviceUtil.getMobileID(AppFrameActivity.this), this.pservice.getQqopenId(),
						params.get("sex").toString(),params.get("nickname").toString(),params.get("address").toString() ,params.get("headporttait").toString(),
						true, AppFrameActivity.this);
			}
			else 
			{
				this.startActivity(LoginSelectAvtivity.class);
				this.finish();
			}
		}else if(PreferencesService.getInstance(this).getLoginType().equals(4))
		{
			if(!StringUtil.isNull(this.phone)&&!StringUtil.isNull(this.password))
			{
				//			Map<String, Object> map = PreferencesService.getInstance(this).getBaiduUserInfo();
				//			String reAppid = map.get("appid").toString();
				//			String reUserId = map.get("userid").toString();
				//			String reChannelId = map.get("channelid").toString();
				//			String reRequestId = map.get("requestid").toString();
				ConnectionManager.getInstance().requestLogin(this.phone,password,DeviceUtil.getMobileID(this), 
						true, AppFrameActivity.this);		
			}
			else
			{
				this.startActivity(LoginSelectAvtivity.class);
				this.finish();
			}
		}
		else 
		{
			this.startActivity(LoginSelectAvtivity.class);
			this.finish();
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		this.finish();
	}


	protected void onNetworkAction(int cmd,Response response)
	{

		String data = ((AllResponse)response).getMsg();
		Log.i("whb", "data -->  "+data);
		if(null ==data||"".equals(data))
		{
			return ;
		}

		try {
			JSONObject jo = JsonUtil.getJSON(data);
			String errorNo  = jo.get("errorNo").toString();
			String errorInfo = jo.getString("errorInfo");

			if(errorNo.equals(Command.RESULT_OK))
			{
				if(Command.USER_LOGIN.equals(cmd)||Command.USER_UNION_LOGIN.equals(cmd))
				{

					this.showToast("登陆成功");	
					String token =  jo.getString("token");
					String time =  jo.getString("time");
					String reList = jo.get("resList").toString();
					List<HashMap<String, Object>> list = JsonUtil.getList(reList);
					HashMap<String, Object> pramasMap = list.get(0);
					UserBean ub = new UserBean();
					ub.setAREA_ID(pramasMap.get("area_id").toString());
					ub.setCIRCLE_ID(pramasMap.get("circle_id").toString());
					ub.setUSER_PASSWORLD(pramasMap.get("user_passworld").toString());
					ub.setUSER_NIKENAME(pramasMap.get("user_nikename").toString());
					ub.setUSER_ADRESS(pramasMap.get("user_adress").toString());
					ub.setUSER_BIRTH(pramasMap.get("user_birth").toString());
					ub.setUSER_EMAIL(pramasMap.get("user_email").toString());
					ub.setUSER_HEADPORTTAIT(pramasMap.get("user_headporttait").toString());
					ub.setREG_DATE(pramasMap.get("reg_date").toString());
					ub.setUSER_STATE(pramasMap.get("user_state").toString());
					ub.setUSER_NAME(pramasMap.get("user_name").toString());
					ub.setUSER_QQ(pramasMap.get("user_qq").toString());
					ub.setUSER_PHONE(pramasMap.get("user_phone").toString());
					ub.setUSER_ID(pramasMap.get("user_id").toString());
					ub.setUSER_QQ(pramasMap.get("user_sex").toString());

					Integer versionno  = Integer.parseInt(StringUtil.isNull(pramasMap.get("versioncode").toString())?"0":pramasMap.get("versioncode").toString());
					pservice.saveUserInfo(ub.getUSER_ID(), ub.getCIRCLE_ID(), ub.getUSER_PHONE(), ub.getUSER_PASSWORLD(),
							ub.getUSER_NIKENAME(),ub.getUSER_ADRESS(),ub.getUSER_BIRTH(),
							ub.getUSER_QQ(),ub.getUSER_EMAIL(),ub.getAREA_ID(),ub.getREG_DATE(),
							ub.getUSER_SEX(),ub.getUSER_HEADPORTTAIT(),ub.getUSER_STATE());

					pservice.savaVersion(versionno);
					pservice.savaVersionTag(false);
					ConnectionManager.getInstance().setToken(pramasMap.get("user_id").toString(),DeviceUtil.getMobileID(this), token, time);
//					this.pservice.savaPushTag(true);
//					if(!this.pservice.getPushTag())
//						AppFrameActivity.this.stopBaiduPush();
					this.startActivity(ContainerHomeActivity.class);
					this.finish();
				}
				//				else if ()
				//				{
				//                           
				//				}
			}
			else
			{
				this.showToast(errorInfo);	
				this.startActivity(LoginSelectAvtivity.class);
				this.finish();
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}



	/***********************************************************************************8888
	 * 
	 * @author Administrator
	 *
	 */
	private class FlashThread implements Runnable {

		public void run(){
			try{
				Thread.sleep(Wait_Time);
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				sendEmptyMessage(Handler_Thread_Code);
			}
		}
	}


 

	//	@Override
	//	public void OnBindUser(Object o) 
	//	{
	//		if(BCSPushMessageReceiver.isOpen)
	//		{
	//			onShowHomeScreen();
	//			BCSPushMessageReceiver.isOpen = false;
	//		}
	//	}

}
