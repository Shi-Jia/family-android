package com.yskj.ldm.base;

 
 

import com.yskj.ldm.R;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PopupDialog {

	private PopupWindow dialog;
	private Context context;
	
	protected LayoutInflater inflater;

	private View contentView;
	
	private TextView mTitle;
	protected TextView mMessage;
	private Button btnCancel;
	private Button btnNeutral;
	private Button btnOk;
	protected RelativeLayout layout;
	View view_top1,view_top2;

	public PopupDialog (Context context){
		this.context = context;
		inflater = LayoutInflater.from(context);
		registerComponent();
	}
	
	
	private void registerComponent(){
		
		contentView = inflater.inflate(R.layout.popup_dialog, null);
		
		mTitle = (TextView) contentView.findViewById(R.id.dialog_title);
		mMessage = (TextView) contentView.findViewById(R.id.dialog_msg);
		
		btnCancel = (Button) contentView.findViewById(R.id.dialog_cancel);
		btnCancel.setVisibility(View.GONE);
		
		btnNeutral = (Button) contentView.findViewById(R.id.dialog_center);
		btnNeutral.setVisibility(View.GONE);
		
		btnOk = (Button) contentView.findViewById(R.id.dialog_ok);
		btnOk.setVisibility(View.GONE);
		
		view_top1 = contentView.findViewById(R.id.separate_top1);
		view_top1.setVisibility(View.GONE);
		view_top2 = contentView.findViewById(R.id.separate_top2);
		view_top2.setVisibility(View.GONE);
		layout = (RelativeLayout) contentView.findViewById(R.id.dialog_center_layout);
	}
	
	public PopupDialog setTitle(String title){
		
		this.mTitle.setText(title);
		
		return this;
	}
	
	
	public PopupDialog setGravity(int gravity){
		
		this.mMessage.setGravity(gravity);
		
		return this;
	}
	
	
	public PopupDialog setMessage(String message){
		
		this.mMessage.setText(message);
		
		return this;
	}
	
	
	public PopupDialog getTextSize(float size){
		
		float s = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,size, 
				  context.getResources().getDisplayMetrics());

		this.mTitle.setTextSize(s);
		
		return this;
	}


	public PopupDialog setPositiveButton(String label,final DialogOnClickListener listener){

		btnOk.setText(label);
		btnOk.setVisibility(View.VISIBLE);
		view_top2.setVisibility(View.VISIBLE);
		btnOk.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				close();
				if(listener != null){
					listener.onClick(v);
				}
			}
		});

		return this;
	}
	
	
	public PopupDialog setNegativeButton(String label,final DialogOnClickListener listener){
		
		btnCancel.setText(label);
		btnCancel.setVisibility(View.VISIBLE);

		btnCancel.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				close();
				if(listener != null){
					listener.onClick(v);
				}
			}
		});

		return this;
	}
	
	
	public PopupDialog setNeutralButton(String label,final DialogOnClickListener listener){
		
		btnNeutral.setText(label);
		btnNeutral.setVisibility(View.VISIBLE);
		view_top1.setVisibility(View.VISIBLE);
		btnNeutral.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				close();
				if(listener != null){
					listener.onClick(v);
				}
			}
		});

		return this;
	}
	
	
	
	
	public void show(View parentView,int width,int height,int gravity,int x,int y){

		if(dialog == null){
			dialog = new PopupWindow(contentView,width,height);
		}

		if(!dialog.isShowing()){
			dialog.showAtLocation(parentView, gravity, x, y);
		}
	}
	
	
	public void show(View parentView,int width,int height,int gravity){

		if(dialog == null){
			dialog = new PopupWindow(contentView,width,height);
		}

		if(!dialog.isShowing()){
			dialog.showAtLocation(parentView, gravity, 0, 0);
		}
	}
	
	
	public void show(View parentView,int width,int height){

		if(dialog == null){
			dialog = new PopupWindow(contentView,width,height,true);
		}

		if(!dialog.isShowing()){
			dialog.showAtLocation(parentView, Gravity.CENTER, 0, 0);
		}
	}
	
	
	public void close(){
		
		if(dialog != null && dialog.isShowing()){
			dialog.dismiss();
		}
	}
	
	public boolean isShowing(){
		
		if(dialog != null){
			return dialog.isShowing();
		}
		
		return false;
	}
	
	
	public interface DialogOnClickListener {
		public void onClick(View v);
	}
	
}
