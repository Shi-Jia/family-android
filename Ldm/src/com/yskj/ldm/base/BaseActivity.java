package com.yskj.ldm.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.android.commu.net.HandlerException;
import com.android.commu.net.ICommuDataListener;
import com.android.commu.net.NetWorkTask;
import com.android.commu.parse.Response;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.download.HttpClientImageDownloader;
import com.tencent.connect.auth.QQAuth;
import com.tencent.tauth.Tencent;
import com.yskj.ldm.activity.cache.PreferencesService;
import com.yskj.ldm.activity.impl.PushUpdata;
import com.yskj.ldm.base.PopupDialog.DialogOnClickListener;
import com.yskj.ldm.db.DatabaseHelper;
import com.yskj.ldm.global.LaoDaoMiApplication;
import com.yskj.ldm.services.BCSPushMessageReceiver;
import com.yskj.ldm.util.BCSUtil;
import com.yskj.ldm.util.StringUtil;




@SuppressLint("HandlerLeak")
public class BaseActivity extends Activity implements ICommuDataListener 
{

	public static final int Handler_ShowProgress_Code = 1000;

	public static final int Handler_CloseProgress_Code = -1000;

	public static final int Handler_Finished_Code = 2000;

	public static final int Handler_Error_Code = -2000;

	protected int mWidth;
	protected int mHeight;
	protected PreferencesService pservice;
	protected Integer userid;
	protected String circleid;
	protected String phone;
	protected String password;
	protected boolean a_login;
	protected boolean r_password;
	private  PopupDialog dialog ;
	public List<HashMap<String, Object>> circleListData = new ArrayList<HashMap<String, Object>>();
	/**
	 * 数据DB
	 */
	protected SQLiteDatabase db = null;
	//	public BMapManager mMapManager = null;

	public LaoDaoMiApplication laodaomiApplication= null;

	public  String mAppid = "1102928207";
	public static QQAuth mQQAuth;
	public Tencent mTencent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		laodaomiApplication = (LaoDaoMiApplication) getApplication();
		laodaomiApplication.addActivity(this);
		mQQAuth = QQAuth.createInstance(mAppid, this);
		mTencent = Tencent.createInstance(mAppid, this);
		DisplayMetrics metric = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metric);
//		BCSPushMessageReceiver.setPushUpdata(this);
		pservice = new PreferencesService(this);
		Map<String, Object> params = this.pservice.getUserInfoPerferences();
		userid = Integer.parseInt(StringUtil.isNull(params.get("userid").toString())?"0":params.get("userid").toString());
		phone = params.get("phone").toString();
		circleid =params.get("circleid").toString();
		password  =params.get("password").toString();
		//		Log.i("pservice", "phone -->   "+phone);
		//		Log.i("pservice", "password -->   "+password);

		Map<String, Object> paramsLogin = this.pservice.getLoginPerferences();
		a_login =  (Boolean)paramsLogin.get("a_login");
		r_password = (Boolean)paramsLogin.get("r_password");
 
		mWidth = metric.widthPixels; //
		mHeight = metric.heightPixels; //
		int densityDpi = metric.densityDpi;  // 屏幕密度DPI（120 / 160 / 240）
		//				this.showToast(densityDpi +"  w = "+mWidth+"  h = "+mHeight+" <--metric");

	}

	protected  void dialogOnCancel()
	{

	}
	protected  void dialogOnSure()
	{

	}
	 
	protected void showAlertDialog(String leftButton,String rightButton,String msg,String title){

		if(dialog == null){

			dialog = new PopupDialog(this);
			dialog.setGravity(Gravity.CENTER);
			dialog.setTitle(title);
			dialog.setNegativeButton(leftButton, new DialogOnClickListener(){
				public void onClick(View v){
					dialogOnCancel();
				}
			});

			dialog.setPositiveButton(rightButton, new DialogOnClickListener(){
				public void onClick(View v){
					dialogOnSure();
				}
			});
		}
		dialog.setMessage(msg);

		dialog.show(this.getWindow().getDecorView(), LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	}
	protected void initWithApiKey() {
		PushManager.startWork(getApplicationContext(),
				PushConstants.LOGIN_TYPE_API_KEY,
				BCSUtil.getMetaValue(BaseActivity.this, "api_key"));
	}


	protected void stopBaiduPush()
	{
		PushManager.stopWork(getApplicationContext());
	}
	protected void showToast(String msg) {

		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}

	protected void showProgressDialog() {

	}

	protected void closeProgressDialog() {

	}

	public void startActivity(Class<?> cls) {

		super.startActivity(new Intent(this, cls));
	}

	protected void startActivityForResult(Class<?> cls, int requestCode) {

		super.startActivityForResult(new Intent(this, cls), requestCode);
	}

	protected View inflate(int resId) {

		return LayoutInflater.from(this).inflate(resId, null);
	}

	public float deviceValue(int unit, float value) {

		return TypedValue.applyDimension(unit, value, getResources()
				.getDisplayMetrics());
	}

	/**********************************************************************************/

	public boolean checkNetworkState() {
		// TODO Auto-generated method stub

		ConnectivityManager mConnectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE); 
		NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo(); 

		if (mNetworkInfo != null) { 
			return mNetworkInfo.isAvailable(); 
		} 			
		return false;
	}

	public void showConnectionProgress(NetWorkTask task) {
		// TODO Auto-generated method stub

		Message msg = new Message();
		msg.what = Handler_ShowProgress_Code;
		msg.obj = task;
		handler.sendMessage(msg);

	}

	public void closeConnectionProgress(NetWorkTask task) {
		// TODO Auto-generated method stub

		Message msg = new Message();
		msg.what = Handler_CloseProgress_Code;
		msg.obj = task;
		handler.sendMessage(msg);
	}

	public void onError(HandlerException e) {
		Message msg = new Message();
		msg.what = Handler_Error_Code;
		msg.obj = e;
		handler.sendMessage(msg);
	}

	public void onFinished(NetWorkTask task, Response response) {
		// TODO Auto-generated method stub

		Message msg = new Message();
		msg.what = Handler_Finished_Code;
		msg.arg1 = task.getCommandId();
		msg.obj = response;
		handler.sendMessage(msg);
	}

	/**********************************************************************************************/

	private Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			onHandleAction(msg);
		}
	};

	public void sendMessage(Message msg) {
		handler.sendMessage(msg);
	}

	protected void sendEmptyMessage(int what) {
		handler.sendEmptyMessage(what);
	}

	private void onHandleAction(Message msg) {

		switch (msg.what) {

		case Handler_ShowProgress_Code: {
			onShowProgressAction((NetWorkTask) msg.obj);
		}
		break;

		case Handler_Finished_Code: {
			onNetworkAction(msg.arg1, (Response) (msg.obj));
		}
		break;

		case Handler_CloseProgress_Code: {
			onCloseProgressAction((NetWorkTask) msg.obj);
		}
		break;

		case Handler_Error_Code: {
			HandlerException e = (HandlerException) (msg.obj);
			onErrorAction(e.getTask().getCommandId(), e);
		}
		break;

		default: {
			onSubHandleAction(msg);
		}
		break;

		}
	}

	protected void onSubHandleAction(Message msg) {
 

	}

	/********************************************************************************************/

	protected void onShowProgressAction(NetWorkTask task) {
		this.showProgressDialog();
	}

	protected void onCloseProgressAction(NetWorkTask task) {
		this.closeProgressDialog();
	}

	protected void onErrorAction(int cmd, HandlerException e) {

	}

	protected void onNetworkAction(int cmd, Response response) {

	}

//	protected void getBaiduUser(Object o) {
//
//	}
//
//	protected void onMessageBox(String type) {
//
//	}
//	@Override
//	public void onUpdataNew(String type) {
//
//		onMessageBox(type);
//	}
//
//	@Override
//	public void onBindUser(Object o) {
//		if(BCSPushMessageReceiver.isOpen)
//		{
//			//			onShowHomeScreen();
//			BCSPushMessageReceiver.isOpen = false;
//		}
//		getBaiduUser(o);
//	}

	//	public void onCallback(BitmapItem item) {
	//
	//	}

}
