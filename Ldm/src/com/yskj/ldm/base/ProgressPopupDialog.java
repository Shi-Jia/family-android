package com.yskj.ldm.base;
 

import com.yskj.ldm.R;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class ProgressPopupDialog extends PopupDialog {

	private ProgressBar progressBar;
	private TextView progressTxt;

	public ProgressPopupDialog(Context context) {
		super(context);

		layout.removeView(mMessage);
		View view = inflater.inflate(R.layout.app_progress_view, null);
		layout.addView(view, new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));

		progressTxt = (TextView) view.findViewById(R.id.textView1);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
	}

	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public TextView getProgressTxt() {
		return progressTxt;
	}

}
