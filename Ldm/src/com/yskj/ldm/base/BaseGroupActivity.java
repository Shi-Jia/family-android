package com.yskj.ldm.base;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.android.commu.net.HandlerException;
import com.android.commu.net.ICommuDataListener;
import com.android.commu.net.NetWorkTask;
import com.android.commu.parse.Response;

public class BaseGroupActivity extends ActivityGroup implements ICommuDataListener{
	
	public static final int Handler_ShowProgress_Code = 1000;
	
	public static final int Handler_CloseProgress_Code = -1000;
	
	public static final int Handler_Finished_Code = 2000;
	
	public static final int Handler_Error_Code = -2000;
	
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);   
		
	}

	
	
	/*****************************************************************************************/
	
	protected void showToast(String msg){
		
		 Toast.makeText(this, msg,Toast.LENGTH_LONG).show();
	}
	
	protected void showProgressDialog() {

		
	}
	
	protected void closeProgressDialog(){
		
	}
	
	
	
	protected void startActivity(Class<?> cls) {
		
		super.startActivity(new Intent(this, cls));
	}
	
	
	protected View inflate(int resId){
		
		return LayoutInflater.from(this).inflate(resId, null);
	}
	
/**********************************************************************************/
	
	public boolean checkNetworkState() {
		// TODO Auto-generated method stub
		return true;
	}

	public void showConnectionProgress(NetWorkTask task) {
		// TODO Auto-generated method stub
		
		Message.obtain(handler, Handler_ShowProgress_Code, task);
	}
	
	public void closeConnectionProgress(NetWorkTask task) {
		// TODO Auto-generated method stub
		
		Message.obtain(handler, Handler_CloseProgress_Code, task);
	}

	public void onError(HandlerException e) {
		// TODO Auto-generated method stub
		
		Message.obtain(handler, Handler_Error_Code, e);
	}

	public void onFinished(NetWorkTask task, Response response) {
		// TODO Auto-generated method stub
		
		Message.obtain(handler, Handler_Finished_Code, task.getCommandId(), 0, response);
	}
	
/**********************************************************************************************/
	
	protected Handler handler = new Handler(){
		
		public void handleMessage(Message msg){
			onHandleAction(msg);
		}
	};
	
	
	protected void onHandleAction(Message msg){
		
		switch(msg.what){
		
			case Handler_ShowProgress_Code:{
				onShowProgressAction((NetWorkTask)msg.obj);
			}break;
		
			case Handler_Finished_Code:{
				onNetworkAction(msg.arg1,(Response)(msg.obj));
			}break;
			
			case Handler_CloseProgress_Code:{
				onCloseProgressAction((NetWorkTask)msg.obj);
			}break;
			
			case Handler_Error_Code:{
				HandlerException e= (HandlerException)(msg.obj);
				onErrorAction(e.getTask().getCommandId(),e);
			}break;
				
			
		}
	}
	
/********************************************************************************************/
	
	
	protected void onShowProgressAction(NetWorkTask task){
		this.showProgressDialog();
	}
	
	protected void onCloseProgressAction(NetWorkTask task){
		this.closeProgressDialog();
	}
	
	protected void onErrorAction(int cmd,HandlerException e){
		showToast(e.getMessage());
	}
	
	protected void onNetworkAction(int cmd,Response response){
		
	}
}
